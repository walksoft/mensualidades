<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("/dashboard");
});

Route::get('/home', function () {
    return redirect("/dashboard");
});

Route::get('dashboard', 'AdminController@index')->name('admin');

Auth::routes();

// setea ruta formulario login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');

Route::group(['middleware'=>['auth'],'prefix'=>'dashboard'],function(){ 
    Route::get('profile','Users\UsersController@profile')->name('users.profile');
    Route::post('profile/update','Users\UsersController@save_profile')->name('users.profile.update');
});

Route::group(['middleware'=>['auth'],'prefix'=>'dashboard/admin'],function(){  
    Route::resource('roles', 'Roles\RolesController');
    Route::put('roles/ajax/edit','Roles\RolesController@edit')->name('roles.ajax.edit');
    Route::put('roles/ajax/update','Roles\RolesController@update')->name('roles.ajax.update');
    Route::put('roles/ajax/delete','Roles\RolesController@destroy')->name('roles.ajax.delete');
    Route::resource('users', 'Users\UsersController');
    Route::put('users/ajax/edit','Users\UsersController@edit')->name('users.ajax.edit');
    Route::put('users/ajax/update','Users\UsersController@update')->name('users.ajax.update');
    Route::put('users/ajax/delete','Users\UsersController@destroy')->name('users.ajax.delete');
    Route::post('image-view','Users\UsersController@image_store');
    Route::get('avatar/{id}','Users\UsersController@avatar')->name('users.avatar');
    Route::get('configurations','Config\ConfigController@index')->name('configurations');
    Route::put('configurations/ajax/update','Config\ConfigController@update')->name('configurations.ajax.update');
    Route::put('configurations/ajax/updatePayments','Config\ConfigController@updatePayments')->name('configurations.ajax.updatePayments');
    Route::get('configurations/avatar/{id}','Config\ConfigController@avatar')->name('configurations.avatar');
    Route::put('configurations/ajax/exec/payments/process','Config\ConfigController@exec_payments_process')->name('configurations.ajax.exec_payments_process');
    Route::put('configurations/ajax/exec/overdue/payments/process','Config\ConfigController@exec_overdue_payments_process')->name('configurations.ajax.exec_overdue_payments_process');    
    Route::put('configurations/ajax/exec/instructors/payment/process','Config\ConfigController@exec_instructors_payment_process')->name('configurations.ajax.exec_instructors_payment_process');    
    Route::resource('services', 'Services\ServicesController');
    Route::put('services/ajax/edit','Services\ServicesController@edit')->name('services.ajax.edit');
    Route::put('services/ajax/update','Services\ServicesController@update')->name('services.ajax.update');
    Route::put('services/ajax/delete','Services\ServicesController@destroy')->name('services.ajax.delete');
    Route::put('services/ajax/types','Services\ServicesController@get_tbl_types')->name('services.ajax.table_types');
    Route::put('services/ajax/types/store','Services\ServicesController@store_type')->name('services.ajax.store_type');    
    Route::put('services/ajax/levels','Services\ServicesController@get_tbl_levels')->name('services.ajax.table_levels'); 
    Route::put('services/ajax/levels/store','Services\ServicesController@store_level')->name('services.ajax.store_level');
    Route::put('services/ajax/update/status','Services\ServicesController@update_status')->name('services.ajax.update_status');    
    Route::put('services/ajax/delete/type/level','Services\ServicesController@delete_type_or_level')->name('services.ajax.delete_type_or_level');    
    Route::resource('students','Students\StudentsController');
    Route::put('students/ajax/edit','Students\StudentsController@edit')->name('students.ajax.edit');
    Route::put('students/ajax/update','Students\StudentsController@update')->name('students.ajax.update');
    Route::put('students/ajax/delete','Students\StudentsController@destroy')->name('students.ajax.delete');
    Route::resource('instructors','Instructors\InstructorsController');
    Route::put('instructors/ajax/edit','Instructors\InstructorsController@edit')->name('instructors.ajax.edit');
    Route::put('instructors/ajax/update','Instructors\InstructorsController@update')->name('instructors.ajax.update');
    Route::put('instructors/ajax/delete','Instructors\InstructorsController@destroy')->name('instructors.ajax.delete');     
    Route::resource('courses','Courses\CoursesController');
    Route::put('courses/ajax/edit','Courses\CoursesController@edit')->name('courses.ajax.edit');
    Route::put('courses/ajax/update','Courses\CoursesController@update')->name('courses.ajax.update');
    Route::put('courses/ajax/delete','Courses\CoursesController@destroy')->name('courses.ajax.delete');
    Route::put('courses/ajax/calcule/time','Courses\CoursesController@update_end_time')->name('courses.ajax.update_end_time');
    Route::put('inscriptions/ajax/index','Inscriptions\InscriptionsController@index')->name('inscriptions.ajax.index');
    Route::put('inscriptions/ajax/edit','Inscriptions\InscriptionsController@edit')->name('inscriptions.ajax.edit');
    Route::put('inscriptions/ajax/update','Inscriptions\InscriptionsController@update')->name('inscriptions.ajax.update');
    Route::put('inscriptions/ajax/delete','Inscriptions\InscriptionsController@destroy')->name('inscriptions.ajax.delete');
    Route::put('inscriptions/ajax/student','Inscriptions\InscriptionsController@info_student')->name('inscriptions.ajax.info_student');
    Route::put('inscriptions/ajax/student/inscribe','Inscriptions\InscriptionsController@inscribe')->name('inscriptions.ajax.inscribe');
    Route::put('inscriptions/ajax/table/registration','Inscriptions\InscriptionsController@table_registration')->name('inscriptions.ajax.table_registration');
    //payments
    Route::get('payments','Payments\PaymentsController@index')->name('payments.index'); 
    Route::put('payments/courses','Payments\PaymentsController@get_courses_student')->name('payments.ajax.get_courses_student');
    Route::put('payments/info/course','Payments\PaymentsController@info_course')->name('payments.ajax.info_course');
    Route::put('payments/save','Payments\PaymentsController@store')->name('payments.ajax.store');    
    Route::put('payments/total','Payments\PaymentsController@get_total')->name('payments.ajax.get_total');    
    Route::put('payments/add/payment','Payments\PaymentsController@add_new_payment')->name('payments.ajax.add_new_payment');    
    Route::put('payments/add/payment/courses','Payments\PaymentsController@get_courses_student_new_payment')->name('payments.ajax.get_courses_student_new_payment');    
    Route::put('payments/add/payment/courses/cash/value','Payments\PaymentsController@info_value_cash_new_payment')->name('payments.ajax.info_value_cash_new_payment');        
    Route::put('payments/save/payment','Payments\PaymentsController@store_new_payment')->name('payments.ajax.store_new_payment');             
    Route::get('payments/view/all/','Payments\PaymentsController@index_payments')->name('payments.index_payments'); 
    Route::get('payments/export/excel/{status?}/{concept?}/{start_date?}/{end_date?}','Payments\PaymentsController@export_excel')->name('payments.export.excel');     
    //jobs
    Route::get('jobs/process/payments/monthly_payments','Jobs\JobsController@monthly_payments_process')->name('jobs.monthly_payments_process'); 
    Route::get('files/display/{id}','Users\UsersController@display_file')->name('display_file'); 
});

Route::group(['middleware'=>['auth'],'prefix'=>'dashboard/student'],function(){    
    Route::get('courses','Students\StudentsController@my_courses')->name('students.my_courses');
    Route::get('payments','Students\StudentsController@my_payments')->name('students.my_payments');
    Route::post('paypal/payment','Payments\Paypal\PaypalController@paypal_payment')->name('students.paypal_payment');
    Route::get('paypal/payment/status','Payments\Paypal\PaypalController@paypal_payment_status')->name('students.paypal_payment_status');
});

