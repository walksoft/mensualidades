<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\Models\Payment;

class PaymentExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    private $fileName = 'payments.xlsx';
    
    private $status;
    private $concept;
    private $start_date;
    private $end_date;

    public function __construct(int $status, int $concept, string $start_date, string $end_date)
    {
        $this->status = $status;
        $this->concept  = $concept;
        $this->start_date = $start_date;
        $this->end_date  = $end_date;        
    }
    
    public function collection()
    {
        
        $status = $this->status ? $this->status : NULL;
        $concept = $this->concept ? $this->concept : NULL;
        $start_date = $this->start_date ? $this->start_date : NULL;
        $end_date = $this->end_date ? $this->end_date : NULL;
        
        $data = Payment::leftJoin('courses','payments.course_id','courses.course_id')
                ->join('status_payments','payments.status_id','status_payments.id')
                ->join('payment_concepts','payments.concept_id','payment_concepts.id')
                ->join('users','payments.user_id','users.id')
                ->leftJoin('services','courses.service_id','services.service_id');        

        if($status){
            $data->where('payments.status_id',$status);
        }//status

        if($concept){
            $data->where('payments.concept_id',$concept);
        }//concept

        //date
        if($start_date && $end_date){
            $start_date = \Carbon\Carbon::createFromFormat('d-m-Y',$start_date)->format('Y-m-d');
            $end_date = \Carbon\Carbon::createFromFormat('d-m-Y',$end_date)->format('Y-m-d');
            $data->whereBetween('payments.deadline',[$start_date,$end_date]);
        }//if
        elseif($start_date && !$end_date){
            $start_date = \Carbon\Carbon::createFromFormat('d-m-Y',$start_date)->format('Y-m-d');
            $data->whereDate('payments.deadline','>',$start_date);
        }//elseif
        elseif($end_date && !$start_date){
            $end_date = \Carbon\Carbon::createFromFormat('d-m-Y',$end_date)->format('Y-m-d');
            $data->whereDate('payments.deadline','<',$end_date);
        }//elseif

        $data->orderBy('payments.created_at','DESC');
        
        $data = $data->get([                
                'payments.reference',
                'status_payments.name as status_id',
                'payment_concepts.name as concept_id',
                \DB::raw("CASE WHEN payments.total_paid IS NULL THEN payments.amount ELSE payments.total_paid END amount"),
                \DB::raw("CONCAT(courses.code,' - ',services.name) as course"),  
                \DB::raw("CASE WHEN users.last_name IS NULL THEN users.name ELSE CONCAT(users.name,' ',users.last_name) END full_name"),
                'payments.deadline', 
            ]);    
        
        return $data;
    }
        
    public function map($data): array
    {
        return[
            $data->reference,
            $data->status_id,
            $data->concept_id,
            (string)$data->amount,
            $data->course,
            $data->full_name,
            ($data->deadline ? \Carbon\Carbon::parse($data->deadline)->format('d/m/Y') : NULL),
        ];
    }
    
    public function headings():array
    {
        return [
            trans('adminlte::payments.reference'),
            trans('adminlte::payments.status'),
            trans('adminlte::payments.concept'),
            trans('adminlte::payments.value'),
            trans('adminlte::courses.course'),
            trans('adminlte::payments.student_instructor'),
            trans('adminlte::payments.deadline')
        ];
    }
}