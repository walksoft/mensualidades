<?php

namespace App;

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Auth;

class MenuFilter implements FilterInterface
{
    public function transform($item, Builder $builder)
    {

          //validate item permits
          if(array_key_exists("permission",$item)){

            $permissionArray = explode(',',$item['permission']);
            $count = 0;
            foreach($permissionArray as $info){
              
                if(auth()->user()->hasPermissionTo($info)){
                    $count++;
                }//if
            }

            if($count==0){
                return false;
            }
 
        }//if
 
        //traslate
        $item['text'] = trans($item['text']);
        return $item;
    }
}