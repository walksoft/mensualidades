<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\User;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Payment;
use App\Http\Controllers\Notifications\NotificationsController;

class Payment_notification extends Notification
{
    use Queueable;

    private $user;
    private $payment;
    private $type;
    
    /**
     * Create a new notification instance.
     * @param User $user User
     * @param Payment $payment payment
     * @param int $type 1.notification after paying, 2.notification generate payment, 3.notification payment pending reminder
     * @return void
     */
    public function __construct(User $user, Payment $payment, int $type)
    {
        $parameters = get_parameter(['smtp_username','config_name','smtp_host','smtp_port','smtp_password','smtp_encryption']);
        
        //mail params
        \Config::set('mail.host',$parameters->smtp_host['value']);
        \Config::set('mail.port',$parameters->smtp_port['value']);
        \Config::set('mail.username',$parameters->smtp_username['value']);
        \Config::set('mail.password',$parameters->smtp_password['value']);
        \Config::set('mail.encryption',$parameters->smtp_encryption['value']);
        \Config::set('mail.from.address',$parameters->smtp_username['value']);
        \Config::set('mail.from.name',$parameters->config_name['value']);  
        
        //app name
        \Config::set('app.name',$parameters->config_name['value']);
        
        $this->user = $user;
        $this->payment = $payment;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {   
        switch($this->type){
            case 1:
                $text_subject=get_parameter('subject_notification_after_paying','Notificación');
                $body=get_parameter('notification_after_paying');
                break;
            case 2:
                $text_subject=get_parameter('subject_notification_generate_payment','Notificación');
                $body=get_parameter('notification_generate_payment');
                break;
            case 3:
                $text_subject=get_parameter('subject_notification_payment_pending_reminder','Notificación');
                $body=get_parameter('notification_payment_pending_reminder');
                break;
            default:
                $text_subject = 'Notificación';
                $body = '';
        }//switch
        $subject = NotificationsController::process_keywords($this->payment,$text_subject);
        $msg = NotificationsController::process_keywords($this->payment,$body);
        return (new MailMessage)->subject($subject)->line(new \Illuminate\Support\HtmlString($msg));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
