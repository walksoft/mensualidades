<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Payment_detail
 *
 * @property int $id
 * @property int $payment_id
 * @property int $course_id
 * @property double $amount
 * @property double $additional
 * @property double $discount
 * @property double $total
 * @property string $observations
 * @property int $ind_system
 * @property int $admin_id
 * @package App\Models
 */
class Payment_detail extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = 'payment_details';
    protected $primaryKey = 'id';
    protected $casts = [
        'id'=>'int',
        'payment_id'=>'int',
        'course_id'=>'int',
    ];
    protected $fillable = [
        'payment_id',
        'course_id',          
        'amount',        
        'additional',
        'discount',  
        'total',
        'observations',
        'ind_system',
        'admin_id',
    ];
    
    public function payments(){
        return $this->belongsTo(\App\Models\Payment::class,'payment_id','id');
    }    
    
    public function course(){
        return $this->belongsTo(\App\Models\Course::class,'course_id','course_id');
    }
        
    public function admin(){
        return $this->belongsTo(\App\User::class,'admin_id','id');
    }  
    
    public function generateTags():array{
        return['payment_details'];
    }//generateTag       
}
