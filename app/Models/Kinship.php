<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Kinship
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 * @package App\Models
 */
class Kinship extends Model
{
    protected $fillable = ['name','description','status'];
}
