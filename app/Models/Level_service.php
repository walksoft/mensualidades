<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Level_service
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @package App\Models
 */
class Level_service extends Model
{
    protected $casts = ['id'=>'int'];
    protected $fillable = ['name','description','status'];
    
    public function service()
    {
        return $this->hasMany(\App\Models\Service::class,'level_id','id');
    }
}
