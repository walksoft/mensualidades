<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Course
 *
 * @property int $course_id
 * @property string $code
 * @property int $service_id
 * @property int $user_id
 * @property string $frequency
 * @property string $start_time
 * @property string $end_time
 * @property int $status
 * @package App\Models
 */
class Course extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $primaryKey = 'course_id';
    protected $casts = [
        'service_id'=>'int',
        'user_id'=>'int',
        'frequency'=>'array',
        'status' => 'int',
    ];
    protected $fillable = [
        'code',
        'service_id',
        'user_id',
        'payment_instructor',
        'frequency',
        'start_time',
        'end_time',
        'observations',
        'status'
    ];
    
    public function service(){
        return $this->belongsTo(\App\Models\Service::class,'service_id','service_id');
    }
    
    public function instructor(){
        return $this->belongsTo(\App\User::class,'user_id','id');
    }
    
    public function registration(){
        return $this->hasMany(\App\Models\Student_registration::class,'course_id','course_id')->orderBy('id','DESC');
    }
    
    public function payments(){
        return $this->hasMany(\App\Models\Payment::class,'course_id','course_id')->orderBy('id','DESC');
    }
    
    public function array_frequency(){        
        $frequency = NULL;
        if($this->frequency){
            foreach(json_decode($this->frequency) as $day){
                $frequency[] = trans('adminlte::adminlte.'.$day);
            }//foreach
        }
        return $frequency;
    }//if
    
    public function array_frequency_original(){        
        return $this->frequency ? json_decode($this->frequency) : NULL;
    }//if
    
    public function generateTags():array{
        return['courses'];
    }//generateTag     
}
