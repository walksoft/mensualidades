<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Tutor
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $phone_number
 * @property string $address
 * @property int $kinship_id
 * @package App\Models
 */
class Tutor extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name',
        'last_name',
        'email',
        'phone_number',
        'address',
        'kinship_id'
    ];
    
    public function user(){
      return $this->belongsTo(\App\User::class);
    }

    public function generateTags():array{
        return['tutors'];
    }//generateTag

}
