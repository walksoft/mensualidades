<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Login_log
 *
 * @property int $service_id
 * @property string $name
 * @property string $description
 * @property int $session_duration
 * @property int $type_id
 * @property int $level_id
 * @property double $monthly_value
 * @property double $timely_payment_value
 * @property double $registration_value
 * @property int $status
 * @package App\Models
 */
class Login_log extends Model
{    
    protected $fillable = [
        'user_id',
        'event',
        'ip_address',
        'user_agent'
    ];
    
    public function user(){
        return $this->belongsTo(\App\User::class,'user_id','id');
    }
}