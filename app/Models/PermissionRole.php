<?php
/**
 * Created by Reliese Model.
 * Date: Sat, 12 May 2018 21:06:21 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class AppParameter
 *
 * @property int $ParameterID
 * @property string $strParent
 * @property string $strValue
 *
 * @package App\Models
 */
class PermissionRole extends Model implements Auditable
{    
        use \OwenIt\Auditing\Auditable;

	protected $primaryKey = 'id';
        protected $table = 'permission_role';
	public $incrementing = true;
	public $timestamps = true;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'permission_id',
                'role_id'
	];
        
        public function generateTags():array{
            return['permission_role'];
        }//generateTag        
}
