<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Status_payment
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @package App\Models
 */
class Status_payment extends Model
{
    protected $casts = ['id'=>'int'];
    protected $fillable = ['name','description','status'];
    
    public function payments()
    {
        return $this->hasMany(\App\Models\Payment::class,'id','status_id');
    }
}
