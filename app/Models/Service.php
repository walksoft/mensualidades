<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Service
 *
 * @property int $service_id
 * @property string $name
 * @property string $description
 * @property int $session_duration
 * @property int $type_id
 * @property int $level_id
 * @property double $monthly_value
 * @property double $timely_payment_value
 * @property double $registration_value
 * @property int $status
 * @package App\Models
 */
class Service extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $primaryKey = 'service_id';
    protected $casts = [
        'service_id'=>'int',
        'type_id'=>'int',
        'level_id'=>'int'
    ];
    protected $fillable = [
        'name',
        'description',
        'session_duration',
        'type_id',
        'level_id',
        'monthly_value',
        'timely_payment_value',
        'registration_value',
        'status'
    ];
    
    public function type_service(){
        return $this->belongsTo(\App\Models\Type_service::class,'type_id','id');
    }
    
    public function level_service(){
        return $this->belongsTo(\App\Models\Level_service::class,'level_id','id');
    }
    
    public function course(){
        return $this->belongsTo(\App\Models\Course::class,'service_id','service_id');
    }
    
    public function generateTags():array{
        return['services'];
    }//generateTag    
}
