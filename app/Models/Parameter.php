<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Parameter extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $primaryKey = 'id';
    protected $table = 'parameters';
    public $incrementing = false;
    public $timestamps = true;

    protected $casts = [
            'id' => 'int'
    ];

    protected $fillable = [
    'name',
    'description',
    'value',

    ];
    
    public function generateTags():array{
        return['parameters'];
    }//generateTag        
}
