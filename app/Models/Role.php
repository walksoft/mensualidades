<?php
/**
 * Created by Reliese Model.
 * Date: Sat, 12 May 2018 21:06:21 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;

/**
 * Class AppParameter
 *
 * @property int $ParameterID
 * @property string $strParent
 * @property string $strValue
 *
 * @package App\Models
 */
class Role extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
        use HasRolesAndPermissions;
        
	protected $primaryKey = 'id';
        protected $table = 'roles';
	public $incrementing = true;
	public $timestamps = true;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'name',
                'slug',
		'description',
		'special',
		'is_admin'
	];
        
        public function generateTags():array{
            return['roles'];
        }//generateTag
}
