<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class User_file
 *
 * @property int $id
 * @property int $user_id
 * @property int $file_id
 * @property string $file_name
 * @property string $route
 * @property string $extension
 * @property int $file_size
 * @package App\Models
 */
class User_file extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $casts = [
        'user_id'=>'int',
        'file_id'=>'int',
        'file_size'=>'int'
    ];
    protected $fillable = [
        'user_id',
        'file_id',
        'file_name',
        'route',
        'extension',
        'file_size'
    ];
    
    public function user(){
        return $this->belongsTo(\App\User::class,'user_id','id');
    }
    
    public function file_type(){
        return $this->belongsTo(\App\Models\File_type::class,'file_id','id');
    }    
    
    public function generateTags():array{
        return['user_files'];
    }//generateTag
}
