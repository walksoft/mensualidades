<?php
/**
 * Created by Reliese Model.
 * Date: Sat, 12 May 2018 21:06:21 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AppParameter
 *
 * @property int $ParameterID
 * @property string $strParent
 * @property string $strValue
 *
 * @package App\Models
 */
class Permission extends Model
{

	protected $primaryKey = 'id';
    protected $table = 'permissions';
	public $incrementing = false;
	public $timestamps = true;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'name',
        'slug',
        'description'
	];
}
