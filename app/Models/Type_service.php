<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Type_service
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @package App\Models
 */
class Type_service extends Model
{
    protected $casts = ['id'=>'int'];
    protected $fillable = ['name','description','status'];
    
    public function service()
    {
        return $this->hasMany(\App\Models\Service::class,'type_id','id');
    }
}
