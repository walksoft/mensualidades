<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment_concept
 *
 * @property int $id
 * @property int $type_id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @package App\Models
 */
class Payment_concept extends Model
{
    protected $casts = ['id'=>'int'];
    protected $fillable = ['type_id','name','description','status'];
    
    public function payments()
    {
        return $this->hasMany(\App\Models\Payment::class,'id','concept_id');
    }
    
    public function transaction_type(){
        return $this->belongsTo(\App\Models\Transaction_type::class,'type_id','type_id');
    }
}
