<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment_concept
 *
 * @property int $type_id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @package App\Models
 */
class Transaction_type extends Model
{
    protected $primaryKey = 'type_id';
    protected $casts = ['type_id'=>'int'];
    protected $fillable = ['name','description','status'];
    
    public function payments()
    {
        return $this->hasMany(\App\Models\Payment_concept::class,'type_id','type_id');
    }
}
