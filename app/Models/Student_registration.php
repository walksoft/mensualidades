<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Student_registration
 *
 * @property int $course_id
 * @property string $code
 * @property int $service_id
 * @property int $user_id
 * @property string $frequency
 * @property string $start_time
 * @property string $end_time
 * @property int $status
 * @package App\Models
 */
class Student_registration extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $casts = [
        'course_id'=>'int',
        'user_id'=>'int'
    ];
    protected $fillable = [
        'course_id',
        'user_id',
        'monthly_value',
        'timely_payment_value',
        'registration_value',
        'observations',
        'status'
    ];
    
    public function course(){
        return $this->belongsTo(\App\Models\Course::class,'course_id','course_id');
    }
    
    public function student(){
        return $this->belongsTo(\App\User::class,'user_id','id');
    }
    
    public function generateTags():array{
        return['student_registrations'];
    }//generateTag
    
}
