<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class File_type
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @package App\Models
 */
class File_type extends Model
{
    protected $casts = ['id'=>'int'];
    protected $fillable = ['name','description','status'];
    
    public function user_file(){
        return $this->hasMany(App\Models\User_file::class);
    }
}
