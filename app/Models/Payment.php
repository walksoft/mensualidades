<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Payment
 *
 * @property int $service_id
 * @property string $name
 * @property string $description
 * @property int $session_duration
 * @property int $type_id
 * @property int $level_id
 * @property double $monthly_value
 * @property double $timely_payment_value
 * @property double $registration_value
 * @property int $status
 * @package App\Models
 */
class Payment extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = 'payments';
    protected $primaryKey = 'id';
    protected $casts = [
        'reference'=>'int',
        'user_id'=>'int',
        'course_id'=>'int',
        'payment_details_sent'=>'array',
        'payment_details_received'=>'array',
    ];
    protected $fillable = [
        'reference',
        'concept_id',
        'user_id',
        'course_id',          
        'amount',        
        'additional',
        'discount',  
        'total_paid',
        'deadline',
        'payment_details_sent',
        'payment_details_received',
        'payment_method',
        'observations',
        'ind_system',
        'admin_id',
        'status_id',
        'payment_date',
    ];
    
    public function alumn(){
        return $this->belongsTo(\App\User::class,'user_id','id');
    }
    
    public function course(){
        return $this->belongsTo(\App\Models\Course::class,'course_id','course_id');
    }
    
    public function admin(){
        return $this->belongsTo(\App\User::class,'admin_id','id');
    }
    
    public function status(){
        return $this->belongsTo(\App\Models\Status_payment::class,'status_id','id');
    }
    
    public function concept(){
        return $this->belongsTo(\App\Models\Payment_concept::class,'concept_id','id');
    }
    
    public function details(){
        return $this->hasMany(\App\Models\Payment_detail::class,'payment_id','id');
    }
    
    public function generateTags():array{
        return['payments'];
    }//generateTag       
}
