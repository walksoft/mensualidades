<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    use Notifiable;
    use HasRolesAndPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','last_name','email','phone_number','address','password','observations','avatar','type_user','status','last_login_at','last_login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function adminlte_image(){
        return route('users.avatar',['id'=>$this->id]).'?'.time();
    }
    
    public function tutor(){
        return $this->hasOne(\App\Models\Tutor::class);
    }
    
    public function user_files(){
        return $this->hasMany(\App\Models\User_file::class,'user_id','id');
    }
    
    public function registration(){
        return $this->hasMany(\App\Models\Student_registration::class,'user_id','id');
    }
    
    public function active_registration(){
        return $this->hasMany(\App\Models\Student_registration::class,'user_id','id')->where('student_registrations.status',1);
    }
    
    public function courses(){
        return $this->hasMany(\App\Models\Course::class,'user_id','id');
    }
    
    public function active_courses(){
        return $this->hasMany(\App\Models\Course::class,'user_id','id')->where('courses.status',1);
    }
    
    public function payments(){
        return $this->hasMany(\App\Models\Payment::class,'user_id','id')->orderBy('id','DESC');
    }
    
    public function pending_payments(){
        return $this->hasMany(\App\Models\Payment::class,'user_id','id')->where('payments.status_id',1)->orderBy('id','DESC');
    } 

    public function records_payments(){
        return $this->hasMany(\App\Models\Payment::class,'user_id','id')->where('payments.status_id','<>',1)->orderBy('id','DESC');
    }     
            
    public function admin_payments_detail(){
        return $this->hasMany(\App\Models\Payment_detail::class,'admin_id','id');
    }
    
    public function name(){
        return $this->name.($this->last_name ? ' '.$this->last_name : '');
    }
}
