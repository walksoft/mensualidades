<?php

namespace App\Http\Controllers\Courses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\User;
use App\Models\Course;
use App\Models\Service;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable; 

class CoursesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder){ 

        if(!auth()->user()->hasPermissionTo('list.course') && 
           !auth()->user()->hasPermissionTo('create.course') &&
           !auth()->user()->hasPermissionTo('edit.course') &&
           !auth()->user()->hasPermissionTo('delete.course')){            
          Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
          return redirect()->route('admin');
        }
        
        $permits = [];
        $permits['create'] = auth()->user()->hasPermissionTo('create.course');
        $permits['edit']   = auth()->user()->hasPermissionTo('edit.course');
        $permits['delete'] = auth()->user()->hasPermissionTo('delete.course');
        
        $permits_inscriptions['list'] = auth()->user()->hasPermissionTo('list.inscription');
        $permits_inscriptions['create'] = auth()->user()->hasPermissionTo('create.inscription');
        $permits_inscriptions['edit']   = auth()->user()->hasPermissionTo('edit.inscription');
        $permits_inscriptions['delete'] = auth()->user()->hasPermissionTo('delete.inscription');
        
        if ($request->ajax()){

            $courses =  Course::join('services','courses.service_id','services.service_id')
                    ->join('users','courses.user_id','users.id')
                    ->get(['courses.course_id',
                            'courses.code',
                            'services.name as service',
                            'users.name',
                            'users.last_name',
                            'courses.frequency',
                            'courses.start_time',
                            'courses.end_time',
                            'courses.status'
                        ]);
            
            $datatable = Datatables::of($courses);

            // Columna Inscripciones
            if($permits_inscriptions['list'] || $permits_inscriptions['list'] || $permits_inscriptions['list'] || $permits_inscriptions['list']){
                $datatable = $datatable->addColumn(trans('adminlte::inscriptions.inscriptions'),function($courses){
                    return '<a href="#" data-id="'.$courses->course_id.'" class="btnModalInscription" data-toggle="modal"><i class="fas fa-paperclip"></i></a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::inscriptions.inscriptions'),'action']);
            }//if
            
            // Columna EDITAR
            if( $permits['edit'] ){
                $datatable = $datatable->addColumn('action',function($courses){
                    return '<a href="#" data-id="'.$courses->course_id.'" class="btn btn-info btn-sm btnModalEdit" data-toggle="modal"><i class="fas fa-pencil-alt"></i></a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.edit'),'action']);
            } // if

            // Columna ELIMINAR
            if($permits['delete'] ){
                $datatable = $datatable->addColumn(trans('adminlte::adminlte.delete'),function($courses){
                    return '<a href="" class="btn btn-danger btn-sm confirmDelete" data-id="'.$courses->course_id.'" data-toggle="modal" data-target="#confirmDelete" ><i class="fas fa-trash"></i></a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.delete'),'action']);
            } // if

            // Se formatean contenidos de columnas
            $datatable->editColumn('name',function($courses){
                return $courses->name.($courses->last_name ? ' '.$courses->last_name : '');
            });
            
            // Se formatean contenidos de columnas
            $datatable->editColumn('frequency',function($courses){
               if($courses->array_frequency()){
                   $html = '';
                   foreach($courses->array_frequency() as $day){
                       $html .= '<span class="badge badge-success">'.$day.'</span>';
                   }//foreach
               }//if
                return $html;
            })->escapeColumns([]);
                    
            // Se formatean contenidos de columnas
            $datatable->editColumn('start_time',function($courses){
                $start_time = $courses->start_time ? \Carbon\Carbon::parse($courses->start_time)->format('g:iA') : NULL;
                $end_time = $courses->end_time ? \Carbon\Carbon::parse($courses->end_time)->format('g:iA') : NULL;
                return $start_time.' - '.$end_time;
            });
            
            // Se formatean contenidos de columnas
            $datatable->editColumn('status',function($courses){
                return $courses->status ? '<span class="badge badge-success">'.trans('adminlte::adminlte.active').'</span>' : '<span class="badge badge-danger">'.trans('adminlte::adminlte.inactive').'</span>';
            })->escapeColumns([]);
            
            $datatable = $datatable->make(true);

            return $datatable;
        }

        $htmlBuilder->parameters([
            'order' => [],
            'ordering' =>false,
            'paging' => true,
            'searching' => true,
            'info' => true,
            'searchDelay' => 350,
            'language' => [
                'url' => url('vendor/datatables/Spanish.json')
            ],
            'buttons'      => ['print', 'excel'],        
        ]);

        $html = $htmlBuilder
            ->addColumn(['data' => 'code' , 'name' => 'code','title'=>trans('adminlte::courses.code') ])
            ->addColumn(['data' => 'service' , 'name' => 'service','title'=>trans('adminlte::courses.class') ])
            ->addColumn(['data' => 'name' , 'name' => 'name','title'=>trans('adminlte::instructors.instructor') ])
            ->addColumn(['data' => 'frequency' , 'name' => 'frequency','title'=>trans('adminlte::courses.frequency') ])
            ->addColumn(['data' => 'start_time' , 'name' => 'start_time','title'=>trans('adminlte::courses.schedule') ])
            ->addColumn(['data' => 'status' , 'name' => 'status','title'=>trans('adminlte::courses.status') ]);
        
        // Columna Inscripciones
        if($permits_inscriptions['list'] || $permits_inscriptions['list'] || $permits_inscriptions['list'] || $permits_inscriptions['list']){
            $html = $html->addColumn(['data' => trans('adminlte::inscriptions.inscriptions'), 'name' => trans('adminlte::inscriptions.inscriptions'),'title'=> '<i class="fas fa-users"></i>','class' => 'center' ]);    
        }//if
                    
        if( $permits['edit'] ){
            $html = $html->addColumn(['data' => 'action', 'name' => 'action','title'=> trans('adminlte::adminlte.edit'),'class' => 'center' ]);
        } // if

        if($permits['delete'] ){
            $html = $html->addColumn(['data' => trans('adminlte::adminlte.delete'), 'name' => trans('adminlte::adminlte.delete'),'title'=> trans('adminlte::adminlte.delete') ]);
        } // if

        return  view('admin.courses.index',compact('permits','html'));
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
        if(!auth()->user()->hasPermissionTo('create.course')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
        return response()->json(['status'=>1,'html'=>view('admin.courses.create')->render()]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){ 

        // Permisos
        if(!auth()->user()->hasPermissionTo('create.course')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        // Validar datos
        $validator = Validator::make($request->all(),[
            'code' => 'required|string|max:20|unique:courses',
            'class' => 'required|numeric',
            'instructor' => 'required|numeric',            
            'startTime' => 'required|string|max:8',            
            'status' => 'required|numeric',
            'frequency' => 'required',
            'observations' => 'nullable|string|max:1000',
        ],[            
            'code.required' => 'El campo Código es requerido.',
            'code.max' =>'El código no puede contener mas de :max caracteres.',
            'code.unique' => 'El código ya se encuentra registrado.',
            'class.required' => 'El campo clase es requerido.',  
            'class.numeric' => 'El campo clase debe ser numerico.',            
            'instructor.required' => 'El campo instructor es requerido.',  
            'instructor.numeric' => 'El campo instructor debe ser numerico.',
            'startTime.required' => 'El campo hora de inicio es requerido.',  
            'startTime.max' => 'El campo hora de inicio no puede contener mas de :max caracteres.',           
            'status.required' => 'El campo estado es requerido.',  
            'status.numeric' => 'El campo estado debe ser numerico.',
            'frequency.required' => 'El campo frecuencia es requerido.',  
            'description.max' => 'El campo observaciones no puede contener mas de :max caracteres.', 
        ]);

        // check validation
        if ($validator->fails()) {
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }
 
        try{
            //Creating Course
            $course = new Course();                    
            $course->code = $request->code;
            $course->service_id = $request->class;            
            $course->user_id = $request->instructor;  
            if(auth()->user()->hasPermissionTo('payment.course') && $request->payment_instructor != ''){
                $course->payment_instructor = $request->payment_instructor;
            }//if
            $course->frequency = $request->frequency;
            $course->start_time = \Carbon\Carbon::parse($request->startTime)->format('G:i').':00';            
            $course->end_time = $this->get_end_time($request->startTime,'G:i',$request->class).':00';
            $course->observations = $request->observations;
            $course->status = $request->status;
            $course->save();
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::courses.course_successfully_created');

        }catch(QueryException $e){
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//store
    
    public function edit(Request $request){
        
        if(!auth()->user()->hasPermissionTo('edit.course')){
            Session::flash('error',trans('adminlte::adminlte.app_msj_not_permissions'));
            return redirect()->route('admin');
        } 
        
        $course = Course::find($request->_id);
        return view('admin.courses.edit',compact('course'))->render();

    } // edit
    
    public function update(Request $request){
        
        // Permisos
        if(!auth()->user()->hasPermissionTo('edit.course')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        $course = Course::find($request->_id);
        
        // Validar datos
        $validator = Validator::make($request->all(),[
            'code' => 'required|string|max:20|unique:courses,code,'.$course->course_id.',course_id',
            'class' => 'required|numeric',
            'instructor' => 'required|numeric',            
            'startTime' => 'required|string|max:8',            
            'status' => 'required|numeric',
            'frequency' => 'required',
            'observations' => 'nullable|string|max:1000',
        ],[            
            'code.required' => 'El campo Código es requerido.',
            'code.max' =>'El código no puede contener mas de :max caracteres.',
            'code.unique' => 'El código ya se encuentra registrado.',
            'class.required' => 'El campo clase es requerido.',  
            'class.numeric' => 'El campo clase debe ser numerico.',            
            'instructor.required' => 'El campo instructor es requerido.',  
            'instructor.numeric' => 'El campo instructor debe ser numerico.',
            'startTime.required' => 'El campo hora de inicio es requerido.',  
            'startTime.max' => 'El campo hora de inicio no puede contener mas de :max caracteres.',           
            'status.required' => 'El campo estado es requerido.',  
            'status.numeric' => 'El campo estado debe ser numerico.',
            'frequency.required' => 'El campo frecuencia es requerido.',  
            'description.max' => 'El campo observaciones no puede contener mas de :max caracteres.', 
        ]);

        // check validation
        if ($validator->fails()) {         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

 
        try{
            //Updating Course                                      
            $course->code = $request->code;
            $course->service_id = $request->class;            
            $course->user_id = $request->instructor;  
            if(auth()->user()->hasPermissionTo('payment.course') && $request->payment_instructor != ''){
                $course->payment_instructor = $request->payment_instructor;
            }//if
            $course->frequency = $request->frequency;
            $course->start_time = \Carbon\Carbon::parse($request->startTime)->format('G:i').':00';            
            $course->end_time = $this->get_end_time($request->startTime,'G:i',$request->class).':00';
            $course->observations = $request->observations;
            $course->status = $request->status;
            $course->save();
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::courses.course_successfully_updated');

        }catch(QueryException $e){
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//update
    
    public function destroy(Request $request){
        // Permisos
        if(!auth()->user()->hasPermissionTo('delete.course')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }

        $course = Course::find($request->id)->delete();
        
        return response()->json([
            'status'=> 1,
            'type' => 'success',
            'msg' =>  trans('adminlte::courses.course_successfully_delete')
        ]); 
        
    }//destroy
    
    public function update_end_time(Request $request){                
        $service = ($request->_service ? $request->_service: NULL);
        $time = $this->get_end_time($request->_date,'g:i A',$service);
        return response()->json(['end_time'=>$time]);        
    }//update_end_time
    
    public function get_end_time($time,$format='G:i',$service_id=NULL){  
        $duration = 0;        
        if($service_id){
            $service = Service::find($service_id);
            if($service){
                $duration = $service->session_duration;
            }//if
        }//if 
        $date = \Carbon\Carbon::parse($time);
        if($duration){
            $date->addMinute($duration);
        }//if
        return $date->format($format);          
    }//update_end_time
}
