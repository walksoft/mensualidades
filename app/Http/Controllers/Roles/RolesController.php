<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable; 

class RolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder){ 
      
        if(!auth()->user()->hasPermissionTo('list.roles') && 
           !auth()->user()->hasPermissionTo('create.roles') &&
           !auth()->user()->hasPermissionTo('edit.roles') &&
           !auth()->user()->hasPermissionTo('delete.roles')){            
          Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
          return redirect()->route('admin');
        }

        $permits = [];
        $permits['create'] = auth()->user()->hasPermissionTo('create.roles');
        $permits['edit']   = auth()->user()->hasPermissionTo('edit.roles');
        $permits['delete'] = auth()->user()->hasPermissionTo('delete.roles');


        if ($request->ajax()) {

            $roles = Role::where('is_admin',1)->orderBy('created_at','DESC')->get(); 

            $datatable = Datatables::of($roles);

            // Columna EDITAR
            if( $permits['edit'] ){
                $datatable = $datatable->addColumn( 'action', function ($roles) {
                    return '<a href="#" data-id="'.$roles->id.'" class="btn btn-info btn-sm btnModalEdit" data-toggle="modal"><i class="fas fa-pencil-alt"></i> Editar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.edit'), 'action']);
            } // if

            // Columna ELIMINAR
            if($permits['delete'] ){
                $datatable = $datatable->addColumn( trans('adminlte::adminlte.delete'), function ($roles) {
                    return '<a href="" class="btn btn-danger btn-sm confirmDelete" data-id="'.$roles->id.'" data-toggle="modal" data-target="#confirmDelete" ><i class="fas fa-trash"></i> Eliminar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.delete'), 'action']);
            } // if

            $datatable->editColumn('created_at', function( $roles ) {
                return \Carbon\Carbon::parse($roles->created_at)->format('d/m/Y');
            });

            $datatable = $datatable->make(true);

            return $datatable;
        }

        $htmlBuilder->parameters([
            'paging' => true,
            'searching' => true,
            'info' => true,
            'searchDelay' => 350,
            'language' => [
                'url' => url('vendor/datatables/Spanish.json')
            ],
            'buttons'      => ['print', 'excel'],
        
        ]);

        $html = $htmlBuilder
            ->addColumn(['data' => 'name' , 'name' => 'name','title'=>trans('adminlte::adminlte.name') ])
            ->addColumn(['data' => 'description', 'name' => 'description','title'=>trans('adminlte::adminlte.description')])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at','title'=> trans('adminlte::adminlte.creationDate') ]);
        
        

        if( $permits['edit'] ){
            $html = $html->addColumn(['data' => 'action', 'name' => 'action','title'=> trans('adminlte::adminlte.edit'),'class' => 'center' ]);
        } // if

        if($permits['delete'] ){
            $html = $html->addColumn(['data' => trans('adminlte::adminlte.delete'), 'name' => trans('adminlte::adminlte.delete'),'title'=> trans('adminlte::adminlte.delete'), 'class' => 'center'  ]);
        } // if


        return  view('admin.roles.index',compact('permits','html'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
        if(!auth()->user()->hasPermissionTo('create.roles')){
          Session::flash('error',trans('adminlte::adminlte.app_msj_not_permissions'));
          return redirect()->route('admin');
        }
        return view('admin.roles.create')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){ 
      
        // Permisos
        if(!auth()->user()->hasPermissionTo('create.roles')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        // Validar datos
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:roles',
        ],[
               
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre del rol no puede ser mayor a :max caracteres.',
            'name.unique' => 'El campo nombre ya ha sido tomado.',
  
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

 
        try{
            //Creating Rol
            $Role = Role::create([
                'name' => $request->name,
                'slug' => strtolower(str_replace(' ','.',$request->name)),
                'description' => $request->description,
                'is_admin' => 1,
            ]);
              
            if( !empty($request->permits) && count($request->permits)>0){
                set_time_limit(0);
                $Permission = Permission::whereIn('slug', $request->permits)->get();
                foreach($Permission as $info){
 
                    $Permission = PermissionRole::create([
                        'permission_id' => $info->id,
                        'role_id' =>  $Role->id,
                    ]);
                    
                }//foreach
                                               
            }//if
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::roles.role_successfully_created');

        }catch(QueryException $e){
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//store


    public function edit(Request $request){
        
        if(!auth()->user()->hasPermissionTo('edit.roles')){
            Session::flash('error',trans('adminlte::adminlte.app_msj_not_permissions'));
            return redirect()->route('admin');
        } 
        
        $role = Role::find($request->_id);
        $Permission=Permission::select(['permissions.slug'])->join('permission_role','permission_role.permission_id','=','permissions.id')->join('roles','roles.id','=','permission_role.role_id')->where('roles.id',$role->id)->pluck('slug')->toArray();
        return view('admin.roles.edit',compact('role','Permission'))->render();

    } // edit


    public function update(Request $request){
       
        $id = $request->id;

        // Permisos
        if(!auth()->user()->hasPermissionTo('edit.roles')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        // Validar datos
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:roles,name,'.$id,
        ],[
               
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre del rol no puede ser mayor a :max caracteres.',
            'name.unique' => 'El campo nombre ya ha sido tomado.',
  
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

  
          try{              
                $permits = array_filter($request->permits);
                $permissions = ((!empty($permits) && count($permits)>0) ? $permits : []);
                $role = Role::find($id); 

                $role->name = $request->name;
                $role->slug = strtolower(str_replace(' ','.',$request->name));                       
                $role->description = $request->description;          
                $role->save();
                $role->syncPermissions($permissions);
                                
                $status = 1;
                $type = 'success';
                $msg = trans('adminlte::roles.role_successfully_updated');
              
          }catch(QueryException $e){
               
             $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
          }//cath
          
          return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
          ]);       

    } // update

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // Permisos
        if(!auth()->user()->hasPermissionTo('delete.roles')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }

        $role = Role::where('id',$request->id)->delete();
        
        return response()->json([
            'status'=> 1,
            'type' => 'success',
            'msg' =>  trans('adminlte::roles.role_successfully_delete')
          ]);   
    }
    
}
