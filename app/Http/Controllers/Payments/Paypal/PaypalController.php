<?php

namespace App\Http\Controllers\Payments\Paypal;

use App\Http\Controllers\Controller;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Illuminate\Http\Request;
use App\User;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;

class PaypalController extends Controller
{
    private $_api_context;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $paypal_credential = get_parameter(['config_id_paypal','config_secret_paypal','config_sandbox_paypal']);        
        $paypal_conf = \Config::get('paypal');
        $paypal_conf['settings']['mode'] = $paypal_credential->config_sandbox_paypal['value'] ? 'sandbox' : 'live';
        $this->_api_context = new ApiContext(
            new OAuthTokenCredential(
                $paypal_credential->config_id_paypal['value'],
                $paypal_credential->config_secret_paypal['value']
            ));
        $this->_api_context->setConfig($paypal_conf['settings']);
        
    }//construct
    
    public function paypal_payment(Request $request){

        $payment = \App\Models\Payment::findOrFail($request->payment);
        
        if($payment->status_id != 1){
            Session::flash('warning',trans('adminlte::adminlte.app_msj_not_state_payment'));          
            return redirect()->route('admin');    
        }//if
                
        if($payment->course && $payment->course->code && $payment->course->service && $payment->course->service->name){
            $item_name = $payment->course->code.' '.$payment->course->service->name;
        }//if
        else{
            $item_name = 'Pago '.get_parameter('config_name');
        }//else
        
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');            
        
        $item = new Item();        
        $item->setName($item_name)->setCurrency('MXN')->setQuantity(1)->setPrice($payment->amount);
        
        $item_list = new ItemList();
        $item_list->setItems([$item]);
        
        $amount = new Amount();
        $amount->setCurrency('MXN')->setTotal($payment->amount);
        
        $transaction = new Transaction();
        $transaction->setAmount($amount)->setItemList($item_list)->setDescription($payment->concept->name)->setInvoiceNumber((string)$payment->reference);
        
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(\URL::route('students.paypal_payment_status'))->setCancelUrl(\URL::route('students.paypal_payment_status'));
        
        $payment_paypal = new Payment();
        $payment_paypal->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)->setTransactions([$transaction]);
        
        try{
            $payment_paypal->create($this->_api_context);
        }//try
        catch(\PayPal\Exception\PPConnectionException $ex){
            if(\Config::get('paypal.debug')){
                Session::flash('warning','Exception: '.$ex->getMessage());
                return redirect()->route('admin');
            }//if
            else{
                Session::flash('warning',trans('adminlte::adminlte.error_paypal_connection'));          
                return redirect()->route('admin');
            }//else
        }//catch
           
        foreach($payment_paypal->getLinks() as $link){
            if($link->getRel() == 'approval_url'){
                $redirect_url = $link->getHref();
                break;
            }//if
        }//foreach
        
        //agregar ID de pago a la sesión
        Session::put('paypal_payment_id',$payment_paypal->getId());

        if(isset($redirect_url)){            
            //redirigir a paypal
            return redirect()->away($redirect_url);
        }//if

        Session::flash('warning',trans('adminlte::adminlte.error_paypal_connection'));
        return redirect()->route('admin');
                                
    }//Paypal_payment
    
    public function paypal_payment_status(Request $request){
      
        //obtener sesion del pago
        $payment_id = Session::get('paypal_payment_id');
        
        //limpiar sesion
        Session::forget('paypal_payment_id');        
        
        $payerId = $request->PayerID;
        $token = $request->token;        
        
        if(empty($payerId) || empty($token)){
            Session::flash('warning',trans('adminlte::adminlte.error_paypal'));
            return redirect()->route('admin');
        }//if
        
        $payment_paypal = Payment::get($payment_id,$this->_api_context);            
        $transactions = $payment_paypal->getTransactions();
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);     
        
        $result = $payment_paypal->execute($execution,$this->_api_context);
        
        if($result->getState() == 'approved'){
            $this->update_payment($transactions[0]->getInvoiceNumber(),$transactions[0]->getAmount()->total,$request);
            Session::flash('success',trans('adminlte::adminlte.payment_made_successfully'));
            return redirect()->route('admin');
        }//if
        
        Session::flash('warning',trans('adminlte::adminlte.payment_canceled'));
        return redirect()->route('admin');        
        
    }//paypal_payment_status
    
    public function update_payment($reference,$total,$request){    
        $payment = \App\Models\Payment::where('reference',$reference)->first();
        $payment->status_id = intval(2);
        $payment->payment_date = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        $payment->total_paid = $total;
        $payment->payment_method = 'PayPal';
        $payment->payment_details_received = $request->all();
        $payment->save();
    }//update_payment
    
}