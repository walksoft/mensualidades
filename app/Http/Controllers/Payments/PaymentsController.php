<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Course;
use App\Models\Student_registration;
use App\Models\Service;
use App\Models\Payment;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use DataTables;
use Excel;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable; 

class PaymentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        if(!auth()->user()->hasPermissionTo('cash.register') && !auth()->user()->hasPermissionTo('generate.payment')){
            Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
            return redirect()->route('admin');
        }
        return view('admin.payments.index');
    }//index
    
    public function store(Request $request){
        
        if(!auth()->user()->hasPermissionTo('cash.register')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }//if
        
        try{

            //Validar datos
            $validator = Validator::make($request->all(),[
                'student_cash_register' => 'required|numeric',
                'radio_selected_payment_frm_cash' => 'required|numeric'
            ],[            
                'student_cash_register.required' => 'Debe seleccionar el alumno para realizar el pago.',
                'student_cash_register.numeric' => 'El campo alumno debe ser numerico.',                    
                'radio_selected_payment_frm_cash.required' => 'Debe seleccionar el pago que desea realizar.',  
                'radio_selected_payment_frm_cash.numeric' => 'El id del pago debe ser numerico.',
            ]);

            // check validation
            if($validator->fails()){
                return response()->json(['status'=> 2,'type' => 'error','msg' => $validator->messages()]); 
            }//if
            
            //Creating Payment
            $payment = Payment::find($request->radio_selected_payment_frm_cash);  
            
            //validate status payment
            if($payment->status_id != 1){
                Session::flash('warning',trans('adminlte::adminlte.app_msj_not_state_payment'));          
                return redirect()->route('admin');    
            }//if
        
            $payment->additional = $request->additional_cash_register ? $request->additional_cash_register : 0;
            $payment->discount = $request->discount_cash_register ? $request->discount_cash_register : 0;
            $payment->total_paid = ($payment->amount + $payment->additional) - ($payment->discount);                
            $payment->payment_method = 'Caja';
            $payment->observations = $request->observations_cash_register;
            $payment->status_id = intval(2);   
            $payment->payment_date = \Carbon\Carbon::now()->format('Y-m-d H:i:s');            
            if($payment->save()){
                $user = User::find($payment->user_id);
                \Notification::send($user,new \App\Notifications\Payment_notification($user,$payment,1));
            }//if

            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::payments.payment_successfully_created');

        
        }catch(QueryException $e){
            return response()->json(['status'=>0,'type' =>'error','msg'=>'error: '.$e]);
        }//cath
            
        return response()->json(['status'=>$status,'type' => $type,'msg'=>$msg,'id'=>$payment->user_id]);  
                
    }//store
    
    public function get_courses_student(Request $request){

        $tbl_cash_register = '';
        $tbl_record_cash_register = '';
        $count = 1;
        $ind = 1;
        $user_id = $request->_id;
        $ind_new = $request->_ind_new;
        
        $payments = Payment::where('user_id',$user_id)->where('status_id',1)->orderBy('created_at','DESC')->get();
        $tbl_cash_register = view('admin.payments.table_payments',compact('payments','count','ind','ind_new'))->render();
            
        $ind = 2;
        $payments = Payment::where('user_id',$user_id)->where('status_id','<>',1)->orderBy('payment_date','DESC')->get();
        $tbl_record_cash_register = view('admin.payments.table_payments',compact('payments','count','ind','ind_new'))->render();
        
        $student = \App\User::find($request->_id);
                
        if($student && $student->type_user == 3){
            $add_new_payment_button = view('admin.payments.add_new_payment_button',compact('user_id'))->render();
        }//if
        else{
            $add_new_payment_button = '';
        }//else
                           
        return response()->json(['student'=>$student,'tbl_cash_register'=>$tbl_cash_register,'tbl_record_cash_register'=>$tbl_record_cash_register,'add_new_payment_button'=>$add_new_payment_button]); 
    }//info_student
    
    public function info_course(Request $request){
        $info = [];       
        $info['course_id'] = NULL;
        $info['code_name'] = NULL;
        $info['instructor'] = NULL;
        $info['calendar'] = NULL;
        $info['clock'] = NULL;        
        $course = Course::find($request->_id);
        $payment = Payment::find($request->_payment);   
        $payment->amount = number_format($payment->amount,0,',','.');        
        if($course){
            $info['course_id'] = $course->course_id;        
            $info['code_name'] = $course->code.' - '.$course->service->name;
            $info['instructor'] = $course->instructor->name();        
            $info['calendar'] = implode(' - ',$course->array_frequency());        
            $info['clock'] = \Carbon\Carbon::parse($course->start_time)->format('g:iA').' - '.\Carbon\Carbon::parse($course->end_time)->format('g:iA');        
        }//if
        return response()->json(['course'=>$info,'payment'=>$payment]);                        
    }//info_course   
    
    public function get_total(Request $request){
        try{
            $payment = Payment::find($request->_id);
            $course_total = (($payment->amount) ? $payment->amount : 0);
            $total = ($course_total + $request->_additional) - ($request->_discount);         
            return response()->json(['status'=>1,'type'=>'success','msg'=>'','total'=>number_format($total,0,',','.')]); 
        }//try
        catch(QueryException $e){
            return response()->json(['status'=>0,'type' =>'error','msg'=>'error: '.$e,'total'=>0]);
        }//cath
    }//get_total
    
    function add_new_payment(){
        return response()->json(['status'=>1,'html'=>view('admin.payments.add_new_payment')->render()]);
    }//add_new_payment
    
    public function get_courses_student_new_payment(Request $request){     
        if($request->_concept && $request->_concept == '1'){
            $courses = Course::join('services','courses.service_id','services.service_id')->where('courses.status',1)->get(['courses.course_id as id',\DB::raw("CONCAT(courses.code,' - ',services.name) as text")])->toArray();
        }//if
        elseif($request->_concept && $request->_concept == '2'){           
            $courses = Student_registration::join('courses','student_registrations.course_id','courses.course_id')->join('services','courses.service_id','services.service_id')->where('courses.status',1)->where('student_registrations.user_id',$request->_id)->get(['courses.course_id as id',\DB::raw("CONCAT(courses.code,' - ',services.name) as text")])->toArray();
        }//elseif
        else{
            $courses = [];
        }//else        
        return response()->json(['courses'=>$courses]); 
    }//info_student
    
    public function info_value_cash_new_payment(Request $request){
        $amount = 0;
        if($request->_concept && $request->_concept == '1'){
            $course = Course::find($request->_course);
            $amount = (($course->service && $course->service->registration_value) ? $course->service->registration_value : 0);
            
        }//if
        elseif($request->_concept && $request->_concept == '2'){           
            $registration = Student_registration::where('user_id',$request->_id)->where('course_id',$request->_course)->first();
            $amount = (($registration->monthly_value) ? $registration->monthly_value : 0);
        }//elseif           
        return response()->json(['amount'=>number_format($amount,0,',','.')]); 
    }//info_value_cash_new_payment
    
    public function store_new_payment(Request $request){                
           
        if(!auth()->user()->hasPermissionTo('generate.payment')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }//if
        
        //Validar datos
        $validator = Validator::make($request->all(),[
            'id' => 'required|numeric',
            'concept' => 'required|numeric',
            'course' => 'required|numeric',
            'month' => 'required|numeric'
        ],[ 
            'id.required' => 'Debe seleccionar el alumno.',
            'id.numeric' => 'El campo alumno debe ser numerico.',                        
            'concept.required' => 'Debe seleccionar el concepto.',
            'concept.numeric' => 'El campo concepto debe ser numerico.',
            'course.required' => 'Debe seleccionar el curso.',  
            'course.numeric' => 'El campo curso debe ser numerico.',
            'month.required' => 'Debe seleccionar el mes.',  
            'month.numeric' => 'El campo mes debe ser numerico.',
        ]);

        // check validation
        if($validator->fails()){
            return response()->json(['status'=> 2,'type' => 'error','msg' => $validator->messages()]); 
        }//if

        $amount = 0;
        if($request->concept && $request->concept == '1'){
            $course = Course::find($request->course);
            $amount = (($course->service && $course->service->registration_value) ? $course->service->registration_value : 0);
            
        }//if
        elseif($request->concept && $request->concept == '2'){           
            $registration = Student_registration::where('user_id',$request->id)->where('course_id',$request->course)->first();
            $amount = (($registration->monthly_value) ? $registration->monthly_value : 0);
        }//elseif 
                              
        //Creating Payment
        $payment = new Payment();                    
        $payment->reference = time();                  
        $payment->concept_id = $request->concept;                                                
        $payment->user_id = $request->id;                    
        $payment->course_id = $request->course;          
        $payment->amount = $amount;   
        $payment->deadline = \Carbon\Carbon::createFromFormat('d/m/Y',(\App\Models\Parameter::where('name','payment_deadline')->first()->value).'/'.$request->month.'/'.date('Y'))->format('Y-m-d');           
        $payment->admin_id = auth()->user()->id;
        $payment->status_id = intval(1);                
        if($payment->save()){
            $user = User::find($payment->user_id);
            \Notification::send($user,new \App\Notifications\Payment_notification($user,$payment,2));
        }//if
        

        $status = 1;
        $type = 'success';
        $msg = trans('adminlte::payments.payment_successfully_generated');
        
        return response()->json(['status'=>$status,'type'=>$type,'msg'=>$msg,'id'=>$request->id,'id_new'=>$payment->id]);  
                
    }//store_new_payment
    
    public function index_payments(Request $request, Builder $htmlBuilder){
        
        if(!auth()->user()->hasPermissionTo('list.payment') && 
           !auth()->user()->hasPermissionTo('create.payment') &&
           !auth()->user()->hasPermissionTo('edit.payment') &&
           !auth()->user()->hasPermissionTo('delete.payment')){            
          Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
          return redirect()->route('admin');
        }        

        $permits = [];       
        $permits['list'] = auth()->user()->hasPermissionTo('list.payment');
        $permits['create'] = auth()->user()->hasPermissionTo('create.payment');
        $permits['edit']   = auth()->user()->hasPermissionTo('edit.payment');
        $permits['delete'] = auth()->user()->hasPermissionTo('delete.payment');
        
                                                
        if ($request->ajax()){

            $payments = Payment::leftJoin('courses','payments.course_id','courses.course_id')
                    ->join('status_payments','payments.status_id','status_payments.id')
                    ->join('payment_concepts','payments.concept_id','payment_concepts.id')
                    ->join('users','payments.user_id','users.id')
                    ->leftJoin('services','courses.service_id','services.service_id');        

            if($request->status){
                $payments->where('payments.status_id',$request->status);
            }//status
            
            if($request->concept){
                $payments->where('payments.concept_id',$request->concept);
            }//concept
            
            //date
            if($request->start_date && $request->end_date){
                $start_date = \Carbon\Carbon::createFromFormat('d-m-Y',$request->start_date)->format('Y-m-d');
                $end_date = \Carbon\Carbon::createFromFormat('d-m-Y',$request->end_date)->format('Y-m-d');
                $payments->whereBetween('payments.deadline',[$start_date,$end_date]);
            }//if
            elseif($request->start_date && !$request->end_date){
                $start_date = \Carbon\Carbon::createFromFormat('d-m-Y',$request->start_date)->format('Y-m-d');
                $payments->whereDate('payments.deadline','>',$start_date);
            }//elseif
            elseif($request->end_date && !$request->start_date){
                $end_date = \Carbon\Carbon::createFromFormat('d-m-Y',$request->end_date)->format('Y-m-d');
                $payments->whereDate('payments.deadline','<',$end_date);
            }//elseif
            
            $payments->orderBy('payments.created_at','DESC');
            
            $payments = $payments->get([
                    'payments.id',
                    \DB::raw("CASE WHEN payments.total_paid IS NULL THEN payments.amount ELSE payments.total_paid END amount"),
                    'payments.reference',
                    'payments.deadline',
                    'status_payments.name as status_id',
                    'payment_concepts.name as concept_id',
                    \DB::raw("CONCAT(courses.code,' - ',services.name) as course"),
                    'users.name',
                    'users.last_name',
                ]);          

            $datatable = Datatables::of($payments);

            // Columna ELIMINAR
            if($permits['delete']){
                $datatable = $datatable->addColumn(trans('adminlte::adminlte.delete'),function($payments){
                    return '<a href="" class="btn btn-danger btn-sm confirmDelete" data-id="'.$payments->id.'" data-toggle="modal" data-target="#confirmDelete" ><i class="fas fa-trash"></i> Eliminar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.delete'),'action']);
            } // if
          
            // Se formatean contenidos de columnas
            $datatable->editColumn('students',function($payments){
                return $payments->name.($payments->last_name ? ' '.$payments->last_name : '');
            });

            $datatable->editColumn('deadline',function($payments){
                $deadline = $payments->deadline ? \Carbon\Carbon::parse($payments->deadline)->format('d/m/Y') : NULL;
                return $deadline;
            });
            
            // Se formatean contenidos de columnas
            $datatable->editColumn('status_id',function($courses){
                return $courses->status_id == 2 ? '<span class="badge badge-success">'.$courses->status_id.'</span>' : '<span class="badge badge-info">'.$courses->status_id.'</span>';
            })->escapeColumns([]);
            
            // Se formatean contenidos de columnas
            $datatable->editColumn('amount',function($courses){
                return '$'.number_format(($courses->amount?$courses->amount:0),0,',','.');
            });
            
            
            $datatable = $datatable->make(true);

            return $datatable;
        }

        $htmlBuilder->parameters([
            'order' => [],
            'ordering' =>false,
            'paging' => true,
            'searching' => true,
            'info' => true,
            'searchDelay' => 350,
            'language' => [
                'url' => url('vendor/datatables/Spanish.json')
            ],
            'buttons'      => ['print', 'excel'],
        
        ]);

        $html = $htmlBuilder
            ->addColumn(['data'=>'reference','name'=>'reference','title'=>trans('adminlte::payments.reference')])
            ->addColumn(['data'=>'status_id','name'=>'status_id','title'=>trans('adminlte::payments.status')])     
            ->addColumn(['data'=>'concept_id','name'=>'concept_id','title'=>trans('adminlte::payments.concept')])
            ->addColumn(['data'=>'amount','name'=>'amount','title'=>trans('adminlte::payments.value')])
            ->addColumn(['data'=>'course','name'=>'course','title'=>trans('adminlte::courses.course')])
            ->addColumn(['data'=>'students','name'=>'students','title'=>trans('adminlte::payments.student_instructor')])
            ->addColumn(['data'=>'deadline','name'=>'deadline','title'=>trans('adminlte::payments.deadline')]);
                        
        if($permits['delete'] ){
            $html = $html->addColumn(['data' => trans('adminlte::adminlte.delete'), 'name' => trans('adminlte::adminlte.delete'),'title'=> trans('adminlte::adminlte.delete') ]);
        } // if

        $html->ajax(['data' => 'function(d){d.concept=$("#concept").val();d.status=$("#status").val();d.start_date=$("#start_date").val();d.end_date=$("#end_date").val();}']);
        
        return  view('admin.payments.all_payments',compact('permits','html'));            
            
    }//index_payments
    
    function export_excel($status=NULL,$concept=NULL,$start_date=NULL,$end_date=NULL){          
        return (new \App\Exports\PaymentExport($status,$concept,$start_date,$end_date))->download('payment.xlsx');
    }//export_excel
}
