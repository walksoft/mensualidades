<?php

namespace App\Http\Controllers\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Parameter;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;


class ConfigController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){ 

        if(!auth()->user()->hasPermissionTo('list.config') && !auth()->user()->hasPermissionTo('edit.config')){
            Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
            return redirect()->route('admin');
        }
        
       $parameter = Parameter::all(); 

       $arrayParameter = [];
       foreach($parameter->toArray() as $key => $value){
            $arrayParameter[$value['name']] = $value;
       }

       $parameter = $arrayParameter;
       return  view('admin.config.index',compact('parameter'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){ 
       
        if(!auth()->user()->hasPermissionTo('edit.config')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
   

        // Validar datos
        $validator = Validator::make($request->all(),[
            'config_name' => 'max:100',
            'config_direction' => 'max:200',
            'config_slogan' => 'max:300',
            'config_email' => 'nullable|email|max:190',
            'config_phone' => 'max:20',
            'config_id_paypal' => 'max:250',
            'config_secret_paypal' => 'max:250',
            
        ],[            
            'config_name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'config_direction.max' => 'El dirección no puede contener mas de :max caracteres.',
            'config_slogan.max' =>'El eslogan no puede contener mas de :max caracteres.',
            'config_email.max' =>'El email no puede contener mas de :max caracteres.',
            'config_phone.max' =>'El teléfono no puede contener mas de :max caracteres.',
            'config_id_paypal.max' =>'El ID cliente PayPal no puede contener mas de :max caracteres.',
            'config_secret_paypal.max' =>'El Secreto cliente PayPal no puede contener mas de :max caracteres.',

        ]);

        //check validation
        if($validator->fails()){         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }
        
        try{

              foreach($request->all() as $key => $value){

                if( $key != '_token' && $key != '_method'){

                    $parameter = Parameter::where('name','=',$key)->first();

                    if( $key == 'config_avatar' ){
               
                        $path = $request->file($key)->store('public');
                        $path = explode('/',$path);
                        $parameter->value=  ($path[1]) ? $path[1]: null;

                    }
                    else{
                        $parameter->value = ($value) ? $value : null;
                        
                    }
                    
                    $parameter->save();

                } // if
              } // foreach


              if( !isset($request->config_sandbox_paypal) ){
                $parameter = Parameter::where('name','=','config_sandbox_paypal')->first();
                $parameter->value = null;
                $parameter->save();
              }

             
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::configurations.config_successfully_updated');

        }catch(QueryException $e){
             $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]); 

     }

     public function avatar($img){    
  
        if($img){
            $avatar = storage_path().'/app/public/'.$img;
        }//if
        else{
            $parameter = asset("media/img/default-avatar.png");
        }//else
        return file_get_contents($avatar);      

     }//avatar

     public function exec_payments_process(){ 
        try{
            $process = new \App\Http\Controllers\Jobs\JobsController();
            $process->monthly_payments_process();
            return response()->json(['status'=>1,'type'=>'success','msg'=>trans('adminlte::configurations.config_successfully_payments_process')]);
        }//try
        catch(\Exception $e){
            return response()->json(['status'=>0,'type' => 'error','msg' => 'error : '.$e->getMessage()]);
        }//cath
     }//exec_payments_process
     
     public function exec_overdue_payments_process(){
        try{
            $process = new \App\Http\Controllers\Jobs\JobsController();
            $process->monthly_pending_reminder_process();
            return response()->json(['status'=>1,'type'=>'success','msg'=>trans('adminlte::configurations.config_successfully_payments_process')]);
        }//try
        catch(\Exception $e){
            return response()->json(['status'=>0,'type' => 'error','msg' => 'error : '.$e->getMessage()]);
        }//cath         
     }//exec_overdue_payments_process
     
     public function exec_instructors_payment_process(Request $request){
        try{            
            if(!$request->month_pay_instructors){
                return response()->json(['status'=>0,'type'=>'error','msg'=>trans('adminlte::configurations.msg_select_month_for_process')]);    
            }//if            
            $process = new \App\Http\Controllers\Jobs\JobsController();
            $process->instructors_payment_process($request->month_pay_instructors);
            return response()->json(['status'=>1,'type'=>'success','msg'=>trans('adminlte::configurations.config_successfully_payments_process')]);
        }//try
        catch(\Exception $e){
            return response()->json(['status'=>0,'type' => 'error','msg' => 'error : '.$e->getMessage()]);
        }//cath           
     }//exec_instructors_payment_process
}
