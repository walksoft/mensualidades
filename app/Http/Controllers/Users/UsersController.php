<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable; 

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder){ 
      
        if(!auth()->user()->hasPermissionTo('list.user') && 
           !auth()->user()->hasPermissionTo('create.user') &&
           !auth()->user()->hasPermissionTo('edit.user') &&
           !auth()->user()->hasPermissionTo('delete.user')){            
          Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
          return redirect()->route('admin');
        }
        
        $permits = [];
        $permits['create'] = auth()->user()->hasPermissionTo('create.user');
        $permits['edit']   = auth()->user()->hasPermissionTo('edit.user');
        $permits['delete'] = auth()->user()->hasPermissionTo('delete.user');
        
        if ($request->ajax()){

            $users = User::where('type_user',1)->orderBy('created_at','DESC')->get();

            $datatable = Datatables::of($users);

            // Columna EDITAR
            if( $permits['edit'] ){
                $datatable = $datatable->addColumn( 'action', function ($users) {
                    return '<a href="#" data-id="'.$users->id.'" class="btn btn-info btn-sm btnModalEdit" data-toggle="modal"><i class="fas fa-pencil-alt"></i> Editar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.edit'), 'action']);
            } // if

            // Columna ELIMINAR
            if($permits['delete'] ){
                $datatable = $datatable->addColumn( trans('adminlte::adminlte.delete'), function ($users) {
                    return '<a href="" class="btn btn-danger btn-sm confirmDelete" data-id="'.$users->id.'" data-toggle="modal" data-target="#confirmDelete" ><i class="fas fa-trash"></i> Eliminar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.delete'), 'action']);
            } // if

            // Se formatean contenidos de columnas
            $datatable->editColumn('created_at', function( $users ) {
                return \Carbon\Carbon::parse($users->created_at)->format('d/m/Y');
            });

            $datatable = $datatable->make(true);

            return $datatable;
        }

        $htmlBuilder->parameters([
            'order' => [],
            'ordering' =>false,
            'paging' => true,
            'searching' => true,
            'info' => true,
            'searchDelay' => 350,
            'language' => [
                'url' => url('vendor/datatables/Spanish.json')
            ],
            'buttons'      => ['print', 'excel'],
        
        ]);

        $html = $htmlBuilder
            ->addColumn(['data' => 'name' , 'name' => 'name','title'=>trans('adminlte::adminlte.name') ])
            ->addColumn(['data' => 'email', 'name' => 'email','title'=>trans('adminlte::adminlte.email') ])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at','title'=> trans('adminlte::adminlte.creationDate') ]);
        
        

        if( $permits['edit'] ){
            $html = $html->addColumn(['data' => 'action', 'name' => 'action','title'=> trans('adminlte::adminlte.edit'),'class' => 'center' ]);
        } // if

        if($permits['delete'] ){
            $html = $html->addColumn(['data' => trans('adminlte::adminlte.delete'), 'name' => trans('adminlte::adminlte.delete'),'title'=> trans('adminlte::adminlte.delete') ]);
        } // if

        return  view('admin.users.index',compact('permits','html'));      
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
        if(!auth()->user()->hasPermissionTo('create.user')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
        return response()->json(['status'=>1,'html'=>view('admin.users.create')->render()]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){ 

        // Permisos
        if(!auth()->user()->hasPermissionTo('create.user')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        // Validar datos
        $validator = Validator::make($request->all(), [
            'roles' => 'required',
            'name' => 'required|string|max:190',
            'email' => 'required|string|email|max:190|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ],[            
            'roles.required' => 'Debe seleccionar al menos un rol.',
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'email.unique' => 'El email ya se encuentra registrado.',  
            'password.required' => 'El campo contraseña es requerido.',
            'password.min' =>'La contraseña debe tener minimo :min caracteres.',
            'password.confirmed' =>'La confirmación de la contraseña no coincide.',
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

 
        try{
            //Creating user
            $user = new User();                    
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->observations = $request->description;
            if($user->save()){
                $user->assignRoles(json_decode($request->roles,true));
            }//if
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::users.user_successfully_created');

        }catch(QueryException $e){
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//store
    
    public function edit(Request $request){
        
        if(!auth()->user()->hasPermissionTo('edit.user')){
            Session::flash('error',trans('adminlte::adminlte.app_msj_not_permissions'));
            return redirect()->route('admin');
        } 
        
        $user = User::find($request->_id);
        
        return view('admin.users.edit',compact('user'))->render();

    } // edit
    
    public function update(Request $request){
        
        // Permisos
        if(!auth()->user()->hasPermissionTo('edit.user')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        $user = User::find($request->_id);
        
        // Validar datos
        $validator = Validator::make($request->all(), [
            'roles' => 'required',
            'name' => 'required|string|max:190',
            'email' => 'required|string|unique:users,email,'.$user->id.'|email|max:190',
            'password' => 'nullable|string|min:8|confirmed',
        ],[            
            'roles.required' => 'Debe seleccionar al menos un rol.',
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'email.unique' => 'El email ya se encuentra registrado.',  
            'password.required' => 'El campo contraseña es requerido.',
            'password.min' =>'La contraseña debe tener minimo :min caracteres.',
            'password.confirmed' =>'La confirmación de la contraseña no coincide.',
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

 
        try{
            //Updating user                   
            $user->name = $request->name;
            $user->email = $request->email;
            if($request->password){
                $user->password = Hash::make($request->password);
            }//if
            $user->observations = $request->description;
            if($user->save()){
                $user->syncRoles(json_decode($request->roles,true));
            }//if
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::users.user_successfully_updated');

        }catch(QueryException $e){
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//update
    
    public function destroy(Request $request){
        // Permisos
        if(!auth()->user()->hasPermissionTo('delete.user')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }

        $user = User::find($request->id)->delete();
        
        return response()->json([
            'status'=> 1,
            'type' => 'success',
            'msg' =>  trans('adminlte::users.user_successfully_delete')
        ]); 
        
    }//destroy'
    
    public function profile(){
        return  view('admin.profile.index');
    }//profile
    
    public function save_profile(Request $request){
        
        // Validar datos
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:190',
            'email' => 'required|string|unique:users,email,'.Auth::user()->id.'|email|max:190',
            'password' => 'nullable|string|min:8|confirmed',
        ],[            
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'email.unique' => 'El email ya se encuentra registrado.',  
            'password.required' => 'El campo contraseña es requerido.',
            'password.min' =>'La contraseña debe tener minimo :min caracteres.',
            'password.confirmed' =>'La confirmación de la contraseña no coincide.',
        ]);

        //check validation
        if($validator->fails()){         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

        $user = User::find(Auth::user()->id);
        
        try{
            //Updating user                   
            $user->name = $request->name;
            $user->email = $request->email;
            if($request->password){
                $user->password = Hash::make($request->password);
            }//if           
            if($request->avatar){
                $user->avatar = $request->file('avatar')->store('pictures');
            }//if
            $user->save();
             
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::users.perfil_successfully_updated');

        }catch(QueryException $e){
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]); 

    }//save_profile
    
    public function avatar($id){    
        $user = User::find($id);
        if($user->avatar){
            $avatar = storage_path().'/app/'.$user->avatar;
        }//if
        else{
            $avatar = asset("media/img/default-avatar.png");
        }//else
        return file_get_contents($avatar);        
    }//avatar
        
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function image_store(Request $request){
        
    	$imageName = $request->file->getClientOriginalName();
        $request->file->move(public_path('upload'), $imageName);
    	return response()->json(['uploaded' => '/upload/'.$imageName]);
    }
    
    public function display_file($id=NULL){
        
        if(!$id){
            abort(404);
        }//if
        
        $file = \App\Models\User_file::findOrFail($id);
        
        $ext = strtolower($file->extension);
        if($ext == 'jpg' || $ext == 'jpeg'){
            $contentType = 'image/jpeg';
        }//if
        elseif($ext == 'png'){
            $contentType = 'image/png';
        }//elseif
        else{
            $contentType = 'application/pdf';
        }//else 
                
        return Response::make(file_get_contents(storage_path().'/app/'.$file->route),200,['Content-Type' => $contentType,'Content-Disposition' => 'inline; filename="'.$file->file_name.'"']); 
        
    }//display_file
    
}
