<?php

namespace App\Http\Controllers\Inscriptions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\User;
use App\Models\Student_registration;
use App\Models\Course;
use App\Models\Payment;
use App\Models\Parameter;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable; 

class InscriptionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){ 
        
        if(!auth()->user()->hasPermissionTo('list.inscription') && 
           !auth()->user()->hasPermissionTo('create.inscription') &&
           !auth()->user()->hasPermissionTo('edit.inscription') &&
           !auth()->user()->hasPermissionTo('delete.inscription')){            
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
        
        $permits = [];
        $permits['create'] = auth()->user()->hasPermissionTo('create.inscription');
        $permits['edit']   = auth()->user()->hasPermissionTo('edit.inscription');
        $permits['delete'] = auth()->user()->hasPermissionTo('delete.inscription');  
        
        $course = \App\Models\Course::find($request->_id);
        
        return response()->json(['status'=>1,'html'=>view('admin.inscriptions.index',compact('permits','course'))->render()]);
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inscribe(Request $request){

        // Permisos
        if(!auth()->user()->hasPermissionTo('create.inscription')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
           
        // Validar datos
        $validator = Validator::make($request->all(),[
            'user_id' => 'required|numeric|unique:student_registrations,user_id,NULL,id,course_id,'.$request->course_id,
            'course_id' => 'required|numeric',
        ],[            
            'user_id.required' => 'Debe seleccionar un estudiante para inscribir.',
            'user_id.unique' => 'El alumno ya se encuentra inscrito en el curso.',
            'course_id.required' => 'Debe seleccionar un curso para inscribir.',   
        ]);

        // check validation
        if ($validator->fails()){
            return response()->json(['status'=> 2,'type' => 'error','msg' => $validator->messages()]); 
        }
 
        try{
            //Course                            
            $course = Course::find($request->course_id);
            
            //Creating inscription
            $registration = new Student_registration();                    
            $registration->course_id = $course->course_id;
            $registration->user_id = $request->user_id;  
            
            if($request->value_monthlyCharge){
                $registration->monthly_value = $request->value_monthlyCharge;
            }//if
            else{
                $registration->monthly_value = $course->service->monthly_value;
            }//else
            
            if($request->value_promptPaymentCollection){
                $registration->timely_payment_value = $request->value_promptPaymentCollection;
            }//if
            else{
                $registration->timely_payment_value = $course->service->timely_payment_value;
            }//else
            
            if($request->value_inscription){
                $registration->registration_value = $request->value_inscription;
            }//if   
            else{
                $registration->registration_value = $course->service->registration_value;
            }//else
            
            $registration->observations = $request->observations;
            $registration->status = 1;
            if($registration->save()){
                $user = User::find($registration->user_id);                
                $payment = $this->create_payment($registration->user_id,$registration->course_id,$registration->registration_value,1);                
                \Notification::send($user,new \App\Notifications\Payment_notification($user,$payment,2));
                
                if($payment){
                    $date_now = \Carbon\Carbon::createFromFormat('d/m/Y 00:00:00',date('d').'/'.date('m').'/'.date('Y').' 00:00:00');
                    $day = Parameter::where('name','payment_deadline')->first()->value;
                    $deadline = \Carbon\Carbon::createFromFormat('d/m/Y 00:00:00',$day.'/'.date('m').'/'.date('Y').' 00:00:00');
                    if($date_now > $deadline){
                        $deadline->addMonth();    
                    }//if
                    $payment = $this->create_payment($registration->user_id,$registration->course_id,$registration->monthly_value,2,$deadline);
                    \Notification::send($user,new \App\Notifications\Payment_notification($user,$payment,2));    
                }//if
            }//if
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::inscriptions.inscription_successfully_created');

        }catch(QueryException $e){
            $status = 0;
            $type = 'error';
            $msg = 'error : '.$e;
        }//cath
        
        $course = \App\Models\Course::find($course->course_id);
        $html = view('admin.inscriptions.table_registration',compact('course'))->render();
        $total = $course->registration->count();
        $students = \App\User::where('status',1)->where('type_user',3)->whereNotIn('id',\App\Models\Student_registration::where('course_id',$course->course_id)->pluck('user_id'))->orderBy('name','ASC')->get(['id',\DB::raw("CASE WHEN last_name IS NULL THEN name ELSE CONCAT(name,' ',last_name) END text")])->toArray();       
        return response()->json(['status'=>$status,'type'=>$type,'msg'=>$msg,'html'=>$html,'total'=>$total,'students'=>$students]);  
    }
       
    public function destroy(Request $request){
        // Permisos
        if(!auth()->user()->hasPermissionTo('delete.inscription')){
            return response()->json(['status'=> 3,'type' => 'error','msg' => trans('adminlte::adminlte.app_msj_not_permissions')]);
        }//if
        $registration = Student_registration::find($request->id);        
        $course = \App\Models\Course::find($registration->course_id);
        $registration->delete();
        $html = view('admin.inscriptions.table_registration',compact('course'))->render();
        $total = $course->registration->count();
        $students = \App\User::where('status',1)->where('type_user',3)->whereNotIn('id',\App\Models\Student_registration::where('course_id',$course->course_id)->pluck('user_id'))->orderBy('name','ASC')->get(['id',\DB::raw("CASE WHEN last_name IS NULL THEN name ELSE CONCAT(name,' ',last_name) END text")])->toArray();
        return response()->json(['status'=> 1,'type'=>'success','msg'=>trans('adminlte::courses.course_successfully_delete'),'html'=>$html,'total'=>$total,'students'=>$students]);        
    }//destroy
    
    public function table_registration(Request $request){
        $course = \App\Models\Course::find($request->course_id);
        return response()->json(['html'=>view('admin.inscriptions.table_registration',compact('course'))->render(),'total'=>$course->registration->count()]);
    }//table_registration
    
    public function info_student(Request $request){
        $courses = Student_registration::join('courses','student_registrations.course_id','courses.course_id')->join('services','courses.service_id','services.service_id')->where('student_registrations.user_id',$request->_id)->get(['courses.course_id as id',\DB::raw("CONCAT(courses.code,' - ',services.name) as text")])->toArray();
        if(count($courses) > 1){
            array_unshift($courses,['id'=>'all','text'=>'--Todas--']);
        }//if
        return response()->json(['student'=>\App\User::find($request->_id),'courses'=>$courses]); 
    }//info_student
    
    public function create_payment($user_id,$course_id,$amount,$concept,$deadline=NULL){
        
        //Creating Payment
        $payment = new Payment();                    
        $payment->reference = time();                  
        $payment->concept_id = $concept;                                                
        $payment->user_id = $user_id;                    
        $payment->course_id = $course_id;          
        $payment->amount = $amount;   
        $payment->status_id = intval(1);      
        if($deadline){
            $payment->deadline = $deadline->format('Y-m-d 00:00:00');    
        }//if
        $payment->save();
        
        return $payment;

    }//create_payment
}
