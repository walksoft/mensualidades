<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\User;
use App\Models\User_file;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable; 

class InstructorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder){ 
        
        if(!auth()->user()->hasPermissionTo('list.instructor') && 
           !auth()->user()->hasPermissionTo('create.instructor') &&
           !auth()->user()->hasPermissionTo('edit.instructor') &&
           !auth()->user()->hasPermissionTo('delete.instructor')){            
          Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
          return redirect()->route('admin');
        }
        
        $permits = [];
        $permits['create'] = auth()->user()->hasPermissionTo('create.instructor');
        $permits['edit']   = auth()->user()->hasPermissionTo('edit.instructor');
        $permits['delete'] = auth()->user()->hasPermissionTo('delete.instructor');
        
        if ($request->ajax()){

            $users = User::join('courses','courses.user_id','users.id')->where('type_user',2)->where('courses.status',1)->groupBy(['users.id','users.name','users.last_name','users.email','users.phone_number','users.status'])->orderBy('users.created_at','DESC')->get(['users.id','users.name','users.last_name','users.email','users.phone_number','users.status',\DB::raw('count(courses.course_id) as courses'),\DB::raw('sum(courses.payment_instructor) as payment'),\DB::raw('(select max(payment_date) from payments where payments.user_id = users.id) as last_payment')]);
            
            $datatable = Datatables::of($users);

            // Columna EDITAR
            if( $permits['edit'] ){
                $datatable = $datatable->addColumn( 'action', function ($users) {
                    return '<a href="#" data-id="'.$users->id.'" class="btn btn-info btn-sm btnModalEdit" data-toggle="modal"><i class="fas fa-pencil-alt"></i></a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.edit'), 'action']);
            } // if

            // Columna ELIMINAR
            if($permits['delete'] ){
                $datatable = $datatable->addColumn( trans('adminlte::adminlte.delete'), function ($users) {
                    return '<a href="" class="btn btn-danger btn-sm confirmDelete" data-id="'.$users->id.'" data-toggle="modal" data-target="#confirmDelete" ><i class="fas fa-trash"></i></a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.delete'), 'action']);
            } // if

            // Se formatean contenidos de columnas
            $datatable->editColumn('name', function( $users ){
                return $users->name.($users->last_name ? ' '.$users->last_name : '');
            });

            // Se formatean contenidos de columnas
            $datatable->editColumn('courses',function($users){
                return '<span class="badge badge-info">'.$users->courses.'</span>';
            })->escapeColumns([]);
            
            // Se formatean contenidos de columnas
            $datatable->editColumn('payment',function($users){
                return '$'.number_format($users->payment,0,',','.');
            })->escapeColumns([]);
            
            // Se formatean contenidos de columnas
            $datatable->editColumn('last_payment',function($users){
                return $users->last_payment ? \Carbon\Carbon::parse($users->last_payment)->format('d/m/Y') : NULL;
            });
            
            // Se formatean contenidos de columnas
            $datatable->editColumn('status',function($users){
                return $users->status ? '<span class="badge badge-success">'.trans('adminlte::adminlte.active').'</span>' : '<span class="badge badge-info">'.trans('adminlte::adminlte.inactive').'</span>';
            })->escapeColumns([]);
         
            $datatable = $datatable->make(true);

            return $datatable;
        }

        $htmlBuilder->parameters([
            'order' => [],
            'ordering' =>false,
            'paging' => true,
            'searching' => true,
            'info' => true,
            'searchDelay' => 350,
            'language' => [
                'url' => url('vendor/datatables/Spanish.json')
            ],
            'buttons'      => ['print', 'excel'],
        
        ]);

        $html = $htmlBuilder
            ->addColumn(['data' => 'name' , 'name' => 'name','title'=>trans('adminlte::adminlte.name') ])
            ->addColumn(['data' => 'email', 'name' => 'email','title'=>trans('adminlte::adminlte.email') ])
            ->addColumn(['data' => 'phone_number', 'name' => 'phone_number','title'=>trans('adminlte::adminlte.phone') ])                
            ->addColumn(['data' => 'courses', 'name' => 'courses','title'=>trans('adminlte::courses.courses'),'class' => 'center'])
            ->addColumn(['data' => 'payment', 'name' => 'payment','title'=>trans('adminlte::courses.payment'),'class' => 'center'])
            ->addColumn(['data' => 'last_payment', 'name' => 'last_payment','title'=>trans('adminlte::courses.lastPayment'),'class' => 'center'])
            ->addColumn(['data' => 'status', 'name' => 'status','title'=>trans('adminlte::adminlte.status') ]);
        
        

        if( $permits['edit'] ){
            $html = $html->addColumn(['data' => 'action', 'name' => 'action','title'=> trans('adminlte::adminlte.edit'),'class' => 'center' ]);
        } // if

        if($permits['delete'] ){
            $html = $html->addColumn(['data' => trans('adminlte::adminlte.delete'), 'name' => trans('adminlte::adminlte.delete'),'title'=> trans('adminlte::adminlte.delete') ]);
        } // if

        return  view('admin.instructors.index',compact('permits','html'));
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
        if(!auth()->user()->hasPermissionTo('create.instructor')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
        return response()->json(['status'=>1,'html'=>view('admin.instructors.create')->render()]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){ 

        // Permisos
        if(!auth()->user()->hasPermissionTo('create.instructor')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        // Validar datos
        $validator = Validator::make($request->all(),[
            'name' => 'required|string||max:190',
            'lastName' => 'nullable|string||max:190',
            'email' => 'required|string|email|max:190|unique:users',
            'phone' => 'nullable|string||max:190',
            'address' => 'nullable|string||max:190',
            'status' => 'required',
            'observations' => 'nullable|string|max:5000',
        ],[            
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'lastName.required' => 'El campo Apellido es requerido.',
            'lastName.max' =>'El Apellido no puede contener mas de :max caracteres.',
            'email.required' => 'El campo email es requerido', 
            'email.unique' => 'El email ya se encuentra registrado.',  
            'phone.required' => 'El campo Teléfono es requerido.',  
            'phone.max' => 'El Teléfono no puede contener mas de :max caracteres.',
            'address.max' => 'El Dirección no puede contener mas de :max caracteres.',
            'state.required' =>'El campo estado es requerido.',
            'observations.max' => 'Las Observaciones no pueden contener mas de :max caracteres.',
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }
 
        try{
            
            //init transactions
            \DB::beginTransaction();
            
            //Creating Instructor
            $instructor = new User();                    
            $instructor->name = $request->name;
            $instructor->last_name = $request->lastName;
            $instructor->email = $request->email;            
            $instructor->phone_number = $request->phone;            
            $instructor->address = $request->address;            
            $instructor->type_user = 2;//instructor
            $instructor->status = $request->status;
            $instructor->observations = $request->observations;
            if($instructor->save()){
                $instructor->assignRoles('instructor1000');
            }//if            
            
            if($request->anexo){
                $user_file = new User_file();
                $user_file->user_id = $instructor->id; 
                $user_file->file_id = 3;//anexo
                $user_file->file_name = $request->file('anexo')->getClientOriginalName();                           
                $user_file->route = $request->file('anexo')->store('files');
                $user_file->extension = $request->file('anexo')->getClientOriginalExtension();
                $user_file->file_size = $request->file('anexo')->getSize();
                $user_file->save();
            }//if
            
            //commit
            \DB::commit();
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::instructors.instructor_successfully_created');

        }catch(QueryException $e){
            //rolback transaction
            \DB::rollBack();
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//store
    
    public function edit(Request $request){
        
        if(!auth()->user()->hasPermissionTo('edit.instructor')){
            Session::flash('error',trans('adminlte::adminlte.app_msj_not_permissions'));
            return redirect()->route('admin');
        } 
        
        $files = [];
        $user = User::find($request->_id);
        
        if(count($user->user_files) > 0){
            foreach($user->user_files as $user_files){
                $files[$user_files->file_id]['id'] = $user_files->id;
                $files[$user_files->file_id]['file_url'] = route('display_file',['id'=>$user_files->id]);
                $files[$user_files->file_id]['file_id'] = $user_files->file_id;
                $files[$user_files->file_id]['file_name'] = $user_files->file_name;
                $files[$user_files->file_id]['route'] = $user_files->route;
                $files[$user_files->file_id]['extension'] = $user_files->extension;
                $files[$user_files->file_id]['file_size'] = $user_files->file_size;                                
            }//foreach
        }//if
        
        return view('admin.instructors.edit',compact('user','files'))->render();

    } // edit
    
    public function update(Request $request){
        
        // Permisos
        if(!auth()->user()->hasPermissionTo('edit.instructor')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        $id = $request->id;       
        
        //Validar datos
        $validator = Validator::make($request->all(),[
            'name' => 'required|string||max:190',
            'lastName' => 'nullable|string||max:190',
            'email' => 'required|string|email|max:190|unique:users,email,'.$id,
            'phone' => 'nullable|string||max:190',
            'address' => 'nullable|string||max:190',
            'status' => 'required',
            'observations' => 'nullable|string|max:5000',
        ],[            
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'lastName.required' => 'El campo Apellido es requerido.',
            'lastName.max' =>'El Apellido no puede contener mas de :max caracteres.',
            'email.required' => 'El campo email es requerido', 
            'email.unique' => 'El email ya se encuentra registrado.',  
            'phone.required' => 'El campo Teléfono es requerido.',  
            'phone.max' => 'El Teléfono no puede contener mas de :max caracteres.',
            'address.max' => 'El Dirección no puede contener mas de :max caracteres.',
            'state.required' =>'El campo estado es requerido.',
            'observations.max' => 'Las Observaciones no pueden contener mas de :max caracteres.',
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

 
        try{
            
            //init transactions
            \DB::beginTransaction();
            
            //Updating Instructor
            $instructor = User::find($id);
            $instructor->name = $request->name;
            $instructor->last_name = $request->lastName;
            $instructor->email = $request->email;            
            $instructor->phone_number = $request->phone;            
            $instructor->address = $request->address;            
            $instructor->status = $request->status;
            $instructor->observations = $request->observations;
            $instructor->save();
            
            $anexo_instructor = User_file::where('user_id',$instructor->id)->where('file_id',3)->first();
            if($request->anexo_instructor && $anexo_instructor){
                $anexo_instructor->file_name = $request->file('anexo_instructor')->getClientOriginalName();                           
                $anexo_instructor->route = $request->file('anexo_instructor')->store('files');
                $anexo_instructor->extension = $request->file('anexo_instructor')->getClientOriginalExtension();
                $anexo_instructor->file_size = $request->file('anexo_instructor')->getSize();
                $anexo_instructor->save();                
            }//if
            else if($request->anexo_instructor){
                $user_file = new User_file();
                $user_file->user_id = $instructor->id; 
                $user_file->file_id = 3;//anexo
                $user_file->file_name = $request->file('anexo_instructor')->getClientOriginalName();                           
                $user_file->route = $request->file('anexo_instructor')->store('files');
                $user_file->extension = $request->file('anexo_instructor')->getClientOriginalExtension();
                $user_file->file_size = $request->file('anexo_instructor')->getSize();
                $user_file->save();
            }//elseif
            //
            //commit
            \DB::commit();
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::instructors.instructor_successfully_updated');

        }catch(QueryException $e){
            //rolback transaction
            \DB::rollBack();
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//update
    
    public function destroy(Request $request){
        // Permisos
        if(!auth()->user()->hasPermissionTo('delete.instructor')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }

        $User = User::find($request->id)->delete();
        
        return response()->json([
            'status'=> 1,
            'type' => 'success',
            'msg' =>  trans('adminlte::instructors.instructor_successfully_delete')
        ]); 
        
    }//destroy
    
}
