<?php

namespace App\Http\Controllers\Notifications;

use App\Http\Controllers\Controller;
use App\Models\Parameter;
use App\Models\Payment;
use Illuminate\Database\QueryException;

class NotificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){ 
        
       $parameter = Parameter::all(); 

       $arrayParameter = [];
       foreach($parameter->toArray() as $key => $value){
            $arrayParameter[$value['name']] = $value;
       }

       $parameter = $arrayParameter;
       return  view('admin.config.index',compact('parameter'));
    }
    
    public static function process_keywords(Payment $payment,String $text){
       
        $keywords = ['[PAGO_CONCEPTO]','[PAGO_VALOR]','[PAGO_FECHA_LIMITE]','[ALUMNO]','[CURSO_NOMBRE]','[CURSO_CODIGO]'];
        $values = [
                ($payment->concept->name ? $payment->concept->name : ''),
                ($payment->amount ? '$'.number_format($payment->amount,0) : ''),
                ($payment->deadline ? \Carbon\Carbon::parse($payment->deadline)->format('d/m/Y') : ''),
                ($payment->alumn->name() ? $payment->alumn->name() : ''),
                ($payment->course && $payment->course->service && $payment->course->service->name ? $payment->course->service->name : ''),
                ($payment->course && $payment->course->code ? $payment->course->code : '')
            ];
         
        return str_replace($keywords,$values,$text);  
        
    }//process_keywords
}
