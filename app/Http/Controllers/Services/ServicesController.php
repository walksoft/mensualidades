<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\User;
use App\Models\Service;
use App\Models\Type_service;
use App\Models\Level_service;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable; 

class ServicesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder){ 
        
        if(!auth()->user()->hasPermissionTo('list.service') && 
           !auth()->user()->hasPermissionTo('create.service') &&
           !auth()->user()->hasPermissionTo('edit.service') &&
           !auth()->user()->hasPermissionTo('delete.service')){            
          Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
          return redirect()->route('admin');
        }
        
        $permits = [];
        $permits['create'] = auth()->user()->hasPermissionTo('create.service');
        $permits['edit']   = auth()->user()->hasPermissionTo('edit.service');
        $permits['delete'] = auth()->user()->hasPermissionTo('delete.service');
        
        $permitsTypes = [];
        $permitsTypes['list'] = auth()->user()->hasPermissionTo('list.typeservice');
        $permitsTypes['create'] = auth()->user()->hasPermissionTo('create.typeservice');
        $permitsTypes['edit']   = auth()->user()->hasPermissionTo('edit.typeservice');
        $permitsTypes['delete'] = auth()->user()->hasPermissionTo('delete.typeservice');
        
        $permitsLevels = [];
        $permitsLevels['list'] = auth()->user()->hasPermissionTo('list.typelevel');
        $permitsLevels['create'] = auth()->user()->hasPermissionTo('create.typelevel');
        $permitsLevels['edit']   = auth()->user()->hasPermissionTo('edit.typelevel');
        $permitsLevels['delete'] = auth()->user()->hasPermissionTo('delete.typelevel');
        
        if ($request->ajax()){

            $services = Service::join('type_services','services.type_id','type_services.id')
                    ->join('level_services','services.level_id','level_services.id')
                    ->orderBy('created_at','DESC')
                    ->get(['services.service_id',
                        'services.name',
                        'services.created_at',
                        'type_services.name as type',
                        'level_services.name as level',
                        'services.session_duration',
                        'services.status'
                        ]);
            $datatable = Datatables::of($services);

            // Columna EDITAR
            if( $permits['edit'] ){
                $datatable = $datatable->addColumn( 'action', function ($services) {
                    return '<a href="#" data-id="'.$services->service_id.'" class="btn btn-info btn-sm btnModalEdit" data-toggle="modal"><i class="fas fa-pencil-alt"></i> Editar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.edit'), 'action']);
            } // if

            // Columna ELIMINAR
            if($permits['delete'] ){
                $datatable = $datatable->addColumn( trans('adminlte::adminlte.delete'), function ($services) {
                    return '<a href="" class="btn btn-danger btn-sm confirmDelete" data-id="'.$services->service_id.'" data-toggle="modal" data-target="#confirmDelete" ><i class="fas fa-trash"></i> Eliminar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.delete'), 'action']);
            } // if

            // Se formatean contenidos de columnas
            $datatable->editColumn('status',function($services){
                return $services->status ? trans('adminlte::adminlte.active') : trans('adminlte::adminlte.inactive');
            });
            
            $datatable = $datatable->make(true);

            return $datatable;
        }

        $htmlBuilder->parameters([
            'order' => [],
            'ordering' =>false,
            'paging' => true,
            'searching' => true,
            'info' => true,
            'searchDelay' => 350,
            'language' => [
                'url' => url('vendor/datatables/Spanish.json')
            ],
            'buttons'      => ['print', 'excel'],        
        ]);

        $html = $htmlBuilder
            ->addColumn(['data' => 'name' , 'name' => 'name','title'=>trans('adminlte::adminlte.name') ])
            ->addColumn(['data' => 'session_duration' , 'name' => 'session_duration','title'=>trans('adminlte::services.duration') ])
            ->addColumn(['data' => 'type' , 'name' => 'type','title'=>trans('adminlte::services.type') ])
            ->addColumn(['data' => 'level' , 'name' => 'level','title'=>trans('adminlte::services.level') ])
            ->addColumn(['data' => 'status' , 'name' => 'status','title'=>trans('adminlte::services.status') ]);
                
        if( $permits['edit'] ){
            $html = $html->addColumn(['data' => 'action', 'name' => 'action','title'=> trans('adminlte::adminlte.edit'),'class' => 'center' ]);
        } // if

        if($permits['delete'] ){
            $html = $html->addColumn(['data' => trans('adminlte::adminlte.delete'), 'name' => trans('adminlte::adminlte.delete'),'title'=> trans('adminlte::adminlte.delete') ]);
        } // if

        return  view('admin.services.index',compact('permits','permitsTypes','permitsLevels','html'));
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
        if(!auth()->user()->hasPermissionTo('create.service')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
        return response()->json(['status'=>1,'html'=>view('admin.services.create')->render()]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){ 

        // Permisos
        if(!auth()->user()->hasPermissionTo('create.service')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        // Validar datos
        $validator = Validator::make($request->all(),[
            'name' => 'required|string||max:191',
            'duration' => 'required|numeric',
            'type' => 'required|numeric',
            'level' => 'required|numeric',            
            'monthlyCharge' => 'required|numeric',
            'promptPaymentCollection' => 'required|numeric',
            'inscription' => 'required|numeric',
            'status' => 'required|numeric|max:1',
            'description' => 'nullable|string|max:5000',
        ],[            
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'duration.required' => 'El campo Duración sesión es requerido.',  
            'duration.numeric' => 'El campo Duración sesión debe ser numerico.',
            'type.required' => 'El campo Tipo es requerido.',  
            'type.numeric' => 'El campo Tipo debe ser numerico.',
            'level.required' => 'El campo Nivel es requerido.',  
            'level.numeric' => 'El campo Nivel debe ser numerico.',
            'monthlyCharge.required' => 'El campo Cobro mensual es requerido.',  
            'monthlyCharge.numeric' => 'El campo Cobro mensual debe ser numerico.',
            'promptPaymentCollection.required' => 'El campo Cobro pronto pago es requerido.',  
            'promptPaymentCollection.numeric' => 'El campo Cobro pronto pago debe ser numerico.',
            'inscription.required' => 'El campo Inscripción es requerido.',  
            'inscription.numeric' => 'El campo Inscripción debe ser numerico.',
            
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

 
        try{
            //Creating Service
            $service = new Service();                    
            $service->name = $request->name;
            $service->session_duration = $request->duration;
            $service->type_id = $request->type;
            $service->level_id = $request->level;
            $service->monthly_value = $request->monthlyCharge;
            $service->timely_payment_value = $request->promptPaymentCollection;
            $service->registration_value = $request->inscription;
            $service->status = $request->status;
            $service->description = $request->description;
            $service->save();
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::services.service_successfully_created');

        }catch(QueryException $e){
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//store
    
    public function edit(Request $request){
        
        if(!auth()->user()->hasPermissionTo('edit.service')){
            Session::flash('error',trans('adminlte::adminlte.app_msj_not_permissions'));
            return redirect()->route('admin');
        } 
        
        $service = Service::find($request->_id);
        
        return view('admin.services.edit', compact('service'))->render();

    } // edit
    
    public function update(Request $request){
        
        // Permisos
        if(!auth()->user()->hasPermissionTo('edit.service')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        $service = Service::find($request->_id);
        
        // Validar datos
        $validator = Validator::make($request->all(),[
            'name' => 'required|string||max:191',
            'duration' => 'required|numeric',
            'type' => 'required|numeric',
            'level' => 'required|numeric',            
            'monthlyCharge' => 'required|numeric',
            'promptPaymentCollection' => 'required|numeric',
            'inscription' => 'required|numeric',
            'status' => 'required|numeric|max:1',
            'description' => 'nullable|string|max:5000',
        ],[            
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'duration.required' => 'El campo Duración sesión es requerido.',  
            'duration.numeric' => 'El campo Duración sesión debe ser numerico.',
            'type.required' => 'El campo Tipo es requerido.',  
            'type.numeric' => 'El campo Tipo debe ser numerico.',
            'level.required' => 'El campo Nivel es requerido.',  
            'level.numeric' => 'El campo Nivel debe ser numerico.',
            'monthlyCharge.required' => 'El campo Cobro mensual es requerido.',  
            'monthlyCharge.numeric' => 'El campo Cobro mensual debe ser numerico.',
            'promptPaymentCollection.required' => 'El campo Cobro pronto pago es requerido.',  
            'promptPaymentCollection.numeric' => 'El campo Cobro pronto pago debe ser numerico.',
            'inscription.required' => 'El campo Inscripción es requerido.',  
            'inscription.numeric' => 'El campo Inscripción debe ser numerico.',
            
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

 
        try{
            //Updating Service                                      
            $service->name = $request->name;
            $service->session_duration = $request->duration;
            $service->type_id = $request->type;
            $service->level_id = $request->level;
            $service->monthly_value = $request->monthlyCharge;
            $service->timely_payment_value = $request->promptPaymentCollection;
            $service->registration_value = $request->inscription;
            $service->status = $request->status;
            $service->description = $request->description;
            $service->save();
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::services.service_successfully_updated');

        }catch(QueryException $e){
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//update
    
    public function destroy(Request $request){
        // Permisos
        if(!auth()->user()->hasPermissionTo('delete.service')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }

        $service = Service::find($request->id)->delete();
        
        return response()->json([
            'status'=> 1,
            'type' => 'success',
            'msg' =>  trans('adminlte::services.service_successfully_delete')
        ]); 
        
    }//destroy
    
    public function get_tbl_types(Request $request){
        
        // Permisos
        if(!auth()->user()->hasPermissionTo('list.typeservice')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
        
        $types = \App\Models\Type_service::all();
        
        return response()->json(['status'=>1,'html'=>view('admin.services.table_types',compact('types'))->render()]);
        
    }//get_tbl_types
    
    public function store_type(Request $request){
        // Permisos
        if(!auth()->user()->hasPermissionTo('create.typeservice')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }        
        
        // Validar datos
        $validator = Validator::make($request->all(),[
            'name' => 'required|string||max:191',
            'description' => 'nullable|string|max:5000',
            'status' => 'required|numeric|max:1',            
        ],[            
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'description.max' =>'La descripción no puede contener mas de :max caracteres.',
            'status.required' => 'El campo estado es requerido.',
            'level.numeric' => 'El campo estado debe ser numerico.',            
        ]);

        // check validation
        if($validator->fails()){         
            return response()->json(['status'=>2,'type'=>'error','msg'=>$validator->messages()]); 
        }//if       

       try{
            //Creating Type service
            $type_service = new Type_service();                    
            $type_service->name = $request->name;
            $type_service->description = $request->description;
            $type_service->status = $request->status;
            $type_service->save();

        }catch(QueryException $e){
            return response()->json(['status'=>0,'type'=>'error','msg'=>'error: '.$e]);
        }//cath
        
        return response()->json(['status'=>1,'type'=>'success','msg'=>trans('adminlte::services.type_service_successfully_created')]);
        
    }//store_type

    public function get_tbl_levels(Request $request){
        
        // Permisos
        if(!auth()->user()->hasPermissionTo('list.typelevel')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
        
        $levels = \App\Models\Level_service::all();
        
        return response()->json(['status'=>1,'html'=>view('admin.services.table_levels',compact('levels'))->render()]);
        
    }//get_tbl_levels    
    
    public function store_level(Request $request){
        
        // Permisos
        if(!auth()->user()->hasPermissionTo('create.typelevel')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }        
        
        // Validar datos
        $validator = Validator::make($request->all(),[
            'name' => 'required|string||max:191',
            'description' => 'nullable|string|max:5000',
            'status' => 'required|numeric|max:1',            
        ],[            
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'description.max' =>'La descripción no puede contener mas de :max caracteres.',
            'status.required' => 'El campo estado es requerido.',
            'level.numeric' => 'El campo estado debe ser numerico.',            
        ]);

        // check validation
        if($validator->fails()){         
            return response()->json(['status'=>2,'type'=>'error','msg'=>$validator->messages()]); 
        }//if       

       try{
            //Creating Type service
            $level_service = new Level_service();                    
            $level_service->name = $request->name;
            $level_service->description = $request->description;
            $level_service->status = $request->status;
            $level_service->save();

        }catch(QueryException $e){
            return response()->json(['status'=>0,'type'=>'error','msg'=>'error: '.$e]);
        }//cath
        
        return response()->json(['status'=>1,'type'=>'success','msg'=>trans('adminlte::services.level_service_successfully_created')]);        
        
    }//store_level
    
    public function update_status(Request $request){        
        try{
            
            if($request->model == 'type'){
                $type_service = \App\Models\Type_service::find($request->id);
                $type_service->status = $request->value;
                $type_service->save();
            }//if

            if($request->model == 'level'){
                $level_service = \App\Models\Level_service::find($request->id);
                $level_service->status = $request->value;
                $level_service->save();
            }//if

            return response()->json(['status'=>1]);
            
        }catch(QueryException $e){
            return response()->json(['status'=>0,'type'=>'error','msg'=>'error: '.$e]);
        }//cath        
    }//update_status
    
    public function delete_type_or_level(Request $request){        
        try{
            
            if($request->model == 'type'){
                $msg = trans('adminlte::services.type_successfully_delete');
                $type_service = \App\Models\Type_service::find($request->id);
                if($type_service->service->count() > 0){
                    return response()->json(['status'=>0,'type'=>'error','msg'=>trans('adminlte::services.type_service_associated_services')]);        
                }//if
                $type_service->delete();
            }//if

            if($request->model == 'level'){
                $msg = trans('adminlte::services.level_successfully_delete');
                $level_service = \App\Models\Level_service::find($request->id);
                if($level_service->service->count() > 0){
                    return response()->json(['status'=>0,'type'=>'error','msg'=>trans('adminlte::services.level_service_associated_services')]);        
                }//if                
                $level_service->delete();
            }//if

            return response()->json(['status'=>1,'type'=>'success','msg'=>$msg]);
            
        }catch(QueryException $e){
            return response()->json(['status'=>0,'type'=>'error','msg'=>'error: '.$e]);
        }//cath        
    }//delete_types_or_levels
}
