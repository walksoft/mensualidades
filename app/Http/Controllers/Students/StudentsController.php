<?php

namespace App\Http\Controllers\Students;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Tutor;
use App\Models\User_file;
use App\User;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable; 

class StudentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder){ 

        if(!auth()->user()->hasPermissionTo('list.student') && 
           !auth()->user()->hasPermissionTo('create.student') &&
           !auth()->user()->hasPermissionTo('edit.student') &&
           !auth()->user()->hasPermissionTo('delete.student')){            
          Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
          return redirect()->route('admin');
        }
        
        $permits = [];
        $permits['create'] = auth()->user()->hasPermissionTo('create.student');
        $permits['edit']   = auth()->user()->hasPermissionTo('edit.student');
        $permits['delete'] = auth()->user()->hasPermissionTo('delete.student');
        
        if ($request->ajax()){

            $users = User::where('type_user',3)->orderBy('created_at','DESC')->get();

            $datatable = Datatables::of($users);

            // Columna EDITAR
            if( $permits['edit'] ){
                $datatable = $datatable->addColumn( 'action', function ($users) {
                    return '<a href="#" data-id="'.$users->id.'" class="btn btn-info btn-sm btnModalEdit" data-toggle="modal"><i class="fas fa-pencil-alt"></i> Editar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.edit'), 'action']);
            } // if

            // Columna ELIMINAR
            if($permits['delete'] ){
                $datatable = $datatable->addColumn( trans('adminlte::adminlte.delete'), function ($users) {
                    return '<a href="" class="btn btn-danger btn-sm confirmDelete" data-id="'.$users->id.'" data-toggle="modal" data-target="#confirmDelete" ><i class="fas fa-trash"></i> Eliminar</a>';
                });
                $datatable = $datatable->rawColumns([trans('adminlte::adminlte.delete'), 'action']);
            } // if

            // Se formatean contenidos de columnas
            $datatable->editColumn('name', function( $users ){
                return $users->name.($users->last_name ? ' '.$users->last_name : '');
            });

            // Se formatean contenidos de columnas
            $datatable->editColumn('status',function($users){
                return $users->status ? trans('adminlte::adminlte.active') : trans('adminlte::adminlte.inactive');
            });
            
            $datatable = $datatable->make(true);

            return $datatable;
        }

        $htmlBuilder->parameters([
            'order' => [],
            'ordering' =>false,
            'paging' => true,
            'searching' => true,
            'info' => true,
            'searchDelay' => 350,
            'language' => [
                'url' => url('vendor/datatables/Spanish.json')
            ],
            'buttons'      => ['print', 'excel'],
        
        ]);

        $html = $htmlBuilder
            ->addColumn(['data' => 'name' , 'name' => 'name','title'=>trans('adminlte::adminlte.name') ])
            ->addColumn(['data' => 'email', 'name' => 'email','title'=>trans('adminlte::adminlte.email') ])
            ->addColumn(['data' => 'phone_number', 'name' => 'phone_number','title'=>trans('adminlte::adminlte.phone') ])
            ->addColumn(['data' => 'status', 'name' => 'status','title'=>trans('adminlte::adminlte.status') ]);
        
        

        if( $permits['edit'] ){
            $html = $html->addColumn(['data' => 'action', 'name' => 'action','title'=> trans('adminlte::adminlte.edit'),'class' => 'center' ]);
        } // if

        if($permits['delete'] ){
            $html = $html->addColumn(['data' => trans('adminlte::adminlte.delete'), 'name' => trans('adminlte::adminlte.delete'),'title'=> trans('adminlte::adminlte.delete') ]);
        } // if

        return  view('admin.students.index',compact('permits','html'));
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
        if(!auth()->user()->hasPermissionTo('create.student')){
            return response()->json(['status'=>3,'type'=>'error','msg'=>trans('adminlte::adminlte.app_msj_not_permissions')]);
        }
        return response()->json(['status'=>1,'html'=>view('admin.students.create')->render()]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){ 

        // Permisos
        if(!auth()->user()->hasPermissionTo('create.student')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        // Validar datos
        $validator = Validator::make($request->all(),[
            'name' => 'required|string||max:190',
            'lastName' => 'nullable|string||max:190',
            'email' => 'required|string|email|max:190|unique:users',
            'phone' => 'nullable|string||max:190',
            'address' => 'nullable|string||max:190',
            'status' => 'required',
            'password' => 'required|string|min:8|confirmed',
            'observations' => 'nullable|string|max:5000',
        ],[            
            'name.required' => 'El campo nombre es requerido.',
            'name.max' =>'El nombre no puede contener mas de :max caracteres.',
            'lastName.required' => 'El campo Apellido es requerido.',
            'lastName.max' =>'El Apellido no puede contener mas de :max caracteres.',
            'email.required' => 'El campo email es requerido', 
            'email.unique' => 'El email ya se encuentra registrado.',  
            'phone.required' => 'El campo Teléfono es requerido.',  
            'phone.max' => 'El Teléfono no puede contener mas de :max caracteres.',
            'address.max' => 'El Dirección no puede contener mas de :max caracteres.',
            'state.required' =>'El campo estado es requerido.',
            'password.required' => 'El campo contraseña es requerido.',
            'password.min' =>'La contraseña debe tener minimo :min caracteres.',
            'password.confirmed' =>'La confirmación de la contraseña no coincide.',            
            'observations.max' => 'Las Observaciones no pueden contener mas de :max caracteres.',
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }
 
        try{
            
            //init transactions
            \DB::beginTransaction();
            
            //Creating Student
            $student = new User();                    
            $student->name = $request->name;
            $student->last_name = $request->lastName;
            $student->email = $request->email;            
            $student->phone_number = $request->phone;            
            $student->address = $request->address;            
            $student->type_user = 3;//estudiante
            $student->status = $request->status;
            $student->observations = $request->observations;
            $student->password = Hash::make($request->password);
            if($student->save()){
                $student->assignRoles('student999');
            }//if
            
            //Creating Tutor
            if($request->tutorName || $request->tutorLastName || $request->tutorEmail || $request->tutorPhone || $request->tutorAddress || $request->relationship){
                $tutor = new Tutor();      
                $tutor->user_id = $student->id;
                $tutor->name = $request->tutorName;
                $tutor->last_name = $request->tutorLastName;
                $tutor->email = $request->tutorEmail;            
                $tutor->phone_number = $request->tutorPhone;            
                $tutor->address = $request->tutorAddress;            
                $tutor->kinship_id = $request->relationship;
                $tutor->save();            
            }//if
            
            if($request->curp){
                $user_file = new User_file();
                $user_file->user_id = $student->id; 
                $user_file->file_id = 1;
                $user_file->file_name = $request->file('curp')->getClientOriginalName();                           
                $user_file->route = $request->file('curp')->store('files');
                $user_file->extension = $request->file('curp')->getClientOriginalExtension();
                $user_file->file_size = $request->file('curp')->getSize();
                $user_file->save();
            }//if
            if($request->birthCertificate){
                $user_file = new User_file();
                $user_file->user_id = $student->id; 
                $user_file->file_id = 2;
                $user_file->file_name = $request->file('birthCertificate')->getClientOriginalName();                           
                $user_file->route = $request->file('birthCertificate')->store('files');
                $user_file->extension = $request->file('birthCertificate')->getClientOriginalExtension();
                $user_file->file_size = $request->file('birthCertificate')->getSize();
                $user_file->save();
            }//if
            
            //commit
            \DB::commit();
                        
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::students.student_successfully_created');

        }catch(QueryException $e){
            //rolback transaction
            \DB::rollBack();
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//store
    
    public function edit(Request $request){
        
        if(!auth()->user()->hasPermissionTo('edit.student')){
            Session::flash('error',trans('adminlte::adminlte.app_msj_not_permissions'));
            return redirect()->route('admin');
        } 
        
        $files = [];
        $count = 1;
        $user = User::find($request->_id);
        
        if(count($user->user_files) > 0){
            foreach($user->user_files as $user_files){
                $files[$user_files->file_id]['id'] = $user_files->id;
                $files[$user_files->file_id]['file_url'] = route('display_file',['id'=>$user_files->id]);
                $files[$user_files->file_id]['file_id'] = $user_files->file_id;
                $files[$user_files->file_id]['file_name'] = $user_files->file_name;
                $files[$user_files->file_id]['route'] = $user_files->route;
                $files[$user_files->file_id]['extension'] = $user_files->extension;
                $files[$user_files->file_id]['file_size'] = $user_files->file_size;                                
            }//foreach
        }//if

        return view('admin.students.edit',compact('user','files','count'))->render();

    } // edit
    
    public function update(Request $request){
        
        // Permisos
        if(!auth()->user()->hasPermissionTo('edit.student')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }
           
        $id = $request->id;       
        
        //Validar datos
        $validator = Validator::make($request->all(),[
            'name_student' => 'required|string||max:190',
            'lastName_student' => 'nullable|string||max:190',
            'email_student' => 'required|string|email|max:190|unique:users,email,'.$id,
            'phone_student' => 'nullable|string||max:190',
            'address_student' => 'nullable|string||max:190',
            'status_student' => 'required',
            'student_password' => 'nullable|string|min:8|confirmed',
            'observations_student' => 'nullable|string|max:5000',
        ],[            
            'name_student.required' => 'El campo nombre es requerido.',
            'name_student.max' =>'El nombre no puede contener mas de :max caracteres.',
            'lastName_student.required' => 'El campo Apellido es requerido.',
            'lastName_student.max' =>'El Apellido no puede contener mas de :max caracteres.',
            'email_student.required' => 'El campo email es requerido', 
            'email_student.unique' => 'El email ya se encuentra registrado.',  
            'phone_student.required' => 'El campo Teléfono es requerido.',  
            'phone_student.max' => 'El Teléfono no puede contener mas de :max caracteres.',
            'address_student.max' => 'El Dirección no puede contener mas de :max caracteres.',
            'state_student.required' =>'El campo estado es requerido.',
            'student_password.required' => 'El campo contraseña es requerido.',
            'student_password.min' =>'La contraseña debe tener minimo :min caracteres.',
            'student_password.confirmed' =>'La confirmación de la contraseña no coincide.',   
            'observations_student.max' => 'Las Observaciones no pueden contener mas de :max caracteres.',
        ]);

        // check validation
        if ($validator->fails()) {
         
            return response()->json([
                'status'=> 2,
                'type' => 'error',
                'msg' => $validator->messages()
            ]); 
        }

 
        try{
            
            //init transactions
            \DB::beginTransaction();
            
            //Updating Student
            $student = User::find($id);
            $student->name = $request->name_student;
            $student->last_name = $request->lastName_student;
            $student->email = $request->email_student;            
            $student->phone_number = $request->phone_student;            
            $student->address = $request->address_student;            
            $student->status = $request->status_student;
            $student->observations = $request->observations_student;
            if($request->student_password){
                $student->password = Hash::make($request->student_password);
            }//if
            $student->save();
            
            //Updating Tutor
            if($request->tutor_id){
                $tutor = Tutor::find($request->tutor_id);
                $tutor->name = $request->tutorName_student;
                $tutor->last_name = $request->tutorLastName_student;
                $tutor->email = $request->tutorEmail_student;            
                $tutor->phone_number = $request->tutorPhone_student;            
                $tutor->address = $request->tutorAddress_student;            
                $tutor->kinship_id = $request->relationship_student;
                $tutor->save();            
            }//if
            
            //Creating Tutor
            else if($request->tutorName_student || $request->tutorLastName_student || $request->tutorEmail_student || $request->tutorPhone_student || $request->tutorAddress_student || $request->relationship_student){
                $tutor = new Tutor();      
                $tutor->user_id = $student->id;
                $tutor->name = $request->tutorName_student;
                $tutor->last_name = $request->tutorLastName_student;
                $tutor->email = $request->tutorEmail_student;            
                $tutor->phone_number = $request->tutorPhone_student;            
                $tutor->address = $request->tutorAddress_student;            
                $tutor->kinship_id = $request->relationship_student;
                $tutor->save();            
            }//if
            
            $curp_student = User_file::where('user_id',$student->id)->where('file_id',1)->first();
            if($request->curp_student && $curp_student){
                $curp_student->file_name = $request->file('curp_student')->getClientOriginalName();                           
                $curp_student->route = $request->file('curp_student')->store('files');
                $curp_student->extension = $request->file('curp_student')->getClientOriginalExtension();
                $curp_student->file_size = $request->file('curp_student')->getSize();
                $curp_student->save();                
            }//if
            else if($request->curp_student){
                $user_file = new User_file();
                $user_file->user_id = $student->id; 
                $user_file->file_id = 1;
                $user_file->file_name = $request->file('curp_student')->getClientOriginalName();                           
                $user_file->route = $request->file('curp_student')->store('files');
                $user_file->extension = $request->file('curp_student')->getClientOriginalExtension();
                $user_file->file_size = $request->file('curp_student')->getSize();
                $user_file->save();
            }//elseif
            
            $birthCertificate_student = User_file::where('user_id',$student->id)->where('file_id',2)->first();
            if($request->birthCertificate_student && $birthCertificate_student){
                $birthCertificate_student->file_name = $request->file('birthCertificate_student')->getClientOriginalName();                           
                $birthCertificate_student->route = $request->file('birthCertificate_student')->store('files');
                $birthCertificate_student->extension = $request->file('birthCertificate_student')->getClientOriginalExtension();
                $birthCertificate_student->file_size = $request->file('birthCertificate_student')->getSize();
                $birthCertificate_student->save();                
            }//if
            else if($request->birthCertificate_student){
                $user_file = new User_file();
                $user_file->user_id = $student->id; 
                $user_file->file_id = 2;
                $user_file->file_name = $request->file('birthCertificate_student')->getClientOriginalName();                           
                $user_file->route = $request->file('birthCertificate_student')->store('files');
                $user_file->extension = $request->file('birthCertificate_student')->getClientOriginalExtension();
                $user_file->file_size = $request->file('birthCertificate_student')->getSize();
                $user_file->save();
            }//elseif
            
            //commit
            \DB::commit();
            
            $status = 1;
            $type = 'success';
            $msg = trans('adminlte::students.student_successfully_updated');

        }catch(QueryException $e){
            //rolback transaction
            \DB::rollBack();
            $status = 0;
             $type = 'error';
             $msg = 'error : '. $e;
        }//cath
        
        return response()->json([
            'status'=>$status,
            'type' => $type,
            'msg' => $msg
        ]);  
        
    }//update
    
    public function destroy(Request $request){
        // Permisos
        if(!auth()->user()->hasPermissionTo('delete.student')){
            return response()->json([
                'status'=> 3,
                'type' => 'error',
                'msg' => trans('adminlte::adminlte.app_msj_not_permissions')
            ]);
        }

        $User = User::find($request->id)->delete();
        
        return response()->json([
            'status'=> 1,
            'type' => 'success',
            'msg' =>  trans('adminlte::students.student_successfully_delete')
        ]); 
        
    }//destroy
    
    public function my_courses(){
        
        if(!auth()->user()->hasPermissionTo('mycourses.student')){            
            Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
            return redirect()->route('admin');
        }//if        
        
        return  view('admin.students.my_courses');
        
    }//my_courses
    
    public function my_payments(){
        
        if(!auth()->user()->hasPermissionTo('paymenthistory.student')){            
            Session::flash('warning',trans('adminlte::adminlte.app_msj_not_permissions'));          
            return redirect()->route('admin');
        }//if   
        
        return  view('admin.students.my_payments');
        
    }//my_courses    
}
