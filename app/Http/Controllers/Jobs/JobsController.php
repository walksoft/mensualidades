<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller;
use App\Models\Parameter;
use App\Models\Payment;
use App\Models\Payment_detail;
use App\Models\Course;
use App\User;
use Illuminate\Database\QueryException;

class JobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){        
        //$this->middleware('auth');
    }

    public function job_monthly_payments_process(){ 
        $this->monthly_payments_process();
    }//job_monthly_payments_process
    
    public function job_monthly_pending_reminder_process(){ 
        $this->monthly_pending_reminder_process();
    }//job_monthly_payments_process
    
    public function job_instructors_payment_process(){ 
        $month = date('m');
        $this->instructors_payment_process($month);
    }//job_instructors_payment_process    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function monthly_payments_process(){ 
        //consulta los cursos activos
        $courses = Course::where('status',1)->get();        
        $day = Parameter::where('name','payment_deadline')->first()->value;
        $date = \Carbon\Carbon::createFromFormat('d/m/Y',$day.'/'.date('m').'/'.date('Y'))->addMonth();
        //recorre los cursos
        foreach($courses as $course){
            foreach($course->registration as $registration){
                if(!$this->exists_payment($registration->user_id,$date,$registration->course_id)){
                    $this->create_payment($registration->user_id,$registration->course_id,$registration->monthly_value,$date);
                }//if
            }//foreach
        }//foreach

    }//monthly_payments_process
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    function monthly_pending_reminder_process(){        
        $date = \Carbon\Carbon::now()->format('Y-m-d');
        //consulta los cursos activos
        $payments = Payment::where('status_id','<>',2)->whereNotNull('deadline')->whereDate('deadline','<',$date)->get();        
        //recorre los cobros vencidos
        foreach($payments as $payment){
            \Notification::send($payment->alumn,new \App\Notifications\Payment_notification($payment->alumn,$payment,3));
        }//foreach
    }//monthly_pending_reminder_process
    
    public function instructors_payment_process($month){
        //consulta los cursos activos
        $instructores = User::where('type_user',2)->where('status',1)->get();           
        $day = Parameter::where('name','instructor_payday')->first()->value;
        $date = \Carbon\Carbon::createFromFormat('d/m/Y',$day.'/'.$month.'/'.date('Y'));        
        foreach($instructores as $instructor){
            if($instructor->active_courses->count() > 0 && !$this->exists_payment($instructor->id,$date)){
                $this->create_payment_instructor($instructor,$instructor->active_courses,$date);
            }//if
        }//foreach        
    }//instructors_payment_process

    public function create_payment_instructor($instructor,$courses,$date){
        $amount = 0;
        $payment = new Payment();
        $payment->reference = time();
        $payment->concept_id = intval(3);                                                
        $payment->user_id = $instructor->id;                           
        $payment->deadline = $date->format('Y-m-d');
        $payment->amount = 0;   
        $payment->observations = '[ Pago generado automáticamente ]';
        $payment->ind_system = intval(1);
        $payment->status_id = intval(1); 
        if($payment->save()){
            foreach($courses as $course){
                $payment_detail = new Payment_detail();
                $payment_detail->payment_id = $payment->id;
                $payment_detail->course_id = $course->course_id;
                $payment_detail->amount = $course->payment_instructor;
                $payment_detail->total = $course->payment_instructor;
                $payment_detail->ind_system = intval(1);
                $payment_detail->save();
                $amount += $course->payment_instructor;                                
            }//foreach
        }//if    
        $payment->amount = $amount;
        $payment->save();
        usleep(2000000);
    }//create_payment_instructor
    
    public function exists_payment($user,$date,$course=NULL){  
        if($course){
            return Payment::where('user_id',$user)->where('course_id',$course)->whereDate('deadline',$date->format('Y-m-d'))->exists();
        }//if
        else{
            return Payment::where('user_id',$user)->whereDate('deadline',$date->format('Y-m-d'))->exists();
        }//else
    }//exists_payment    
    
    public function create_payment($user_id,$course_id,$amount,$date){
        //Creating Payment
        $payment = new Payment();                    
        $payment->reference = time();                  
        $payment->concept_id = intval(2);                                                
        $payment->user_id = $user_id;                    
        $payment->course_id = $course_id;     
        $payment->deadline = $date->format('Y-m-d');
        $payment->amount = $amount;
        $payment->ind_system = intval(1);
        $payment->status_id = intval(1);                
        if($payment->save()){
            $user = User::find($payment->user_id);
            \Notification::send($user,new \App\Notifications\Payment_notification($user,$payment,2));
        }//if
        usleep(2000000);
    }//create_payment
}
