<?php

if(!function_exists('random_code')){
    
    function random_code(){ 
        return rand(1111,9999);
    }//random_code
    
}//if

if(!function_exists('get_parameter')){
    
    function get_parameter($name,$default = null){
        if(is_array($name)){
            $parameter = \App\Models\Parameter::whereIn('name',$name)->get();
            if($parameter && $parameter->count()){                
                $object = (object)[];
                foreach($parameter as $key => $value){
                    $obc = (string)$value->name;
                    $object->$obc =  ['id'=>$value->id,'name'=>$value->name,'description'=>$value->description,'value'=>$value->value,'created_at'=>$value->created_at,'updated_at'=>$value->updated_at];                   
                }//foreach
                return $object;
            }//if
            else if($default){
                return $default;
            }//else    
            else{
                return false;
            }//else
        }//if
        else{
            $parameter = \App\Models\Parameter::where('name',$name)->first();
            if($parameter && $parameter->value){
                return $parameter->value;
            }//if
            else if($default){
                return $default;
            }//else    
            else{
                return false;
            }//else
        }//else                
    }//get_parameter
    
}//if