<?php

namespace App\Listeners;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Login;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $event->user->update(['last_login_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),'last_login_ip'=>$this->request->getClientIp()]);
        \App\Models\Login_log::create(['user_id'=>$event->user->id,'event'=>'login','ip_address'=> $this->request->getClientIp(),'user_agent'=>$this->request->header('User-Agent')]);
    }
}
