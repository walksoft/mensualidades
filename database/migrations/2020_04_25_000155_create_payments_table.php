<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->bigInteger('reference')->unique();
            $table->unsignedBigInteger('concept_id');
            $table->foreign('concept_id')->references('id')->on('payment_concepts');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');             
            $table->unsignedBigInteger('course_id')->nullable();
            $table->foreign('course_id')->references('course_id')->on('courses');             
            $table->double('amount')->default(0);
            $table->double('additional')->default(0);
            $table->double('discount')->default(0);
            $table->double('total_paid')->nullable();
            $table->timestamp('deadline')->nullable();
            $table->json('payment_details_sent')->nullable();
            $table->json('payment_details_received')->nullable();
            $table->string('payment_method',55)->nullable();
            $table->text('observations')->nullable();
            $table->tinyInteger('ind_system')->default(0);
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->foreign('admin_id')->references('id')->on('users');            
            $table->unsignedBigInteger('status_id');
            $table->foreign('status_id')->references('id')->on('status_payments');
            $table->timestamp('payment_date')->nullable();    
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}