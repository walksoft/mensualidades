<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('course_id');
            $table->string('code',20)->unique();
            $table->unsignedBigInteger('service_id');
            $table->foreign('service_id')->references('service_id')->on('services');             
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');    
            $table->double('payment_instructor')->default(0);
            $table->json('frequency');
            $table->time('start_time');
            $table->time('end_time');
            $table->text('observations')->nullable();
            $table->tinyInteger('status');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
