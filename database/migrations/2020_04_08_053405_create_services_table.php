<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services',function(Blueprint $table){
            $table->bigIncrements('service_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('session_duration');
            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('type_services');
            $table->unsignedBigInteger('level_id');
            $table->foreign('level_id')->references('id')->on('level_services');
            $table->double('monthly_value');
            $table->double('timely_payment_value');
            $table->double('registration_value');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
