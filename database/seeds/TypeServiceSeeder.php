<?php

use Illuminate\Database\Seeder;

class TypeServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_services')->insert([
            'name' => 'Presscolar',
            'description' => 'Tipo de servicio Presscolar',
            'status' => 1
        ]);
        
        DB::table('type_services')->insert([
            'name' => 'Adolecentes',
            'description' => 'Tipo de servicio Adolecentes',
            'status' => 1
        ]);
                
        DB::table('type_services')->insert([
            'name' => 'Adultos',
            'description' => 'Tipo de servicio Adultos',
            'status' => 1
        ]);
    }
}
