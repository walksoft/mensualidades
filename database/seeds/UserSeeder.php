<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Usuario Administrador
        DB::table('users')->insert([
            'name' => 'adminstrador',
            'email' => 'admin@admin.com',
            'type_user' => 1,
            'status' => 1,
            'password' => bcrypt('admin123'),
            'observations' => 'Administrador del sistema',
        ]);
    }
}
