<?php

use Illuminate\Database\Seeder;

class ParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        
        // Configuración
        DB::table('parameters')->insert([
            'name' => 'config_name',
            'description' => 'Campo formulario configuración, nombre de escuela ',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'config_direction',
            'description' => 'Campo formulario configuración, dirección de la escuela',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'config_slogan',
            'description' => 'Campo formulario configuración, eslogan de la escuela',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'config_email',
            'description' => 'Campo formulario configuración, email de contacto de la escuela',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'config_phone',
            'description' => 'Campo formulario configuración, teléfono de contacto de la escuela',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'config_avatar',
            'description' => 'Campo formulario configuración, logo de la escuela',
            'value' => '',
        ]);

        // Pagos
        DB::table('parameters')->insert([
            'name' => 'config_id_paypal',
            'description' => 'Campo formulario pagos, ID cliente PayPal',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'config_secret_paypal',
            'description' => 'Campo formulario pagos, Secreto cliente PayPal',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'config_sandbox_paypal',
            'description' => 'Campo formulario pagos, Modo sandbox de Paypal',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'cutting_day',
            'description' => 'Fecha de corte para el proceso de creación de pagos pendientes.',
            'value' => '27',
        ]);

        DB::table('parameters')->insert([
            'name' => 'payment_deadline',
            'description' => 'Fecha limite de pago el proceso de generación de pagos pendientes',
            'value' => '05',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'smtp_host',
            'description' => 'Configuración HOST SMTP',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'smtp_port',
            'description' => 'Configuración PORT SMTP',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'smtp_username',
            'description' => 'Configuración USERNAME SMTP',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'smtp_password',
            'description' => 'Configuración PASSWORD SMTP',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'smtp_encryption',
            'description' => 'Configuración ENCRYPTION SMTP',
            'value' => '',
        ]);

        DB::table('parameters')->insert([
            'name' => 'notification_after_paying',
            'description' => 'Notificación después de realizar un pago',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'notification_generate_payment',
            'description' => 'Notificación después de generar un recibo de pago',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'notification_payment_pending_reminder',
            'description' => 'Notificación recordatorio de pago pendiente',
            'value' => '',
        ]);        

        DB::table('parameters')->insert([
            'name' => 'subject_notification_after_paying',
            'description' => 'Asunto - notificación después de realizar un pago',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'subject_notification_generate_payment',
            'description' => 'Asunto - notificación después de generar un recibo de pago',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'subject_notification_payment_pending_reminder',
            'description' => 'Asunto - notificación recordatorio de pago pendiente',
            'value' => '',
        ]);         
        
        DB::table('parameters')->insert([
            'name' => 'court_day_pay_instructors',
            'description' => 'Día de corte pago instructores',
            'value' => '',
        ]);
        
        DB::table('parameters')->insert([
            'name' => 'instructor_payday',
            'description' => 'Día de pago instructores',
            'value' => '',
        ]);         
        
    }
}
