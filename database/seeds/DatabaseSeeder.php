<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->truncateTables([
            'roles',
            'permissions',
            'permission_role',
            'parameters',
         ]);
         
         $this->call(RoleSeeder::class);
         $this->call(PermissionSeeder::class);
         $this->call(PermissionRoleSeeder::class);
         $this->call(TypeServiceSeeder::class);
         $this->call(LevelServiceSeeder::class);
         $this->call(ParameterSeeder::class);
         $this->call(KinshipsSeeder::class);
         $this->call(FiletypeSeeder::class);
         $this->call(StatusPaymentSeeder::class);
         $this->call(TransactionTypesSeeder::class);
         $this->call(PaymentConceptsSeeder::class);         
         $this->call(UserSeeder::class);
		 $this->call(RoleUserSeeder::class);		 
         
    }

    protected function truncateTables(array $tables){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::disableForeignKeyConstraints();
        

        foreach($tables as $table){
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Schema::enableForeignKeyConstraints();
        
    }
}
