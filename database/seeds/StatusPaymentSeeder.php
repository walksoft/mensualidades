<?php

use Illuminate\Database\Seeder;

class StatusPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Status_payments
        DB::table('status_payments')->insert([
            'id' => 1,
            'name' => 'Pendiente',
            'description' => 'Estado Pendiente',
            'status' => 1,
        ]);
        DB::table('status_payments')->insert([
            'id' => 2,
            'name' => 'Pagado',
            'description' => 'Estado Pagado',
            'status' => 1,
        ]);
    }
}
