<?php

use Illuminate\Database\Seeder;

class FiletypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('file_types')->insert([
            'name' => 'CURP',
            'description' => 'Tipo de Archivo CURP',
            'status' => 1
        ]);
        
        DB::table('file_types')->insert([
            'name' => 'Acta de nacimiento',
            'description' => 'Tipo de Archivo Acta de nacimiento',
            'status' => 1
        ]);
                
        DB::table('file_types')->insert([
            'name' => 'Anexo',
            'description' => 'Tipo de Archivo Anexo',
            'status' => 1
        ]);
    }
}
