<?php

use Illuminate\Database\Seeder;

class PaymentConceptsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_concepts')->insert([
            'id' => 1,
            'type_id'=>1,
            'name' => 'Inscripción',
            'description' => 'Concepto de pago -  Inscripción',
            'status' => 1,
        ]);
        DB::table('payment_concepts')->insert([
            'id' => 2,
            'type_id'=>1,
            'name' => 'Pago mensual',
            'description' => 'Concepto de pago - Pago mensual',
            'status' => 1,
        ]);
        DB::table('payment_concepts')->insert([
            'id' => 3,
            'type_id'=>2,
            'name' => 'Pago a instructor',
            'description' => 'Concepto de pago - Pago de salario a instructor',
            'status' => 1,
        ]);
    }
}
