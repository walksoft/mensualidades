<?php

use Illuminate\Database\Seeder;

class LevelServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('level_services')->insert([
            'name' => 'Basico 1',
            'description' => 'Nivel de clase Basico 1',
            'status' => 1
        ]);
        
        DB::table('level_services')->insert([
            'name' => 'Intermedio 1',
            'description' => 'Nivel de clase Intermedio 1',
            'status' => 1
        ]);
                
        DB::table('level_services')->insert([
            'name' => 'Avanzado 1',
            'description' => 'Nivel de clase Avanzado 1',
            'status' => 1
        ]);
    }
}
