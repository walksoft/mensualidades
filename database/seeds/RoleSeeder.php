<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Rol Administrador
        DB::table('roles')->insert([
            'name' => 'Administrador',
            'slug' => 'administradores',
            'description' => 'Administrar todo el sistema',
            'is_admin' => 1,
        ]);
        
        // Student Administrador
        DB::table('roles')->insert([
            'id'=>999,
            'name' => 'Estudiantes',
            'slug' => 'student999',
            'description' => 'Rol Estudiante',
            'is_admin' => 0,
        ]);
        
        // Instructor Administrador
        DB::table('roles')->insert([
            'id'=>1000,
            'name' => 'Instructor',
            'slug' => 'instructor1000',
            'description' => 'Rol Instructor',
            'is_admin' => 0,
        ]);
    }
}
