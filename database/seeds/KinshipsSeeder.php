<?php

use Illuminate\Database\Seeder;

class KinshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('kinships')->insert([
            'name' => 'Hija',
            'description' => 'Parentesco Hija',
            'status' => 1
        ]);        
        
        DB::table('kinships')->insert([
            'name' => 'Hijao',
            'description' => 'Parentesco Hijo',
            'status' => 1
        ]);      
                
        DB::table('kinships')->insert([
            'name' => 'Madre',
            'description' => 'Parentesco Madre',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Padre',
            'description' => 'Parentesco Padre',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Madrastra',
            'description' => 'Parentesco Madrastra',
            'status' => 1
        ]);
                        
        DB::table('kinships')->insert([
            'name' => 'Abuela(o)',
            'description' => 'Parentesco Abuela(o)',
            'status' => 1
        ]);
                                
        DB::table('kinships')->insert([
            'name' => 'Bisabuela(o)',
            'description' => 'Parentesco Bisabuela(o)',
            'status' => 1
        ]);
                                        
        DB::table('kinships')->insert([
            'name' => 'Tatarabuela(o)',
            'description' => 'Parentesco Tatarabuela(o)',
            'status' => 1
        ]);
                                                
        DB::table('kinships')->insert([
            'name' => 'Nieta(o)',
            'description' => 'Parentesco Nieta(o)',
            'status' => 1
        ]);
                                                        
        DB::table('kinships')->insert([
            'name' => 'Bisnieta(o)',
            'description' => 'Parentesco Bisnieta(o)',
            'status' => 1
        ]);
                                                                
        DB::table('kinships')->insert([
            'name' => 'Tataranieta(o)',
            'description' => 'Parentesco Tataranieta(o)',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Hermana(o)',
            'description' => 'Parentesco Hermana(o)',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Media(o) hermana(o)',
            'description' => 'Parentesco Media(o) hermana(o)',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Hermanastra(o)',
            'description' => 'Parentesco Hermanastra(o)',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Tía(o)',
            'description' => 'Parentesco Tía(o)',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Sobrina(o)',
            'description' => 'Parentesco Sobrina(o)',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Prima(o)',
            'description' => 'Parentesco Prima(o)',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Consuegra(o)',
            'description' => 'Parentesco Consuegra(o)',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Suegra(o)',
            'description' => 'Parentesco Suegra(o)',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Nuera',
            'description' => 'Parentesco Nuera',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Yerno',
            'description' => 'Parentesco Yerno',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Concuña(o)',
            'description' => 'Parentesco Concuña(o)',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Cuñada(o)',
            'description' => 'Parentesco Cuñada(o)',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Comadre',
            'description' => 'Parentesco Comadre',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Compadre',
            'description' => 'Parentesco Compadre',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Madrina ',
            'description' => 'Parentesco Madrina ',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Padrino',
            'description' => 'Parentesco Padrino',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Ahijada(o)',
            'description' => 'Parentesco Ahijada(o)',
            'status' => 1
        ]);
        
        DB::table('kinships')->insert([
            'name' => 'Esposa',
            'description' => 'Parentesco Esposa',
            'status' => 1
        ]);
                
        DB::table('kinships')->insert([
            'name' => 'Esposo',
            'description' => 'Parentesco Esposo',
            'status' => 1
        ]);
        
    }
}
