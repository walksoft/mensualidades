<?php

use Illuminate\Database\Seeder;

class TransactionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_types')->insert([
            'name' => 'IN',
            'description' => 'Entrada de dinero',
            'status' => 1
        ]);
        
        DB::table('transaction_types')->insert([
            'name' => 'OUT',
            'description' => 'Salida de dinero',
            'status' => 1
        ]);
    }
}
