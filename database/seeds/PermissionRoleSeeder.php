<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Administrador
        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 1
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 1
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 3,
            'role_id' => 1
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 4,
            'role_id' => 1
        ]);
        
        DB::table('permission_role')->insert([
            'permission_id' => 51,
            'role_id' => 999
        ]);
        
        DB::table('permission_role')->insert([
            'permission_id' => 52,
            'role_id' => 999
        ]);        
                
    }
}
