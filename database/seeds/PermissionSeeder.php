<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Roles
        DB::table('permissions')->insert([
            'name' => 'list.roles',
            'slug' => 'list.roles',
            'description' => 'list.roles'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.roles',
            'slug' => 'create.roles',
            'description' => 'create.roles'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.roles',
            'slug' => 'edit.roles',
            'description' => 'edit.roles'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.roles',
            'slug' => 'delete.roles',
            'description' => 'delete.roles'
        ]);
        
        // Usuarios
        DB::table('permissions')->insert([
            'name' => 'list.user',
            'slug' => 'list.user',
            'description' => 'list.user'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.user',
            'slug' => 'create.user',
            'description' => 'create.user'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.user',
            'slug' => 'edit.user',
            'description' => 'edit.user'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.user',
            'slug' => 'delete.user',
            'description' => 'delete.user'
        ]);

        // Estudiantes
        DB::table('permissions')->insert([
            'name' => 'list.student',
            'slug' => 'list.student',
            'description' => 'list.student'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.student',
            'slug' => 'create.student',
            'description' => 'create.student'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.student',
            'slug' => 'edit.student',
            'description' => 'edit.student'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.student',
            'slug' => 'delete.student',
            'description' => 'delete.student'
        ]);
           
        // Servicios
        DB::table('permissions')->insert([
            'name' => 'list.service',
            'slug' => 'list.service',
            'description' => 'list.service'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.service',
            'slug' => 'create.service',
            'description' => 'create.service'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.service',
            'slug' => 'edit.service',
            'description' => 'edit.service'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.service',
            'slug' => 'delete.service',
            'description' => 'delete.service'
        ]);

        // Reportes
        DB::table('permissions')->insert([
            'name' => 'list.report',
            'slug' => 'list.report',
            'description' => 'list.report'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.report',
            'slug' => 'create.report',
            'description' => 'create.report'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.report',
            'slug' => 'edit.report',
            'description' => 'edit.report'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.report',
            'slug' => 'delete.report',
            'description' => 'delete.report'
        ]);
        
        // Config
        DB::table('permissions')->insert([
            'name' => 'list.config',
            'slug' => 'list.config',
            'description' => 'list.config'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.config',
            'slug' => 'edit.config',
            'description' => 'edit.config'
        ]);
        
        // Instructores
        DB::table('permissions')->insert([
            'name' => 'list.instructor',
            'slug' => 'list.instructor',
            'description' => 'list.instructor'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.instructor',
            'slug' => 'create.instructor',
            'description' => 'create.instructor'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.instructor',
            'slug' => 'edit.instructor',
            'description' => 'edit.instructor'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.instructor',
            'slug' => 'delete.instructor',
            'description' => 'delete.instructor'
        ]);
        
        // Courses
        DB::table('permissions')->insert([
            'name' => 'list.course',
            'slug' => 'list.course',
            'description' => 'list.course'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.course',
            'slug' => 'create.course',
            'description' => 'create.course'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.course',
            'slug' => 'edit.course',
            'description' => 'edit.course'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.course',
            'slug' => 'delete.course',
            'description' => 'delete.course'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'payment.course',
            'slug' => 'payment.course',
            'description' => 'payment.course'
        ]);
        
        // Inscriptions
        DB::table('permissions')->insert([
            'name' => 'list.inscription',
            'slug' => 'list.inscription',
            'description' => 'list.inscription'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.inscription',
            'slug' => 'create.inscription',
            'description' => 'create.inscription'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.inscription',
            'slug' => 'edit.inscription',
            'description' => 'edit.inscription'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.inscription',
            'slug' => 'delete.inscription',
            'description' => 'delete.inscription'
        ]);
        DB::table('permissions')->insert([
            'name' => 'update_values.inscription',
            'slug' => 'update_values.inscription',
            'description' => 'update_values.inscription'
        ]);
        
        //Payment
        DB::table('permissions')->insert([
            'name' => 'list.payment',
            'slug' => 'list.payment',
            'description' => 'list.payment'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.payment',
            'slug' => 'create.payment',
            'description' => 'create.payment'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.payment',
            'slug' => 'edit.payment',
            'description' => 'edit.payment'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.payment',
            'slug' => 'delete.payment',
            'description' => 'delete.payment'
        ]);

        //Caja
        DB::table('permissions')->insert([
            'name' => 'cash.register',
            'slug' => 'cash.register',
            'description' => 'cash.register'
        ]);

        DB::table('permissions')->insert([
            'name' => 'generate.payment',
            'slug' => 'generate.payment',
            'description' => 'generate.payment'
        ]);
        
        //types services
        DB::table('permissions')->insert([
            'name' => 'list.typeservice',
            'slug' => 'list.typeservice',
            'description' => 'list.typeservice'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.typeservice',
            'slug' => 'create.typeservice',
            'description' => 'create.typeservice'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.typeservice',
            'slug' => 'edit.typeservice',
            'description' => 'edit.typeservice'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.typeservice',
            'slug' => 'delete.typeservice',
            'description' => 'delete.typeservice'
        ]);     
        
        //types levels
        DB::table('permissions')->insert([
            'name' => 'list.typelevel',
            'slug' => 'list.typelevel',
            'description' => 'list.typelevel'
        ]);

        DB::table('permissions')->insert([
            'name' => 'create.typelevel',
            'slug' => 'create.typelevel',
            'description' => 'create.typelevel'
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit.typelevel',
            'slug' => 'edit.typelevel',
            'description' => 'edit.typelevel'
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete.typelevel',
            'slug' => 'delete.typelevel',
            'description' => 'delete.typelevel'
        ]);        
        
        DB::table('permissions')->insert([
            'name' => 'Payment history student',
            'slug' => 'paymenthistory.student',
            'description' => 'Payment history student'
        ]);          
        
        DB::table('permissions')->insert([
            'name' => 'My courses student',
            'slug' => 'mycourses.student',
            'description' => 'My courses student'
        ]);          
        
    }
}
