<?php

return [

    'full_name'                   => 'Full name',
    'email'                       => 'Email',
    'password'                    => 'Password',
    'retype_password'             => 'Retype password',
    'remember_me'                 => 'Remember Me',
    'register'                    => 'Register',
    'register_a_new_membership'   => 'Register a new membership',
    'i_forgot_my_password'        => 'I forgot my password',
    'i_already_have_a_membership' => 'I already have a membership',
    'sign_in'                     => 'Sign In',
    'log_out'                     => 'Log Out',
    'toggle_navigation'           => 'Toggle navigation',
    'login_message'               => 'Sign in to start your session',
    'register_message'            => 'Register a new membership',
    'password_reset_message'      => 'Reset Password',
    'reset_password'              => 'Reset Password',
    'send_password_reset_link'    => 'Send Password Reset Link',
    'verify_message'              => 'Your account needs a verification',
    'verify_email_sent'                => 'A fresh verification link has been sent to your email address.',
    'verify_check_your_email'     => 'Before proceeding, please check your email for a verification link.',
    'verify_if_not_recieved'      => 'If you did not receive the email',
    'verify_request_another'      => 'click here to request another',
    
     //general
     'name'                        => 'Name',
     'state'                       => 'State',
     'description'                 => 'Description',    
     'save'                        => 'Save',
     'active'                      => 'Active',
     'inactive'                    => 'Inactive',
     'select'                      => '--Select--',
     'return'                      => 'Return',
     'creationDate'                => 'Creation date',
     'edit'                        => 'Edit',
     'delete'                      => 'Delete',    
     'administration'              => 'Administration',
     'start'                       => 'Start',
     'create'                      => 'Create',
     'cancel'                      => 'Cancel', 
     'accept'                      => 'Accept',   
     'file'                        => 'File',
     'size'                        => 'Size',
     'date'                        => 'Date',
     'download'                    => 'Download',
     'list'                        => 'List',
     'all'                         => 'All',
     'close'                       => 'Close',
     'profile'                     => 'Profile',
     'user_profile'                => 'User Profile',
     'roles'                       => 'Roles',
     'information'                 => 'Information',
     'password'                    => 'Password',
     'confirm_password'            => 'Confirm Password',
     'setting'                     => 'Settings',
     'select'                      => 'Select',
     'yes'                         => 'Yes',
     'no'                          => 'No',
     'timeline'                    => 'Timeline',
     'add'                         => 'Add',
     //rastro de miga
     'home'                        => 'Administration',    
     
     // messages confirm delete
     'confirm_msg_delete_1'        => 'Are you sure?',
     'confirm_msg_delete_2'        => 'Do you really want to delete this record? This process cannot be undone.',
 
     // messages confirm edit
     'confirm_msg_update_1'        => 'Are you sure?',
     'confirm_msg_update_2'        => 'Do you really want to update this record?',
 
     'dateStart'                   => 'Date Start',
     'dateEnd'                     => 'Date End',
     'filter'                      => 'Filter',
     'app_msj_not_permissions'     => 'You do not have permission to execute this action',
     'language'                    => 'Language',
     'of'                          => 'Of',
     'numNotifications'            => 'You have XX notifications',
     'viewAll'                     => 'View all',
     'birthdayClients'             => 'Birthday client(s)',
     'clientsNotes'                => 'Clients notes',
     'pendingDocuments'            => 'Pending documents',    
     'english'                     => 'English',
     'spanish'                     => 'Spanish',
     'seeDeletedClients'           => 'See deleted clients',
     'consult'                     => 'Consult',
     'registerTimeClock'           => 'Register time clock',
     'switchOffice'                => 'Switch office',
     'dynamicQueries'              => 'Dynamic queries',
     'viewDynamicQueries'          => 'View dynamic queries',
     'globalInformationLookup'     => 'Global information lookup',
     'refresh'                     => 'Refresh',
     'criteria'                    => 'Criteria',
     'alerts_confirmButtonText'    => 'Ok',
     'configurations'              => 'Configurations',
     'generateCertificate'         => 'Generate certificate',
];
