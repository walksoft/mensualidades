<?php

/* 
 *Constantes para el modulo - Uusarios
 * 
 */

return [

    'users'                       => 'Usuarios',
    'createRol'                   => 'Crear Usuario',
    'editRole'                    => 'Editar Usuario',
    'deleteeRole'                 => 'Eliminar Usuario',
    'selectRoles'                 => 'Seleccionar roles',  
    'user_successfully_created'   => 'Usuario creado con &eacute;xito',
    'user_successfully_updated'   => 'Usuario actualizado con &eacute;xito',
    'perfil_successfully_updated' => 'Perfil actualizado con &eacute;xito',
    'user_successfully_delete'    => 'Usuario borrado con &eacute;xito',
    'permissions'                 => 'Permisos', 
    'profile'                     => 'Mi perfil',
    'observations'                => 'Observaciones',
        
];