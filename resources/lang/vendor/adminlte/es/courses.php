<?php

/* 
 *Constantes para el modulo - Instructores
 * 
 */

return [

    'courses'                          => 'Cursos',
    'activeCourses'                    => 'Cursos activos',
    'course'                           => 'Curso',
    'createCourse'                     => 'Crear Curso',
    'editCourse'                       => 'Editar Curso',
    'deleteCourse'                     => 'Eliminar Curso', 
    'course_successfully_created'      => 'Curso creado con &eacute;xito',
    'course_successfully_updated'      => 'Curso actualizado con &eacute;xito',
    'course_successfully_delete'       => 'Curso borrado con &eacute;xito',
    'lastName'                         => 'Apellido',  
    'observations'                     => 'Observaciones',
    'code'                             => 'Código',
    'class'                            => 'Clase',
    'startTime'                        => 'Hora de inicio',
    'endTime'                          => 'Hora final',
    'status'                           => 'Estado',
    'frequency'                        => 'Frecuencia',
    'observations'                     => 'Observaciones',
    'schedule'                         => 'Horario',
    'payment_instructor'               => 'Pago instructor',
    'payment'                          => 'Pago',
    'lastPayment'                      => 'Ultimo pago',
        
];