<?php

/* 
 *Constantes para el modulo - Instructores
 * 
 */

return [

    'instructors'                      => 'Instructores',
    'instructor'                       => 'Instructor',
    'createInstructor'                 => 'Crear Instructor',
    'editInstructor'                   => 'Editar Instructor',
    'deleteInstructor'                 => 'Eliminar Instructor', 
    'instructor_successfully_created'  => 'Instructor creado con &eacute;xito',
    'instructor_successfully_updated'  => 'Instructor actualizado con &eacute;xito',
    'instructor_successfully_delete'   => 'Instructor borrado con &eacute;xito',
    'lastName'                         => 'Apellido',  
    'observations'                     => 'Observaciones',
    'lastName'                         => 'Apellido',
    'annexed'                          => 'Anexo',
        
];