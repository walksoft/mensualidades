<?php

/* 
 *Constantes para el modulo - Uusarios
 * 
 */

return [

    'students'                      => 'Alumnos',
    'student'                       => 'Alumno',
    'infStudent'                    => 'Información alumno',
    'createStudent'                 => 'Crear Alumno',
    'editStudent'                   => 'Editar Alumno',
    'deleteStudent'                 => 'Eliminar Alumno', 
    'student_successfully_created'  => 'Alumno creado con &eacute;xito',
    'student_successfully_updated'  => 'Alumno actualizado con &eacute;xito',
    'student_successfully_delete'   => 'Alumno borrado con &eacute;xito',
    'observations'                  => 'Observaciones',
    'lastName'                      => 'Apellido',    
    'curp'                          => 'CURP',
    'birthCertificate'              => 'Acta de nacimiento',    
    'tutor'                         => 'Tutor',
    'infTutor'                      => 'Informacion tutor',
    'tutorName'                     => 'Nombre tutor',
    'tutorLastName'                 => 'Apellido tutor',    
    'tutorEmail'                    => 'Email tutor',
    'tutorPhone'                    => 'Teléfono tutor',
    'tutorAddress'                  => 'Dirección tutor', 
    'relationship'                  => 'Parentesco', 
    'payments'                      => 'Pagos',
    'courses'                       => 'Cursos',
    'my_courses'                    => 'Mis cursos',
    'my_payments'                   => 'Mis pagos',
    'payment_history'               => 'Histórico de pagos',
        
];