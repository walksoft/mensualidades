<?php

/* 
 *Constantes para el modulo - roles
 * 
 */

return [

    'configurations'              => 'Configuraciones',
    'configuration'               => 'Configuración',
    'slogan'                      => 'Eslogan',
    'paymentsMethods'             => 'Métodos de pago', 
    'collectionProcess'           => 'Proceso de cobros',
    'id_paypal'                   => 'ID cliente PayPal',
    'secret_paypal'               => 'Secreto cliente PayPal',
    'sandbox_paypal'              => 'Modo sandbox de Paypal',
    'cuttingDay'                  => 'Día de corte',
    'paydayLimit'                 => 'Día limite de pago',
    'runProcess'                  => 'Ejecutar proceso',
    'config_successfully_updated' => 'Configuración actualizada con &eacute;xito',
    'config_successfully_payments_process' => 'Proceso ejecutado exitosamente',
    'smtp' =>'SMTP',
    'host' =>'Host',
    'port' =>'Port',    
    'username' =>'User name',
    'password' =>'Password',
    'encryption' => 'Encryption',
    'notifications' => 'Notificaciones',
    'after_making_a_payment' => 'Después de realizar un pago',
    'when_generating_a_payment_receipt' => 'Al generar un recibo de pago',
    'pending_payment_reminder' => 'Recordatorio de pago pendiente',
    'keywords' => 'Palabras clave',
    'subject' => 'Asunto',
    'report_past_due_payments' => 'Notificar pagos vencidos',
    'processes' => 'Procesos',
    'paymentToInstructors' => 'Pago a instructores',
    'paymentToInstructors' => 'Pago a instructores',
    'payday' => 'Día de pago',
    'month' => 'Mes',
    'msg_select_month_for_process' => 'Debe seleccionar el mes para la ejecución del proceso',
];