<?php

/* 
 *Constantes para el modulo - Servicios
 * 
 */

return [

    'services'                       => 'Servicios',
    'createService'                  => 'Crear Servicio',
    'editService'                    => 'Editar Servicio',
    'deleteeService'                 => 'Eliminar Servicio',
    'service_successfully_created'   => 'Servicio creado con &eacute;xito',
    'type_service_successfully_created'   => 'Tipo de servicio creado con &eacute;xito',
    'level_service_successfully_created'  => 'Nivel de servicio creado con &eacute;xito',    
    'service_successfully_updated'   => 'Servicio actualizado con &eacute;xito',
    'service_successfully_delete'    => 'Servicio borrado con &eacute;xito',    
    'type_successfully_delete'    => 'Tipo de servicio borrado con &eacute;xito',
    'level_successfully_delete'    => 'Nivel de servicio borrado con &eacute;xito',    
    'services'                       => 'Servicios',
    'services'                       => 'Servicios',
    'duration'                       => 'Duración sesión (min)',
    'type'                           => 'Tipo',
    'types'                          => 'Tipos',
    'typesOfServices'                => 'Tipos de servicios',
    'level'                          => 'Nivel',
    'levels'                         => 'Niveles',
    'monthlyCharge'                  => 'Cobro mensual',
    'promptPaymentCollection'        => 'Cobro pronto pago',
    'inscription'                    => 'Inscripción',
    'status'                         => 'Estado',    
    'type_service_associated_services'   => 'El tipo de servicio está asociado a servicios',
    'level_service_associated_services'   => 'El nivel de servicio está asociado a servicios',
    
    
];