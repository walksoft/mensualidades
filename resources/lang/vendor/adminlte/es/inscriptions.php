<?php

/* 
 *Constantes para el modulo - Instructores
 * 
 */

return [

    'inscriptions'                     => 'Inscripciones',
    'inscription'                     => 'Inscripcion',
    'inscribe'                         => 'Inscribir',
    'createInscription'                => 'Crear inscripcion',
    'editInscription'                  => 'Editar inscripcion',
    'deleteInscription'                => 'Eliminar inscripcion', 
    'inscription_successfully_created' => 'Alumno inscrito con &eacute;xito',
    'inscription_successfully_updated' => 'Inscripcion actualizada con &eacute;xito',
    'inscription_successfully_delete'  => 'Inscripcion borrada con &eacute;xito',
    'lastName'                         => 'Apellido',  
    'observations'                     => 'Observaciones',
    'code'                             => 'Código',
    'class'                            => 'Clase',
    'startTime'                        => 'Hora de inicio',
    'endTime'                          => 'Hora final',
    'status'                           => 'Estado',
    'frequency'                        => 'Frecuencia',
    'observations'                     => 'Observaciones',
    'schedule'                         => 'Horario',
    'modifyPrices'                     => 'Modificar precios',
        
];