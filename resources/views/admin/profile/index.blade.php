@extends('adminlte::page')
@section('title',trans('adminlte::users.profile'))
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileinput/css/fileinput.min.css') }}">
<style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@stop
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fas fa-fw fa-user" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::users.profile')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::users.profile')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="row">
<div class="col-md-12">
<div class="card">
    <div class="card-body">
<div id="kv-avatar-errors-2" class="center-block" style="width:800px;display:none"></div>
<form class="form" id="form-profile" name="form-profile" action="" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="row">
        <div class="col-sm-4 text-center">
            <div class="kv-avatar">
                <div class="file-loading">
                    <input id="avatar" name="avatar" type="file">
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" required="required" value="{{Auth::user()->name}}" placeholder="{{trans('adminlte::adminlte.name')}}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="email" class="control-label">{{trans('adminlte::adminlte.email')}}<span class="kv-reqd">*</span></label>
                        <input type="email" class="form-control" name="email" required="required" value="{{Auth::user()->email}}" placeholder="{{trans('adminlte::adminlte.email')}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password" class="control-label">{{trans('adminlte::adminlte.password')}}</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="{{trans('adminlte::adminlte.password')}}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password_confirmation" class="control-label">{{trans('adminlte::adminlte.confirm_password')}}</label>
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="{{trans('adminlte::adminlte.confirm_password')}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" id="btn-save-profile" name="btn-save-profile" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </div>
</form>
</div>
</div>
</div>
    </div>
@stop 
@section('js')
<script src="{{ asset('vendor/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
<script type="text/javascript"> 
    
    $(function(){    
        
        $('#form-profile').submit(function(e){
            
            e.preventDefault();

            var formData = new FormData(this);
            
            $.ajax({
                headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
                url:"{{route('users.profile.update')}}",
                type:'POST',
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                datatype:'JSON',
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete: function(){
                    $('.contaninerLoading').hide(); 
                },
                success: function(data){                                
                    if(data.status === 1){                        
                        msgNotification(data.type,data.msg);
                        $('#avatar').fileinput('reset');
                        $(".avatar-upload").attr('src','{{route("users.avatar",["id"=>Auth::user()->id])}}?'+new Date().getTime());
                        $(".user-image").attr('src','{{route("users.avatar",["id"=>Auth::user()->id])}}?'+new Date().getTime());
                    }//if
                    else{
                        ctlErrorRequestAjax(data);
                    }//else
                },
                error: function (xhr,textStatus,thrownError) {
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
            });
        });
    
        $("#avatar").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            showBrowse: false,
            browseOnZoneClick: true,
            removeLabel: '',
            removeIcon: '<i class="fas fa-trash-alt"></i>',
            removeTitle: 'Eliminar',
            elErrorContainer: '#kv-avatar-errors-2',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img id="avatar-upload" class="avatar-upload" src="{{route("users.avatar",["id"=>Auth::user()->id])}}?'+new Date().getTime()+'" alt="Modificar"><h6 class="text-muted">Modificar</h6>',
            layoutTemplates: {main2: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"],
            msgZoomModalHeading:'Vista previa',
            fileActionSettings:{zoomIcon: '<i class="fas fa-search-plus"></i>'},
            previewZoomButtonIcons:{
                toggleheader:'<i class="fas fa-arrows-alt-v"></i>',
                fullscreen:'<i class="fas fa-expand-arrows-alt"></i>',
                borderless:'<i class="fas fa-expand"></i>',
                close:'<i class="far fa-window-close"></i>'
            }
        });    
    });
</script>
@stop 

