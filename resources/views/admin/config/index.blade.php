@extends('adminlte::page')
@section('title',trans('adminlte::adminlte.configurations'))
@section('css')
<link href="{{ asset('vendor/summernote/dist/summernote-lite.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileinput/css/fileinput.min.css') }}">
<style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}

</style>
@stop
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fas fa-cogs" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::configurations.configurations')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::configurations.configurations')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')

    <div class="card">
            <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab"><i class="fas fa-cogs"></i> {{trans('adminlte::configurations.configuration')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab"><i class="fas fa-credit-card"></i> {{trans('adminlte::configurations.paymentsMethods')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#payments" data-toggle="tab"><i class="fas fa-cogs"></i> {{trans('adminlte::configurations.processes')}}</a></li>                  
                  <li class="nav-item"><a class="nav-link" href="#smtp" data-toggle="tab"><i class="fas fa-envelope"></i> {{trans('adminlte::configurations.smtp')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#notifications" data-toggle="tab"><i class="fas fa-envelope"></i> {{trans('adminlte::configurations.notifications')}}</a></li>
                </ul>
            </div><!-- /.card-header -->

            <div class="card-body">

                <div class="tab-content">

                  <div class="active tab-pane" id="activity">

                    <div id="kv-avatar-errors-2" class="center-block" style="width:800px;display:none"></div>
                    <form class="form form-vertical submitForms" action="/site/avatar-upload/2" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-sm-4 text-center">
                                <div class="kv-avatar">
                                    <div class="file-loading">
                                        <input id="config_avatar" name="config_avatar" type="file">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="config_name" class="control-label">{{trans('adminlte::adminlte.name')}}</label>
                                            <input type="text" maxlength="100" class="form-control" name="config_name"  value="@if($parameter['config_name']['value']){{$parameter['config_name']['value']}}@endif" placeholder="{{trans('adminlte::adminlte.name')}}" @if(!auth()->user()->hasPermissionTo('edit.config')) disabled="disabled" @endif>
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="config_direction" class="control-label">{{trans('adminlte::adminlte.direction')}}</label>
                                            <input type="text" maxlength="200" class="form-control" id="config_direction" name="config_direction"  value="@if(isset($parameter['config_direction']['value'])){{$parameter['config_direction']['value']}}@endif" placeholder="{{trans('adminlte::adminlte.direction')}}" @if(!auth()->user()->hasPermissionTo('edit.config')) disabled="disabled" @endif>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                     <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="config_slogan" class="control-label">{{trans('adminlte::configurations.slogan')}}</label>
                                            <input type="text" maxlength="300" class="form-control" name="config_slogan" id="config_slogan" value="@if(isset($parameter['config_slogan']['value'])){{$parameter['config_slogan']['value']}}@endif" placeholder="{{trans('adminlte::configurations.slogan')}}" @if(!auth()->user()->hasPermissionTo('edit.config')) disabled="disabled" @endif >
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="config_email" class="control-label">{{trans('adminlte::adminlte.email')}}</label>
                                            <input type="email" maxlength="190"  class="form-control" name="config_email"  value="@if(isset($parameter['config_email']['value'])){{$parameter['config_email']['value']}}@endif" placeholder="{{trans('adminlte::adminlte.email')}}" @if(!auth()->user()->hasPermissionTo('edit.config')) disabled="disabled" @endif>
                                        </div>
                                    </div>
                               
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="config_phone" class="control-label">{{trans('adminlte::adminlte.phone')}}</label>
                                            <input type="text" maxlength="10" class="form-control" name="config_phone"  value="@if(isset($parameter['config_phone']['value'])){{$parameter['config_phone']['value']}}@endif" placeholder="{{trans('adminlte::adminlte.phone')}}" @if(!auth()->user()->hasPermissionTo('edit.config')) disabled="disabled" @endif>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                  


                  </div>

                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="timeline">
        
                            <div class="row">

                            
                                <div class="col-sm-9">
                                    <div class="row">

                                    
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                            
                                                <label for="config_id_paypal" class="control-label">{{trans('adminlte::configurations.id_paypal')}}</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i style="padding-top:5px;" class="fa fa-key fa-lg"></i></span>
                                                    <input type="text" class="form-control" id="config_id_paypal" name="config_id_paypal"  maxlength="250" value="@if(isset($parameter['config_id_paypal']['value'])){{$parameter['config_id_paypal']['value']}}@endif" placeholder="{{trans('adminlte::configurations.id_paypal')}}" @if(!auth()->user()->hasPermissionTo('edit.config')) disabled="disabled" @endif>
                                                </div>
                                            </div>
                                    
                                            <div class="form-group">
                                                <label for="config_secret_paypal" class="control-label">{{trans('adminlte::configurations.secret_paypal')}}</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i style="padding-top:5px;" class="fa fa-unlock-alt fa-lg"></i></span>
                                                    <input type="text" maxlength="250" class="form-control" name="config_secret_paypal"  value="@if(isset($parameter['config_secret_paypal']['value'])){{$parameter['config_secret_paypal']['value']}}@endif" placeholder="{{trans('adminlte::configurations.secret_paypal')}}" @if(!auth()->user()->hasPermissionTo('edit.config')) disabled="disabled" @endif>
                                                </div>
                                            </div>
                        

                                            <div class="form-group">
                                                <label class="control-label">{{trans('adminlte::configurations.sandbox_paypal')}}</label>
                                                <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="config_sandbox_paypal" name="config_sandbox_paypal" value="1" @if(!auth()->user()->hasPermissionTo('edit.config')) disabled="disabled" @endif  @if (isset($parameter['config_sandbox_paypal']['value'])) checked @endif>
                                                <label class="custom-control-label" for="config_sandbox_paypal"></label>
                                                </div>
                                            </div>


                                        </div>


                                    </div>
                 
                         
                                </div>
                            </div>
                    

                    </div>
                  <!-- /.tab-pane -->   

                    <div class="tab-pane" id="payments">
                        <div class="row">
                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="fas fa-business-time"></i> {{trans('adminlte::configurations.collectionProcess')}}</h3>
                                    </div>  
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <button type="button" id="" name="" class="btn btn-info payments_process"><i class="fas fa-cog"></i> {{trans('adminlte::configurations.runProcess')}}</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <label for="cutting_day" class="control-label">{{trans('adminlte::configurations.cuttingDay')}}</label>
                                                <input type="number" value="@if(isset($parameter['cutting_day']['value'])){{$parameter['cutting_day']['value']}}@endif" id="cutting_day" name="cutting_day" class="form-control" min="1" max="28" placeholder="{{trans('adminlte::configurations.cuttingDay')}}">                                
                                            </div>
                                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <label for="payment_deadline" class="control-label">{{trans('adminlte::configurations.paydayLimit')}}</label>
                                                <input type="number" value="@if(isset($parameter['payment_deadline']['value'])){{$parameter['payment_deadline']['value']}}@endif" id="payment_deadline" name="payment_deadline" class="form-control" min="1" max="28" placeholder="{{trans('adminlte::configurations.paydayLimit')}}">                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="fas fa-file-invoice-dollar"></i> {{trans('adminlte::configurations.paymentToInstructors')}}</h3>
                                    </div>  
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <button type="button" id="" name="" class="btn btn-info instructors_payment_process"><i class="fas fa-cog"></i> {{trans('adminlte::configurations.runProcess')}}</button>
                                            </div>
                                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">                                                
                                                <select id="" name="" class="form-control select2 select2-hidden-accessible month_pay_instructors" data-placeholder="{{trans('adminlte::adminlte.month')}}" style="width:100%;">
                                                    <option value=""></option> 
                                                    <option @if(date('m') == '01') selected="selected" @endif value="01">{{trans('adminlte::adminlte.January')}}</option> 
                                                    <option @if(date('m') == '02') selected="selected" @endif value="02">{{trans('adminlte::adminlte.February')}}</option> 
                                                    <option @if(date('m') == '03') selected="selected" @endif value="03">{{trans('adminlte::adminlte.March')}}</option> 
                                                    <option @if(date('m') == '04') selected="selected" @endif value="04">{{trans('adminlte::adminlte.April')}}</option>
                                                    <option @if(date('m') == '05') selected="selected" @endif value="05">{{trans('adminlte::adminlte.May')}}</option> 
                                                    <option @if(date('m') == '06') selected="selected" @endif value="06">{{trans('adminlte::adminlte.June')}}</option> 
                                                    <option @if(date('m') == '07') selected="selected" @endif value="07">{{trans('adminlte::adminlte.July')}}</option> 
                                                    <option @if(date('m') == '08') selected="selected" @endif value="08">{{trans('adminlte::adminlte.August')}}</option> 
                                                    <option @if(date('m') == '09') selected="selected" @endif value="09">{{trans('adminlte::adminlte.September')}}</option>
                                                    <option @if(date('m') == '10') selected="selected" @endif value="10">{{trans('adminlte::adminlte.October')}}</option> 
                                                    <option @if(date('m') == '11') selected="selected" @endif value="11">{{trans('adminlte::adminlte.November')}}</option>
                                                    <option @if(date('m') == '12') selected="selected" @endif value="12">{{trans('adminlte::adminlte.December')}}</option> 
                                                </select>
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <label for="court_day_pay_instructors" class="control-label">{{trans('adminlte::configurations.cuttingDay')}}</label>
                                                <input type="number" value="@if(isset($parameter['court_day_pay_instructors']['value'])){{$parameter['court_day_pay_instructors']['value']}}@endif" id="court_day_pay_instructors" name="court_day_pay_instructors" class="form-control" min="1" max="28" placeholder="{{trans('adminlte::configurations.cuttingDay')}}">                                
                                            </div>
                                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <label for="instructor_payday" class="control-label">{{trans('adminlte::configurations.payday')}}</label>
                                                <input type="number" value="@if(isset($parameter['instructor_payday']['value'])){{$parameter['instructor_payday']['value']}}@endif" id="instructor_payday" name="instructor_payday" class="form-control" min="1" max="28" placeholder="{{trans('adminlte::configurations.payday')}}">                                
                                            </div>       
                                        </div>   
                                    </div>
                                </div>
                            </div>                                                            
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="far fa-calendar-times"></i> {{trans('adminlte::configurations.report_past_due_payments')}}</h3>
                                    </div>  
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <button type="button" id="" name="" class="btn btn-danger overdue_payments_process"><i class="fas fa-cog"></i> {{trans('adminlte::configurations.runProcess')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                        </div> 
                    </div>                
                  
                    <div class="tab-pane" id="smtp">
                        <div class="row">
                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <label for="smtp_host" class="control-label">{{trans('adminlte::configurations.host')}}</label>
                                <input type="text" value="@if(isset($parameter['smtp_host']['value'])){{$parameter['smtp_host']['value']}}@endif" id="smtp_host" name="smtp_host" class="form-control" placeholder="{{trans('adminlte::configurations.host')}}">                                
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <label for="smtp_port" class="control-label">{{trans('adminlte::configurations.port')}}</label>
                                <input type="number" value="@if(isset($parameter['smtp_port']['value'])){{$parameter['smtp_port']['value']}}@endif" id="smtp_port" name="smtp_port" class="form-control" placeholder="{{trans('adminlte::configurations.port')}}">                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <label for="smtp_username" class="control-label">{{trans('adminlte::configurations.username')}}</label>
                                <input type="text" value="@if(isset($parameter['smtp_username']['value'])){{$parameter['smtp_username']['value']}}@endif" id="smtp_username" name="smtp_username" class="form-control" placeholder="{{trans('adminlte::configurations.username')}}">                                
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <label for="smtp_password" class="control-label">{{trans('adminlte::configurations.password')}}</label>
                                <input type="password" value="@if(isset($parameter['smtp_password']['value'])){{$parameter['smtp_password']['value']}}@endif" id="smtp_password" name="smtp_password" class="form-control" placeholder="{{trans('adminlte::configurations.password')}}">                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <label for="smtp_encryption" class="control-label">{{trans('adminlte::configurations.encryption')}}</label>
                                <select id="smtp_encryption" name="smtp_encryption" class="form-control select2 select2-hidden-accessible" style="width:100%;">
                                    <option value=""></option>
                                    <option @if(isset($parameter['smtp_encryption']['value']) && $parameter['smtp_encryption']['value'] == 'ssl') selected="selected" @endif value="ssl">SSL</option>
                                    <option @if(isset($parameter['smtp_encryption']['value']) && $parameter['smtp_encryption']['value'] == 'tls') selected="selected" @endif value="tls">TLS</option>
                                </select>
                            </div>
                        </div>
                        
                    </div>
               
                  <div class="tab-pane" id="notifications">
                    <div class="row">
                        <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="" class="control-label">{{trans('adminlte::configurations.after_making_a_payment')}}</label>
                            <input type="text" value="@if(isset($parameter['subject_notification_after_paying']['value'])){{$parameter['subject_notification_after_paying']['value']}}@endif" id="subject_notification_after_paying" name="subject_notification_after_paying" class="form-control" placeholder="{{trans('adminlte::configurations.subject')}}">
                            <textarea class="text-summernote" name="notification_after_paying" id="notification_after_paying">@if(isset($parameter['notification_after_paying']['value'])){{$parameter['notification_after_paying']['value']}}@endif</textarea>
                        </div>
                        <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="" class="control-label">{{trans('adminlte::configurations.when_generating_a_payment_receipt')}}</label>
                            <input type="text" value="@if(isset($parameter['subject_notification_generate_payment']['value'])){{$parameter['subject_notification_generate_payment']['value']}}@endif" id="subject_notification_generate_payment" name="subject_notification_generate_payment" class="form-control" placeholder="{{trans('adminlte::configurations.subject')}}">
                            <textarea class="text-summernote" name="notification_generate_payment" id="notification_generate_payment">@if(isset($parameter['notification_generate_payment']['value'])){{$parameter['notification_generate_payment']['value']}}@endif</textarea>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="" class="control-label">{{trans('adminlte::configurations.pending_payment_reminder')}}</label>
                            <input type="text" value="@if(isset($parameter['subject_notification_payment_pending_reminder']['value'])){{$parameter['subject_notification_payment_pending_reminder']['value']}}@endif" id="subject_notification_payment_pending_reminder" name="subject_notification_payment_pending_reminder" class="form-control" placeholder="{{trans('adminlte::configurations.subject')}}">
                            <textarea class="text-summernote" name="notification_payment_pending_reminder" id="notification_payment_pending_reminder">@if(isset($parameter['notification_payment_pending_reminder']['value'])){{$parameter['notification_payment_pending_reminder']['value']}}@endif</textarea>
                        </div>
                        <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <br>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-keyboard"></i>&nbsp;&nbsp;{{trans('adminlte::configurations.keywords')}}</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <ul>
                                        <li><b>[PAGO_CONCEPTO]</b>: Concepto del pago o cobro generado</li>
                                        <li><b>[PAGO_VALOR]</b>: Valor total del pago o cobro generado</li>
                                        <li><b>[PAGO_FECHA_LIMITE]</b>: Fecha límite de pago</li>
                                        <li><b>[ALUMNO]</b>: Nombre del alumno</li>
                                        <li><b>[CURSO_NOMBRE]</b>: Nombre del curso</li>
                                        <li><b>[CURSO_CODIGO]</b>: Código del curso</li>
                                    </ul>
                                </div>
                                <!-- /.card-body -->
                            </div>                            
                        </div>
                    </div> 
                  

                </div>

          

            </div>
            
            
    </div>
    <div class="card-body form-group">
            @if(auth()->user()->hasPermissionTo('edit.config'))<button type="submit" class="btn btn-success">Guardar</button>@endif
    </div>
    </form>


 

@stop 

@section('js')
<script src="{{ asset('vendor/summernote/dist/summernote-lite.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
<script type="text/javascript"> 
    _token = "{{csrf_token()}}";
    $(document).ready(function(){
        //notification_after_paying
        $('.text-summernote').summernote({tabsize:2,height:150});
        
        $('.select2').select2();
        
        $(".instructors_payment_process").click(function(){
            
            $.ajax({
                headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
                url:"{{route('configurations.ajax.exec_instructors_payment_process')}}",
                type:'POST',
                cache:false,
                data:{'month_pay_instructors':$(".month_pay_instructors").val(),'_token':_token,'_method':'PUT'},
                datatype:'JSON',
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete:function(){
                    $('.contaninerLoading').hide();
                },
                success:function(data){                
                    if(data.status === 1){                        
                        msgNotification(data.type,data.msg);
                    }//if
                    else{
                        ctlErrorRequestAjax(data);
                    }//else
                },
                error:function(xhr,textStatus,thrownError){
                    $('.contaninerLoading').hide();
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
            }); 
        });                
        
        $(".payments_process").click(function(){
            $.ajax({
                headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
                url:"{{route('configurations.ajax.exec_payments_process')}}",
                type:'POST',
                cache:false,
                data:{'_token':_token,'_method':'PUT'},
                datatype:'JSON',
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete:function(){
                    $('.contaninerLoading').hide();
                },
                success:function(data){                
                    if(data.status === 1){                        
                        msgNotification(data.type,data.msg);
                    }//if
                    else{
                        ctlErrorRequestAjax(data);
                    }//else
                },
                error:function(xhr,textStatus,thrownError){
                    $('.contaninerLoading').hide();
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
            }); 
        });

        $(".overdue_payments_process").click(function(){
            $.ajax({
                headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
                url:"{{route('configurations.ajax.exec_overdue_payments_process')}}",
                type:'POST',
                cache:false,
                data:{'_token':_token,'_method':'PUT'},
                datatype:'JSON',
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete:function(){
                    $('.contaninerLoading').hide();
                },
                success:function(data){                
                    if(data.status === 1){                        
                        msgNotification(data.type,data.msg);
                    }//if
                    else{
                        ctlErrorRequestAjax(data);
                    }//else
                },
                error:function(xhr,textStatus,thrownError){
                    $('.contaninerLoading').hide();
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
            }); 
        });        
        
        $('.submitForms').on('submit', function(e) {


            // evito que propague el submit
            e.preventDefault();
            
            // agrego la data del form a formData
            var formData = new FormData(this);
            formData.append('_token', $('input[name=_token]').val());


            
            $.ajax({
                type:'POST',
                url: "{{ route('configurations.ajax.update') }}",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete:function(){
                    $('.contaninerLoading').hide(); 
                },
                success:function(data){

                    if(data.status === 1){                        
                        msgNotification(data.type,data.msg);
                        $('#avatar').fileinput('reset');
                        @if(isset($parameter["config_avatar"]["value"]) && !empty($parameter["config_avatar"]["value"]))
                        $(".avatar-upload").attr('src','{{route("configurations.avatar",["id"=> $parameter["config_avatar"]["value"] ])}}?'+new Date().getTime());
                        @endif
                    }//if
                    else{
                        ctlErrorRequestAjax(data);
                    }//else
                    
                },
                error: function(jqXHR, text, error){
                    toastr.error('Validation error!', 'No se pudo Añadir los datos<br>' + error, {timeOut: 5000});
                }
            });


        });


        $('.submitFormsPayments').on('submit', function(e) {


            // evito que propague el submit
            e.preventDefault();

            // agrego la data del form a formData
            var formData = new FormData(this);
            formData.append('_token', $('input[name=_token]').val());



            $.ajax({
                type:'POST',
                url: "{{ route('configurations.ajax.updatePayments') }}",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete:function(){
                    $('.contaninerLoading').hide(); 
                },
                success:function(data){

                if(data.status === 1){                        
                    msgNotification(data.type,data.msg);
                }//if
                else{
                    ctlErrorRequestAjax(data);
                }//else
                
                },
                error: function(jqXHR, text, error){
                    toastr.error('Validation error!', 'No se pudo Añadir los datos<br>' + error, {timeOut: 5000});
                }
            });

        });

    });

    $("#config_avatar").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="fas fa-trash-alt"></i>',
        removeTitle: 'Eliminar',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '@if(isset($parameter["config_avatar"]["value"]) && !empty($parameter["config_avatar"]["value"])) <img style="width:auto;height:auto;max-width:100%;max-height:100%;" id="avatar-upload" class="avatar-upload" src="{{route("configurations.avatar",["id"=> $parameter["config_avatar"]["value"] ])}}?'+new Date().getTime()+'" alt="Modificar"><h6 class="text-muted">Modificar</h6> @else <i class="fas fa-file-image text-info" style="padding:20px;font-size: 6em;line-height: 1;"></i><h6 class="text-muted">Modificar</h6> @endif ',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"],
        msgZoomModalHeading:'Vista previa',
        fileActionSettings:{zoomIcon: '<i class="fas fa-search-plus"></i>'},
        previewZoomButtonIcons:{
            toggleheader:'<i class="fas fa-arrows-alt-v"></i>',
            fullscreen:'<i class="fas fa-expand-arrows-alt"></i>',
            borderless:'<i class="fas fa-expand"></i>',
            close:'<i class="far fa-window-close"></i>'
        }
    });
</script>
@stop 