@extends('adminlte::page')
@section('title', 'Admin')
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="far fa-money-bill-alt" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::students.my_payments')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::students.my_payments')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-bordered dt-responsive table-head-fixed">
                            <thead>
                                <tr>
                                    <th style="width:10px;text-align:center">#</th>
                                    <th>{{trans('adminlte::payments.reference')}}</th>            
                                    <th style="text-align:center">{{trans('adminlte::payments.value')}}</th>          
                                    <th>{{trans('adminlte::payments.concept')}}</th>
                                    <th>{{trans('adminlte::payments.course')}}</th>
                                    <th>{{trans('adminlte::payments.status')}}</th>
                                    <th>{{trans('adminlte::payments.payment_method')}}</th>
                                    <th>{{trans('adminlte::payments.paidThe')}}</th>
                                </tr>
                            </thead>
                            <tbody>  
                                @php 
                                    $count = 1;
                                @endphp
                                @if(Auth::user()->records_payments->count() > 0)
                                    @foreach(Auth::user()->records_payments as $payment)
                                    <tr class="cell_table_record_cash_register">
                                            <td style="width:10px;text-align:center">{{$count++}}</td>
                                            <td>{{$payment->reference}}</td>
                                            <td style="text-align:center">${{number_format($payment->total_paid,0,',','.')}}</td>
                                            <td>{{$payment->concept->name}}</td>
                                            <td>{{$payment->course->code}} - {{$payment->course->service->name}}</td>
                                            <td>{{$payment->status->name}}</td>
                                            <td style="text-align:center">{{$payment->payment_method}}</td>
                                            <td>@if($payment->payment_date) {{\Carbon\Carbon::parse($payment->payment_date)->format('d/m/Y g:i A')}} @endif</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8">
                                            No se encontraron pagos realizados...
                                        </td>
                                    </tr>
                                @endif        
                            </tbody>
                        </table>                          
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
@stop