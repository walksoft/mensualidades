{{csrf_field()}}
{{ method_field('PUT') }}
<input type="hidden" name="id" id="id" value="{{$user->id}}">
<div class="row">
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="name_student" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
        <input type="text" value="{{$user->name}}" id="name_student" class="form-control" maxlength="190" name="name_student" placeholder="{{trans('adminlte::adminlte.name')}}">
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="lastName_student" class="control-label">{{trans('adminlte::students.lastName')}}</label>
        <input type="text" value="{{$user->last_name}}" id="lastName_student" class="form-control" maxlength="190" name="lastName_student" placeholder="{{trans('adminlte::students.lastName')}}">
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="email_student" class="control-label">{{trans('adminlte::adminlte.email')}}<span class="kv-reqd">*</span></label>
        <input type="email" value="{{$user->email}}" id="email_student" class="form-control" maxlength="190" name="email_student" placeholder="{{trans('adminlte::adminlte.email')}}">
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="phone_student" class="control-label">{{trans('adminlte::adminlte.phone')}}</label>
        <input type="text" value="{{$user->phone_number}}" id="phone_student" class="form-control" maxlength="190" name="phone_student" placeholder="{{trans('adminlte::adminlte.phone')}}">
    </div>    
</div>
<div class="row">
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="address_student" class="control-label">{{trans('adminlte::adminlte.direction')}}</label>
        <input type="text"  value="{{$user->address}}" id="address_student" class="form-control" minlength="8" maxlength="255" name="address_student" placeholder="{{trans('adminlte::adminlte.direction')}}">
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="status_student" class="control-label">{{trans('adminlte::adminlte.status')}}<span class="kv-reqd">*</span></label>
        <select id="status_student" name="status_student" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            <option @if($user->status == 1) selected="selected" @endif value="1">{{trans('adminlte::adminlte.active')}}</option>
            <option @if($user->status == 0) selected="selected" @endif value="0">{{trans('adminlte::adminlte.inactive')}}</option>
        </select>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="student_password" class="control-label">{{trans('adminlte::adminlte.password')}}<span class="kv-reqd">*</span></label>
        <input type="password" id="student_password" class="form-control" minlength="8" maxlength="255" name="student_password" placeholder="{{trans('adminlte::adminlte.password')}}">
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="student_password_confirmation" class="control-label">{{trans('adminlte::adminlte.confirm_password')}}<span class="kv-reqd">*</span></label>
        <input type="password" id="student_password_confirmation" class="form-control" minlength="8" maxlength="255" name="student_password_confirmation" placeholder="{{trans('adminlte::adminlte.confirm_password')}}">
    </div>      
</div>
<div class="row">    
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pagos-tab" data-toggle="tab" href="#tabEditPagos" role="tab" aria-controls="tabEditPagos" aria-selected="false">{{trans('adminlte::students.payments')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="cursos-tab" data-toggle="tab" href="#tabEditCursos" role="tab" aria-controls="tabEditCursos" aria-selected="false">{{trans('adminlte::students.courses')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="files-tab" data-toggle="tab" href="#files" role="tab" aria-controls="files" aria-selected="true">{{trans('adminlte::adminlte.files')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="editTutor-tab" data-toggle="tab" href="#tabEditTutor" role="tab" aria-controls="tabEditTutor" aria-selected="true">{{trans('adminlte::students.tutor')}}</a>
            </li>    
            <li class="nav-item">
                <a class="nav-link" id="observations-tab" data-toggle="tab" href="#tabObservations" role="tab" aria-controls="tabObservations" aria-selected="true">{{trans('adminlte::adminlte.observations')}}</a>
            </li>            
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tabEditPagos" role="tabpanel" aria-labelledby="pagos-tab">     
                <div class="card-body table-responsive p-0" id="table_cash_register" style="height:200px;">
                    <table class="table table-bordered dt-responsive table-head-fixed">
                        <thead>
                            <tr>
                                <th style="width:10px;text-align:center">#</th>
                                <th>{{trans('adminlte::payments.reference')}}</th>            
                                <th style="text-align:center">{{trans('adminlte::payments.value')}}</th>          
                                <th>{{trans('adminlte::payments.concept')}}</th>
                                <th>{{trans('adminlte::payments.course')}}</th>
                                <th>{{trans('adminlte::payments.status')}}</th>
                                <th>{{trans('adminlte::payments.payment_method')}}</th>                                
                                <th>{{trans('adminlte::payments.paymentDate')}}</th>
                            </tr>
                        </thead>
                        <tbody>        
                            @if($user->payments->count() > 0)
                                @foreach($user->payments as $payment)
                                <tr class="cell_table_record_cash_register">
                                        <td style="width:10px;text-align:center">{{$count++}}</td>
                                        <td>{{$payment->reference}}</td>
                                        @if($payment->total_paid)
                                            <td style="text-align:center">${{number_format($payment->total_paid,0,',','.')}}</td>                        
                                        @else
                                            <td style="text-align:center">${{number_format($payment->amount,0,',','.')}}</td>
                                        @endif
                                        <td>{{$payment->concept->name}}</td>
                                        <td>{{$payment->course->code}} - {{$payment->course->service->name}}</td>
                                        <td>{{$payment->status->name}}</td>
                                        <td>{{$payment->payment_method}}</td>
                                        <td>@if($payment->payment_date) {{\Carbon\Carbon::parse($payment->payment_date)->format('d/m/Y g:i A')}} @endif</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">
                                        No se encontraron pagos pendientes...
                                    </td>
                                </tr>
                            @endif        
                        </tbody>
                    </table>   
                </div>
            </div>
            <div class="tab-pane" id="tabEditCursos" role="tabpanel" aria-labelledby="cursos-tab">
                <table class="table table-bordered dt-responsive table-head-fixed">
                    <thead>
                        <tr>
                            <th>{{trans('adminlte::courses.code')}}</th>
                            <th>{{trans('adminlte::adminlte.name')}}</th>
                            <th>{{trans('adminlte::courses.schedule')}}</th>
                            <th>{{trans('adminlte::courses.frequency')}}</th>      
                            <th>{{trans('adminlte::inscriptions.inscription')}}</th>                                  
                            <th>{{trans('adminlte::adminlte.status')}}</th>
                            <th>{{trans('adminlte::adminlte.delete')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($user->registration->count() > 0)
                            @foreach($user->registration as $course)
                                <tr>
                                    <td>{{$course->course->code}}</td>
                                    <td>{{$course->course->service->name}}</td>
                                    <td>
                                        @php
                                            $start_time = $course->course->start_time ? \Carbon\Carbon::parse($course->course->start_time)->format('g:iA') : NULL;
                                            $end_time = $course->course->end_time ? \Carbon\Carbon::parse($course->course->end_time)->format('g:iA') : NULL;
                                        @endphp
                                        {{$start_time}} - {{$end_time}}
                                    </td>
                                    <td>
                                        @if($course->course->array_frequency())
                                           @foreach($course->course->array_frequency() as $day)
                                                <span class="badge badge-success">{{$day}}</span>
                                            @endforeach
                                        @endif                                    
                                    </td>   
                                    <td>{{\Carbon\Carbon::parse($course->created_at)->format('d/m/Y')}}</td>
                                    <td>
                                        @if($course->status)                        
                                            <span class="badge badge-success">{{trans('adminlte::adminlte.active')}}</span>
                                        @else
                                            <span class="badge badge-danger">{{trans('adminlte::adminlte.inactive')}}</span>
                                        @endif                    
                                    </td>
                                    <td>
                                        <a href="" class="confirmDelete-inscription" data-id="{{$course->code}}" data-toggle="modal" data-target="#confirmDelete-inscription"><span class="badge badge-danger">{{trans('adminlte::adminlte.delete')}}</span></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                        <tr>
                            <td colspan="7">Aun no se encuentran registrados cursos...</td>
                        </tr>
                        @endif
                    </tbody>
                </table>                
            </div>
            <div class="tab-pane" id="files" role="tabpanel" aria-labelledby="files-tab">
                <table class="table table-sm table-striped table-bordered dt-responsive">
                    <thead>                  
                        <tr>
                            <th>{{trans('adminlte::adminlte.file')}}</th>
                            <th><i class="fas fa-paperclip"></i></th>
                            <th><i class="fas fa-file-upload"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(array_key_exists('1',$files) || \App\Models\File_type::find(1)->status == 1)
                            <tr>
                                <td>{{trans('adminlte::students.curp')}}</td>
                                <td>
                                    <a target="_blank" href="@if(array_key_exists('1',$files) && $files[1]['id']) {{$files[1]['file_url']}} @endif" class="" name="curp_student" id="curp_student">@if(array_key_exists('1',$files) && $files[1]['file_name']) {{$files[1]['file_name']}} @endif</a>
                                </td>
                                <td>
                                    @if(\App\Models\File_type::find(1)->status == 1)
                                        <input type="file" id="curp_student" class="form-control-file" name="curp_student">
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if(array_key_exists('2',$files) || \App\Models\File_type::find(2)->status == 1)
                            <tr>
                                <td>{{trans('adminlte::students.birthCertificate')}}</td>
                                <td>
                                    <a target="_blank" href="@if(array_key_exists('2',$files) && $files[2]['id']) {{$files[2]['file_url']}} @endif" class="" name="curp_student" id="curp_student">@if(array_key_exists('2',$files) && $files[2]['file_name']) {{$files[2]['file_name']}} @endif</a>
                                </td>
                                <td>
                                    @if(\App\Models\File_type::find(2)->status == 1)
                                        <input type="file" id="birthCertificate_student" class="form-control-file" name="birthCertificate_student">
                                    @endif
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>                
            </div>
            <div class="tab-pane" id="tabEditTutor" role="tabpanel" aria-labelledby="editTutor-tab">                   
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <br>
                        <input type="hidden" value="@if($user->tutor && $user->tutor->id){{$user->tutor->id}} @endif" id="tutor_id" name="tutor_id">
                        <div class="row">
                            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <label for="tutorName_student" class="control-label">{{trans('adminlte::students.tutorName')}}</label>
                                <input type="text" value="@if($user->tutor && $user->tutor->name){{$user->tutor->name}} @endif" id="tutorName_student" class="form-control" maxlength="190" name="tutorName_student" placeholder="{{trans('adminlte::students.tutorName')}}">
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <label for="tutorLastName_student" class="control-label">{{trans('adminlte::students.tutorLastName')}}</label>
                                <input type="text" value="@if($user->tutor && $user->tutor->last_name){{$user->tutor->last_name}} @endif" id="tutorLastName_student" class="form-control" maxlength="190" name="tutorLastName_student" placeholder="{{trans('adminlte::students.tutorLastName')}}">
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <label for="tutorEmail_student" class="control-label">{{trans('adminlte::students.tutorEmail')}}</label>
                                <input type="email" value="@if($user->tutor && $user->tutor->email){{$user->tutor->email}} @endif" id="tutorEmail_student" class="form-control" maxlength="190" name="tutorEmail_student" placeholder="{{trans('adminlte::students.tutorEmail')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <label for="tutorPhone_student" class="control-label">{{trans('adminlte::students.tutorPhone')}}</label>
                                <input type="text" value="@if($user->tutor && $user->tutor->phone_number){{$user->tutor->phone_number}} @endif" id="tutorPhone_student" class="form-control" maxlength="190" name="tutorPhone_student" placeholder="{{trans('adminlte::students.tutorPhone')}}">
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <label for="tutorAddress_student" class="control-label">{{trans('adminlte::students.tutorAddress')}}</label>
                                <input type="text" value="@if($user->tutor && $user->tutor->address){{$user->tutor->address}} @endif" id="tutorAddress_student" class="form-control" minlength="8" maxlength="190" name="tutorAddress_student" placeholder="{{trans('adminlte::students.tutorAddress')}}">
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <label for="relationship_student" class="control-label">{{trans('adminlte::students.relationship')}}</label>
                                <select id="relationship_student" name="relationship_student" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
                                    <option value=""></option>
                                    @foreach(\App\Models\Kinship::where('status',1)->orderBy('name','ASC')->get() as $kinship)
                                        <option @if($user->tutor && $user->tutor->kinship_id && ($kinship->id == $user->tutor->kinship_id)) selected="selected "@endif value="{{$kinship->id}}">{{ $kinship->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                    </div>    
            </div>
            <div class="tab-pane" id="tabObservations" role="tabpanel" aria-labelledby="observations-tab">
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <br>
                    <textarea id="observations_student" class="form-control" rows="4" maxlength="1000" name="observations_student">{{$user->observations}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>