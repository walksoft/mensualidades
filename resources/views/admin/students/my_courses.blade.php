@extends('adminlte::page')
@section('title', 'Admin')
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fas fa-award" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::students.my_courses')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::students.my_courses')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-bordered dt-responsive table-head-fixed">
                            <thead>
                                <tr>
                                    <th>{{trans('adminlte::courses.code')}}</th>
                                    <th>{{trans('adminlte::adminlte.name')}}</th>
                                    <th>{{trans('adminlte::courses.schedule')}}</th>
                                    <th>{{trans('adminlte::courses.frequency')}}</th>      
                                    <th>{{trans('adminlte::inscriptions.inscription')}}</th>                                  
                                    <th>{{trans('adminlte::adminlte.status')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(Auth::user()->registration->count() > 0)
                                    @foreach(Auth::user()->registration as $course)
                                        <tr>
                                            <td>{{$course->course->code}}</td>
                                            <td>{{$course->course->service->name}}</td>
                                            <td>
                                                @php
                                                    $start_time = $course->course->start_time ? \Carbon\Carbon::parse($course->course->start_time)->format('g:iA') : NULL;
                                                    $end_time = $course->course->end_time ? \Carbon\Carbon::parse($course->course->end_time)->format('g:iA') : NULL;
                                                @endphp
                                                {{$start_time}} - {{$end_time}}
                                            </td>
                                            <td>
                                                @if($course->course->array_frequency())
                                                   @foreach($course->course->array_frequency() as $day)
                                                        <span class="badge badge-success">{{$day}}</span>
                                                    @endforeach
                                                @endif                                    
                                            </td>   
                                            <td>{{\Carbon\Carbon::parse($course->created_at)->format('d/m/Y')}}</td>
                                            <td>
                                                @if($course->status)                        
                                                    <span class="badge badge-success">{{trans('adminlte::adminlte.active')}}</span>
                                                @else
                                                    <span class="badge badge-danger">{{trans('adminlte::adminlte.inactive')}}</span>
                                                @endif                    
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td colspan="6">Aun no se encuentran cursos...</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
@stop