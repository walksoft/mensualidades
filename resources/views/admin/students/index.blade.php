@extends('adminlte::page')
@section('title',trans('adminlte::students.students'))
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fas fa-user-graduate " style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::students.students')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::students.students')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            @if($permits['create'])
                <a href="#" class="btn btn-block btn-success btnModalCreate" data-toggle="modal">{{trans('adminlte::adminlte.create')}}</a>
            @endif
        </div>
    </div>
    <div class="card-body">
        {!! $html->table(['class' => 'table table-striped table-bordered dt-responsive table-stundents', 'style' => 'width:100%']) !!}
    </div>
</div>
<div class="modal fade" id="modal-create-student" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fa fa-user-friends"></i> {{trans('adminlte::students.createStudent')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm-crate-student" name="frm-crate-student" enctype="multipart/form-data">
                <div class="modal-body" id="content-create-student"></div>            
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="btn-save-student" name="btn-save-student" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-edit-student" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fa fa-user-friends"></i> {{trans('adminlte::students.editStudent')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm-edit-student" name="frm-edit-student" enctype="multipart/form-data">
                <div class="modal-body" id="content-edit-student"></div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="btn-save-edit-student" name="btn-save-edit-student" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop
@section('js')
{!! $html->scripts() !!}
<script>
_token = "{{csrf_token()}}";
_method = "PUT";
$(function () {
    
    $(".btnModalCreate").click(function(){
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('students.create')}}",
            type:'GET',
            cache:false,
            data:{'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                if(data.status !== 1){
                    ctlErrorRequestAjax(data);
                }//if
                else{
                    $("#content-create-student").html(data.html);
                    $('.select2').select2();
                    $('#modal-create-student').modal('show');
                }//else
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });
    
    $('#frm-crate-student').submit(function(e){
        
        e.preventDefault();
        
        var formData = new FormData(this);
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('students.store')}}",
            type:'POST',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){                                
                if(data.status === 1){
                    $('#modal-create-student').modal('hide');
                    msgNotification(data.type, data.msg);
                    let table = $('.table-stundents').DataTable();
                    table.ajax.reload();
                }else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 

    $(document).on('click','.btnModalEdit',function(){

        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        var id = $(this).data('id');

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('students.ajax.edit')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_token':_token,'_method':_method},
            datatype:'html',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                $("#content-edit-student").html(data);
                $('.select2').select2();
                $('#modal-edit-student').modal('show');
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });

    //$(document).on('click','#btn-save-edit-student',function (){
    $('#frm-edit-student').submit(function(e){
        
        e.preventDefault();
        
        var formData = new FormData(this);
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('students.ajax.update')}}",
            type:'POST',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            datatype:'JSON',
            beforeSend:function(){
            $('.contaninerLoading').show();
            },
            complete: function(){
            $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#modal-edit-student').modal('hide');
                    msgNotification(data.type,data.msg);
                    let table = $('.table-stundents').DataTable();
                    table.ajax.reload();
                }
                else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 
    
    // Confirm delete
    $('.deleteForm').click(function(){

        let _token = "{{csrf_token()}}";
        let _method = "PUT";
        let id = $('#idDelete').val();

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content') },
            url: "{{route('students.ajax.delete')}}",
            type: 'POST',
            cache: false,
            data:{id:id,'_token':_token,'_method':_method},
            datatype: 'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#confirmDelete').modal('hide');
                    msgNotification(data.type, data.msg);
                    let table = $('.table-stundents').DataTable();
                    table.ajax.reload();
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    });

});

</script>
@stop  