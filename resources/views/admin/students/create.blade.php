{{csrf_field()}}
<div class="row">        
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <ul class="nav nav-tabs" id="create-myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="student-tab" data-toggle="tab" href="#tbStudent" role="tab" aria-controls="tbStudent" aria-selected="false">{{trans('adminlte::students.infStudent')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tutor-tab" data-toggle="tab" href="#tabTutor" role="tab" aria-controls="tabTutor" aria-selected="false">{{trans('adminlte::students.infTutor')}}</a>
            </li>         
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tbStudent" role="tabpanel" aria-labelledby="student-tab">
                <div class="row">
                    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <label for="name" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
                        <input type="text" id="name" class="form-control" maxlength="190" name="name" placeholder="{{trans('adminlte::adminlte.name')}}">
                    </div>
                    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <label for="lastName" class="control-label">{{trans('adminlte::students.lastName')}}</label>
                        <input type="text" id="lastName" class="form-control" maxlength="190" name="lastName" placeholder="{{trans('adminlte::students.lastName')}}">
                    </div>
                    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <label for="email" class="control-label">{{trans('adminlte::adminlte.email')}}<span class="kv-reqd">*</span></label>
                        <input type="email" id="email" class="form-control" maxlength="190" name="email" placeholder="{{trans('adminlte::adminlte.email')}}">
                    </div>
                    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <label for="phone" class="control-label">{{trans('adminlte::adminlte.phone')}}</label>
                        <input type="text" id="phone" class="form-control" maxlength="190" name="phone" placeholder="{{trans('adminlte::adminlte.phone')}}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <label for="address" class="control-label">{{trans('adminlte::adminlte.direction')}}</label>
                        <input type="text" id="address" class="form-control" minlength="8" maxlength="190" name="address" placeholder="{{trans('adminlte::adminlte.direction')}}">
                    </div>
                    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <label for="status" class="control-label">{{trans('adminlte::adminlte.status')}}<span class="kv-reqd">*</span></label>
                        <select id="status" name="status" class="form-control select2 select2-hidden-accessible" data-placeholder="{{trans('adminlte::adminlte.status')}}" style="width: 100%;">
                            <option value=""></option>
                            <option value="1">{{trans('adminlte::adminlte.active')}}</option>
                            <option value="0">{{trans('adminlte::adminlte.inactive')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <label for="password" class="control-label">{{trans('adminlte::adminlte.password')}}<span class="kv-reqd">*</span></label>
                        <input type="password" id="password" class="form-control" minlength="8" maxlength="255" name="password" placeholder="{{trans('adminlte::adminlte.password')}}">
                    </div>
                    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <label for="password_confirmation" class="control-label">{{trans('adminlte::adminlte.confirm_password')}}<span class="kv-reqd">*</span></label>
                        <input type="password" id="password_confirmation" class="form-control" minlength="8" maxlength="255" name="password_confirmation" placeholder="{{trans('adminlte::adminlte.confirm_password')}}">
                    </div>                    
                </div>                 
            </div>
            <div class="tab-pane" id="tabTutor" role="tabpanel" aria-labelledby="tutor-tab">
                <div class="row">
                    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label for="tutorName" class="control-label">{{trans('adminlte::students.tutorName')}}</label>
                        <input type="text" id="tutorName" class="form-control" maxlength="190" name="tutorName" placeholder="{{trans('adminlte::students.tutorName')}}">
                    </div>
                    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label for="tutorLastName" class="control-label">{{trans('adminlte::students.tutorLastName')}}</label>
                        <input type="text" id="tutorLastName" class="form-control" maxlength="190" name="tutorLastName" placeholder="{{trans('adminlte::students.tutorLastName')}}">
                    </div>
                    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label for="tutorEmail" class="control-label">{{trans('adminlte::students.tutorEmail')}}</label>
                        <input type="email" id="tutorEmail" class="form-control" maxlength="190" name="tutorEmail" placeholder="{{trans('adminlte::students.tutorEmail')}}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label for="tutorPhone" class="control-label">{{trans('adminlte::students.tutorPhone')}}</label>
                        <input type="text" id="tutorPhone" class="form-control" maxlength="190" name="tutorPhone" placeholder="{{trans('adminlte::students.tutorPhone')}}">
                    </div>
                    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label for="tutorAddress" class="control-label">{{trans('adminlte::students.tutorAddress')}}</label>
                        <input type="text" id="tutorAddress" class="form-control" minlength="8" maxlength="190" name="tutorAddress" placeholder="{{trans('adminlte::students.tutorAddress')}}">
                    </div>
                    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label for="relationship" class="control-label">{{trans('adminlte::students.relationship')}}</label>
                        <select id="relationship" name="relationship" class="form-control select2 select2-hidden-accessible" data-placeholder="{{trans('adminlte::students.relationship')}}" style="width: 100%;">
                            <option value=""></option>
                            @foreach(\App\Models\Kinship::where('status',1)->orderBy('name','ASC')->get() as $kinship)
                                <option value="{{$kinship->id}}">{{ $kinship->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>     
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="observations" class="control-label">{{trans('adminlte::adminlte.observations')}}</label>
        <textarea id="observations" class="form-control" rows="3" maxlength="5000" name="observations"></textarea>
    </div>
</div>
@if(\App\Models\File_type::find(1)->status == 1 || \App\Models\File_type::find(2)->status == 1)
    <div class="row">    
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table class="table table-sm table-striped table-bordered dt-responsive">
                <thead>                  
                    <tr>
                        <th>{{trans('adminlte::adminlte.file')}}</th>
                        <th><i class="fas fa-paperclip"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @if(\App\Models\File_type::find(1)->status == 1)
                        <tr>
                            <td>{{trans('adminlte::students.curp')}}</td>
                            <td><input type="file" id="curp" class="form-control-file" name="curp"></td>
                        </tr>
                    @endif
                    @if(\App\Models\File_type::find(2)->status == 1)
                        <tr>
                            <td>{{trans('adminlte::students.birthCertificate')}}</td>
                            <td><input type="file" id="birthCertificate" class="form-control-file" name="birthCertificate"></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endif
