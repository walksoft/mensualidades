<input type="hidden" name="id" id="id" value="{{$course->course_id}}">
<div class="row">
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="code_course" class="control-label">{{trans('adminlte::courses.code')}}<span class="kv-reqd">*</span></label>
        <input type="text" value="{{$course->code}}" id="code_course" class="form-control" maxlength="20" name="code_course" placeholder="{{trans('adminlte::courses.code')}}">
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="class_course" class="control-label">{{trans('adminlte::courses.class')}}<span class="kv-reqd">*</span></label>
        <select id="class_course" name="class_course" class="form-control select-class select2 select2-hidden-accessible" data-placeholder="{{trans('adminlte::courses.class')}}" style="width: 100%;">
            <option value=""></option>
            @foreach(\App\Models\Service::where('status',1)->get() as $service)
                <option @if($course->service_id == $service->service_id) selected="selected" @endif value="{{$service->service_id}}">{{$service->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="instructor_course" class="control-label">{{trans('adminlte::instructors.instructor')}}<span class="kv-reqd">*</span></label>
        <select id="instructor_course" name="instructor_course" class="form-control select-2 select2 select2-hidden-accessible" data-placeholder="{{trans("adminlte::instructors.instructor")}}" style="width: 100%;">
            <option value=""></option>
            @foreach(\App\User::where('status',1)->where('type_user',2)->get() as $user)
                <option @if($course->user_id == $user->id) selected="selected" @endif value="{{$user->id}}">{{$user->name}} {{$user->last_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="status_course" class="control-label">{{trans('adminlte::courses.status')}}<span class="kv-reqd">*</span></label>
        <select id="status_course" name="status_course" class="form-control select-2 select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            <option @if($course->status == 1) selected="selected" @endif value="1">{{trans('adminlte::adminlte.active')}}</option>
            <option @if($course->status == 0) selected="selected" @endif value="0">{{trans('adminlte::adminlte.inactive')}}</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="form-group">
            <label for="startTime_course" class="control-label">{{trans('adminlte::courses.startTime')}}<span class="kv-reqd">*</span></label>
            <div class="input-group date" id="datetimepicker-startTime_course" data-target-input="nearest">
                <input type="text" value="{{\Carbon\Carbon::parse($course->start_time)->format('g:iA')}}" id="startTime_course" name="startTime_course" class="form-control datetimepicker-input" data-target="#datetimepicker-startTime_course"/>
                <div class="input-group-append" data-target="#datetimepicker-startTime_course" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="form-group">
            <label for="endTime_course" class="control-label">{{trans('adminlte::courses.endTime')}}<span class="kv-reqd">*</span></label>
            <div class="input-group date" id="datetimepicker-endTime_course" data-target-input="nearest">
                <input type="text" value="{{\Carbon\Carbon::parse($course->end_time)->format('g:iA')}}" disabled="disabled" id="endTime_course" name="endTime_course" class="form-control datetimepicker-input" data-target="#datetimepicker-endTime_course"/>
                <div class="input-group-append" data-target="#datetimepicker-endTime_course" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="payment_instructor_course" class="control-label">{{trans('adminlte::courses.payment_instructor')}}</label>
        <input type="number" @if(!auth()->user()->hasPermissionTo('payment.course')) disabled="disabled" @endif value="{{$course->payment_instructor}}" id="payment_instructor_course" class="form-control" name="payment_instructor_course" placeholder="{{trans('adminlte::courses.payment_instructor')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="frequency_course" class="control-label">{{trans('adminlte::courses.frequency')}}<span class="kv-reqd">*</span></label>
        <select id="frequency_course" name="frequency_course[]" multiple="multiple" class="form-control select-2 select2" data-placeholder="{{trans("adminlte::courses.frequency")}}" style="width: 100%;">
            <option @if($course->array_frequency_original() && in_array('monday',$course->array_frequency_original())) selected="selected" @endif value="monday">{{trans('adminlte::adminlte.monday')}}</option>
            <option @if($course->array_frequency_original() && in_array('tuesday',$course->array_frequency_original())) selected="selected" @endif value="tuesday">{{trans('adminlte::adminlte.tuesday')}}</option>
            <option @if($course->array_frequency_original() && in_array('wednesday',$course->array_frequency_original())) selected="selected" @endif value="wednesday">{{trans('adminlte::adminlte.wednesday')}}</option>
            <option @if($course->array_frequency_original() && in_array('thursday',$course->array_frequency_original())) selected="selected" @endif value="thursday">{{trans('adminlte::adminlte.thursday')}}</option>
            <option @if($course->array_frequency_original() && in_array('friday',$course->array_frequency_original())) selected="selected" @endif value="friday">{{trans('adminlte::adminlte.friday')}}</option>
            <option @if($course->array_frequency_original() && in_array('saturday',$course->array_frequency_original())) selected="selected" @endif value="saturday">{{trans('adminlte::adminlte.saturday')}}</option>
            <option @if($course->array_frequency_original() && in_array('sunday',$course->array_frequency_original())) selected="selected" @endif value="sunday">{{trans('adminlte::adminlte.sunday')}}</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="observations_course" class="control-label">{{trans('adminlte::courses.observations')}}</label>
        <textarea id="observations_course" class="form-control" rows="3" maxlength="1000" name="observations_course">{{$course->observations}}</textarea>
    </div>
</div>