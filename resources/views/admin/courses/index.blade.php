@extends('adminlte::page')
@section('title',trans('adminlte::courses.courses'))
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fab fa-buffer" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::courses.courses')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::courses.courses')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            @if($permits['create'])
                <a href="#" class="btn btn-block btn-success btnModalCreate" data-toggle="modal">Crear</a>
            @endif
        </div>
    </div>
    <div class="card-body">
        {!! $html->table(['class' => 'table table-striped table-bordered dt-responsive table-courses', 'style' => 'width:100%']) !!}
    </div>
</div>
<div class="modal fade" id="modal-create-course" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fab fa-buffer"></i> {{trans('adminlte::courses.createCourse')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="content-create-course"></div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-course" name="btn-save-course" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-edit-course" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fab fa-buffer"></i> {{trans('adminlte::courses.editCourse')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="content-edit-course"></div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-edit-course" name="btn-save-edit-course" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-inscription" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fas fa-paperclip"></i> {{trans('adminlte::inscriptions.inscriptions')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm-inscription" name="frm-inscription" enctype="multipart/form-data">
                <div class="modal-body" id="content-inscription"></div>            
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div id="confirmDelete-inscription" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header" style="display:flow-root">
        <div class="icon-box">
          <i class="fa fa-times"></i>
        </div>
        <h4 class="modal-title">{{trans('adminlte::adminlte.confirm_msg_delete_1')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <p>{{trans('adminlte::adminlte.confirm_msg_delete_2')}}</p>
        <input type="hidden" name="idDelete-inscription" id="idDelete-inscription"/>
      </div>
      <div class="modal-footer" style="display:flow-root">
        <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('adminlte::adminlte.cancel')}}</button>
        <button type="button" class="btn btn-danger deleteForm-inscription">{{trans('adminlte::adminlte.accept')}}</button>
      </div>
    </div>
  </div>
</div>
@stop
@section('js')
{!! $html->scripts() !!}
<script>
_token = "{{csrf_token()}}";
_method = "PUT";
$(document).on('click',".confirmDelete-inscription",function(){
    $('#idDelete-inscription').val($(this).data('id'));
});
$(function () {
    
    $(".btnModalCreate").click(function(){
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('courses.create')}}",
            type:'GET',
            cache:false,
            data:{'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                if(data.status !== 1){
                    ctlErrorRequestAjax(data);
                }//if
                else{
                    $("#content-create-course").html(data.html);
                    $('.select-class').select2().on('select2:select',function(e){
                        update_end_time($("#startTime").val(),'endTime','class');
                    });
                    $('.select-2').select2();
                    $('#datetimepicker-startTime').datetimepicker({format:'LT'});                    
                    $("#datetimepicker-startTime").on("datetimepicker.hide",function(date){                        
                        update_end_time($("#startTime").val(),'endTime','class');
                    }); 
                    $('#modal-create-course').modal('show');
                }//else
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });
    
    $("#btn-save-course").click(function(){
                
        var frequency = $("#frequency").val().length ? JSON.stringify($("#frequency").val()) : null;
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('courses.store')}}",
            type:'POST',
            cache:false,
            data:{'code':$("#code").val(),'frequency':frequency,'class':$("#class").val(),'instructor':$("#instructor").val(),'startTime':$("#startTime").val(),'status':$("#status").val(),'payment_instructor':$("#payment_instructor").val(),'observations':$("#observations").val(),'_token':_token},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){                                
                if(data.status === 1){
                    $('#modal-create-course').modal('hide');
                    msgNotification(data.type,data.msg);
                    let table = $('.table-courses').DataTable();
                    table.ajax.reload();
                }else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 

    $(document).on('click','.btnModalEdit',function(){

        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        var id = $(this).data('id');

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('courses.ajax.edit')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_token':_token,'_method':_method},
            datatype:'html',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                $("#content-edit-course").html(data);
                $('.select-class').select2().on('select2:select',function(e){
                    update_end_time($("#startTime_course").val(),'endTime_course','class_course');
                });
                $('.select-2').select2();
                $('#datetimepicker-startTime_course').datetimepicker({format:'LT'});                    
                $("#datetimepicker-startTime_course").on("datetimepicker.hide",function(date){                        
                    update_end_time($("#startTime_course").val(),'endTime_course','class_course');
                });  
                $('#modal-edit-course').modal('show');
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });

    $(document).on('click','#btn-save-edit-course',function (){

        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        
        var frequency = $("#frequency_course").val().length ? JSON.stringify($("#frequency_course").val()) : null;
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('courses.ajax.update')}}",
            type:'POST',
            cache:false,
            data:{'_id':$("#id").val(),'code':$("#code_course").val(),'frequency':frequency,'class':$("#class_course").val(),'instructor':$("#instructor_course").val(),'startTime':$("#startTime_course").val(),'status':$("#status_course").val(),'payment_instructor':$("#payment_instructor_course").val(),'observations':$("#observations_course").val(),'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
            $('.contaninerLoading').show();
            },
            complete: function(){
            $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#modal-edit-course').modal('hide');
                    msgNotification(data.type,data.msg);
                    let table = $('.table-courses').DataTable();
                    table.ajax.reload();
                }
                else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 
    
    // Confirm delete
    $('.deleteForm').click(function(){

        let _token = "{{csrf_token()}}";
        let _method = "PUT";
        let id = $('#idDelete').val();

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content') },
            url: "{{route('courses.ajax.delete')}}",
            type: 'POST',
            cache: false,
            data:{id:id,'_token':_token,'_method':_method},
            datatype: 'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#confirmDelete').modal('hide');
                    msgNotification(data.type, data.msg);
                    let table = $('.table-courses').DataTable();
                    table.ajax.reload();
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    });
            
    $(document).on('click','.btnModalInscription',function(){
        var id = $(this).data('id');
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('inscriptions.ajax.index')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                if(data.status !== 1){
                    ctlErrorRequestAjax(data);
                }//if
                else{
                    $("#content-inscription").html(data.html);
                    $('.select-student-inscription').select2();
                    $('.select-student-inscription').select2().on('select2:select',function(e){
                        update_info_student(e.params.data.id);
                    });
                    $('#modal-inscription').modal('show');
                }//else
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });
    
    $(document).on('click','#btn-save-inscription',function(){
        
        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        var value_monthlyCharge = @if(!auth()->user()->hasPermissionTo('update_values.inscription')) null; @else $("#value_monthlyCharge").val(); @endif
        var value_promptPayment = @if(!auth()->user()->hasPermissionTo('update_values.inscription')) null; @else $("#value_promptPaymentCollection").val(); @endif
        var value_inscription = @if(!auth()->user()->hasPermissionTo('update_values.inscription')) null; @else $("#value_inscription").val(); @endif
        
        if($("#student").val() === '' || $("#course_id").val() === ''){
            return false;
        }//if
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('inscriptions.ajax.inscribe')}}",
            type:'POST',
            cache:false,
            data:{'user_id':$("#student").val(),'course_id':$("#course_id").val(),'value_monthlyCharge':value_monthlyCharge,'value_promptPaymentCollection':value_promptPayment,'value_inscription':value_inscription,'observations':$("#observations-inscription").val(),'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide();
            },
            success:function(data){
                if(data.status === 1){
                    $("#btn-save-inscription").attr('disabled','disabled');
                    $("#observations-inscription").val('');
                    $("#table_registration").html(data.html);
                    $("#total-inscriptions").html(data.total);      
                    $('.select-student-inscription').empty();                                        
                    $('.select-student-inscription').select2({data:data.students});
                    $('.select-student-inscription').select2('destroy').val("").select2();
                    msgNotification(data.type,data.msg);                    
                }
                else{
                    ctlErrorRequestAjax(data);
                }
            },
            error:function(xhr,textStatus,thrownError){
                $('.contaninerLoading').hide();
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });    
    });
    
    //Confirm delete
    $('.deleteForm-inscription').click(function(){

        let _token = "{{csrf_token()}}";
        let _method = "PUT";
        let id = $('#idDelete-inscription').val();

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content') },
            url: "{{route('inscriptions.ajax.delete')}}",
            type: 'POST',
            cache: false,
            data:{id:id,'_token':_token,'_method':_method},
            datatype: 'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                if(data.status === 1){
                    $("#table_registration").html(data.html);
                    $("#total-inscriptions").html(data.total);      
                    $('.select-student-inscription').empty();                                        
                    $('.select-student-inscription').select2({data:data.students});
                    $('.select-student-inscription').select2('destroy').val("").select2();
                    $('#confirmDelete-inscription').modal('hide');
                    msgNotification(data.type,data.msg);                                                            
                }//if
                else{
                    ctlErrorRequestAjax(data);
                }//else
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    });
        
});

function update_end_time(date,element,element_class){
        
        if($("#"+element_class).val() === '' || date === ''){
            return false;
        }//if
        
        var _token = "{{csrf_token()}}";
        var _method = "PUT";

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('courses.ajax.update_end_time')}}",
            type:'POST',
            cache:false,
            data:{'_date':date,_service:$("#"+element_class).val(),'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide();
            },
            success:function(data){
                $("#"+element).val(data.end_time);
            },
            error:function(xhr,textStatus,thrownError){
                $('.contaninerLoading').hide();
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        }); 
        
}//update_end_time

function update_info_student(id){
    
        $("#card-student").css('display','none');
        $("#name-student").html('');
        $("#id-student").html('');
        $("#email-student").html('');
        $("#phone-student").html('');
                
        if(id === ''){
            return false;
        }//if
               
        var _token = "{{csrf_token()}}";
        var _method = "PUT";

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('inscriptions.ajax.info_student')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide();
            },
            success:function(data){
                var name = data.student.name+(data.student.last_name ? ' '+data.student.last_name : '');
                $("#btn-save-inscription").removeAttr('disabled','disabled');
                $("#card-student").css('display','block');
                $("#name-student").html(name);
                $("#id-student").html(data.student.id);
                $("#email-student").html(data.student.email);
                $("#phone-student").html(data.student.phone_number);
            },
            error:function(xhr,textStatus,thrownError){
                $('.contaninerLoading').hide();
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });     
}//update_info_student

function update_table_registration(course_id){
    
        var _token = "{{csrf_token()}}";
        var _method = "PUT";

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('inscriptions.ajax.table_registration')}}",
            type:'POST',
            sync:false,
            cache:false,
            data:{'course_id':course_id,'_token':_token,'_method':_method},
            datatype:'html',
            beforeSend:function(){},
            complete:function(){},
            success:function(data){
                $("#table_registration").html(data.html);
                $("#total-inscriptions").html(data.total);                
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
}//update_table_registration

</script>
@stop  