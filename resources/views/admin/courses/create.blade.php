<div class="row">
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="code" class="control-label">{{trans('adminlte::courses.code')}}<span class="kv-reqd">*</span></label>
        <input type="text" id="code" class="form-control" maxlength="20" name="code" placeholder="{{trans('adminlte::courses.code')}}">
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="class" class="control-label">{{trans('adminlte::courses.class')}}<span class="kv-reqd">*</span></label>
        <select id="class" name="class" class="form-control select-class select2 select2-hidden-accessible" data-placeholder="{{trans('adminlte::courses.class')}}" style="width: 100%;">
            <option value=""></option>
            @foreach(\App\Models\Service::where('status',1)->get() as $service)
                <option value="{{$service->service_id}}">{{$service->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="instructor" class="control-label">{{trans('adminlte::instructors.instructor')}}<span class="kv-reqd">*</span></label>
        <select id="instructor" name="instructor" class="form-control select-2 select2 select2-hidden-accessible" data-placeholder="{{trans("adminlte::instructors.instructor")}}" style="width: 100%;">
            <option value=""></option>
            @foreach(\App\User::where('status',1)->where('type_user',2)->get() as $user)
                <option value="{{$user->id}}">{{$user->name}} {{$user->last_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="status" class="control-label">{{trans('adminlte::courses.status')}}<span class="kv-reqd">*</span></label>
        <select id="status" name="status" class="form-control select-2 select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            <option value="1">{{trans('adminlte::adminlte.active')}}</option>
            <option value="0">{{trans('adminlte::adminlte.inactive')}}</option>
        </select>
    </div>    
</div>
<div class="row">
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="form-group">
            <label for="startTime" class="control-label">{{trans('adminlte::courses.startTime')}}<span class="kv-reqd">*</span></label>
            <div class="input-group date" id="datetimepicker-startTime" data-target-input="nearest">
                <input type="text" id="startTime" name="startTime" class="form-control datetimepicker-input" data-target="#datetimepicker-startTime"/>
                <div class="input-group-append" data-target="#datetimepicker-startTime" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="form-group">
            <label for="endTime" class="control-label">{{trans('adminlte::courses.endTime')}}<span class="kv-reqd">*</span></label>
            <div class="input-group date" id="datetimepicker-endTime" data-target-input="nearest">
                <input type="text" disabled="disabled" id="endTime" name="endTime" class="form-control datetimepicker-input" data-target="#datetimepicker-endTime"/>
                <div class="input-group-append" data-target="#datetimepicker-endTime" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="payment_instructor" class="control-label">{{trans('adminlte::courses.payment_instructor')}}</label>
        <input @if(!auth()->user()->hasPermissionTo('payment.course')) disabled="disabled" @endif type="number" id="payment_instructor" class="form-control" name="payment_instructor" placeholder="{{trans('adminlte::courses.payment_instructor')}}">
    </div>    
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="frequency" class="control-label">{{trans('adminlte::courses.frequency')}}<span class="kv-reqd">*</span></label>
        <select id="frequency" name="frequency[]" multiple="multiple" class="form-control select-2 select2" data-placeholder="{{trans("adminlte::courses.frequency")}}" style="width: 100%;">
            <option value="monday">{{trans('adminlte::adminlte.monday')}}</option>
            <option value="tuesday">{{trans('adminlte::adminlte.tuesday')}}</option>
            <option value="wednesday">{{trans('adminlte::adminlte.wednesday')}}</option>
            <option value="thursday">{{trans('adminlte::adminlte.thursday')}}</option>
            <option value="friday">{{trans('adminlte::adminlte.friday')}}</option>
            <option value="saturday">{{trans('adminlte::adminlte.saturday')}}</option>
            <option value="sunday">{{trans('adminlte::adminlte.sunday')}}</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="observations" class="control-label">{{trans('adminlte::courses.observations')}}</label>
        <textarea id="observations" class="form-control" rows="3" maxlength="1000" name="observations"></textarea>
    </div>
</div>