@if(auth()->user()->hasPermissionTo('create.typelevel'))
    <div class="row">
        <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <label for="name_level" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
            <input type="text" id="name_level" class="form-control" name="name_level" placeholder="{{trans('adminlte::adminlte.name')}}">
        </div>      
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <label for="description_level" class="control-label">{{trans('adminlte::adminlte.description')}}</label>
            <input type="text" id="description_level" class="form-control" name="description_level" placeholder="{{trans('adminlte::adminlte.description')}}">
        </div> 
        <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <label for="status_level" class="control-label">{{trans('adminlte::adminlte.status')}}<span class="kv-reqd">*</span></label>
            <select id="status_level" name="status_level" class="form-control select-2 select2 select2-hidden-accessible" data-placeholder="{{trans('adminlte::adminlte.status')}}" style="width:100%;">
                <option value=""></option>
                <option value="1">{{trans('adminlte::adminlte.active')}}</option>
                <option value="0">{{trans('adminlte::adminlte.inactive')}}</option>
            </select>
        </div> 
        <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
            <label>&nbsp;&nbsp;</label><br>
            <button type="button" id="btn-save-level" name="btn-save-level" class="btn btn-block btn-success"><i class="fas fa-save"></i>&nbsp;&nbsp;{{trans('adminlte::adminlte.save')}}</button>
        </div>
    </div>
@endif
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-bordered dt-responsive table-head-fixed">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>{{trans('adminlte::adminlte.name')}}</th>
                    <th>{{trans('adminlte::adminlte.description')}}</th>
                    <th>{{trans('adminlte::adminlte.status')}}</th>
                    @if(auth()->user()->hasPermissionTo('delete.typelevel'))
                        <th>{{trans('adminlte::adminlte.delete')}}</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @if($levels->count() > 0)
                    @foreach($levels as $level)
                        <tr>
                            <td>{{$level->id}}</td>
                            <td>
                                @if(auth()->user()->hasPermissionTo('edit.typelevel'))
                                    <input type="text" id="name_{{$level->id}}" class="form-control input-sm name_type" name="name_{{$level->id}}" value="{{$level->name}}" placeholder="{{trans('adminlte::adminlte.name')}}">
                                @else
                                    {{$level->name}}
                                @endif
                            </td>
                            <td>
                                @if(auth()->user()->hasPermissionTo('edit.typelevel'))
                                    <input type="text" id="description_{{$level->id}}" class="form-control input-sm description_type" name="description_{{$level->id}}" value="{{$level->description}}" placeholder="{{trans('adminlte::adminlte.description')}}">
                                @else
                                    {{$level->description}}
                                @endif
                            </td>                   
                            <td>
                                @if($level->status)  
                                    @if(auth()->user()->hasPermissionTo('edit.typelevel'))
                                        <a href="#" class="status_level" data-id="{{$level->id}}" data-val="0">
                                    @endif
                                        <span class="badge badge-success">{{trans('adminlte::adminlte.active')}}</span>
                                    @if(auth()->user()->hasPermissionTo('edit.typelevel'))
                                        </a>
                                    @endif
                                @else
                                    @if(auth()->user()->hasPermissionTo('edit.typelevel'))
                                        <a href="#" class="status_level" data-id="{{$level->id}}" data-val="1">
                                    @endif
                                        <span class="badge badge-danger">{{trans('adminlte::adminlte.inactive')}}</span>
                                    @if(auth()->user()->hasPermissionTo('edit.typelevel'))
                                        </a>
                                    @endif
                                @endif                    
                            </td>
                            @if(auth()->user()->hasPermissionTo('delete.typelevel'))
                                <td>
                                    <a href="#" class="confirmDelete-level" data-id="{{$level->id}}"><span class="badge badge-danger">{{trans('adminlte::adminlte.delete')}}</span></a>    
                                </td>
                            @endif
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="7">No se encontraron registros...</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>