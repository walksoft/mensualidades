<input type="hidden" name="id" id="id" value="{{$service->service_id}}">
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="name_service" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
        <input type="text" value="{{$service->name}}" id="name_service" class="form-control" maxlength="190" name="name_service" placeholder="{{trans('adminlte::adminlte.name')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="duration_service" class="control-label">{{trans('adminlte::services.duration')}}<span class="kv-reqd">*</span></label>
        <input type="number" value="{{$service->session_duration}}" id="duration_service" class="form-control" maxlength="190" name="duration_service" placeholder="{{trans('adminlte::services.duration')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="type_service" class="control-label">{{trans('adminlte::services.type')}}<span class="kv-reqd">*</span></label>
        <select id="type_service" name="type_service" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            @foreach(\App\Models\Type_service::where('status',1)->get() as $type)
                <option @if($type->id == $service->type_id) selected="selected" @endif value="{{$type->id}}">{{$type->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="level_service" class="control-label">{{trans('adminlte::services.level')}}<span class="kv-reqd">*</span></label>
        <select id="level_service" name="level_service" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            @foreach(\App\Models\Level_service::where('status',1)->get() as $level)
                <option @if($level->id == $service->level_id) selected="selected" @endif value="{{$level->id}}">{{$level->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="monthlyCharge_service" class="control-label">{{trans('adminlte::services.monthlyCharge')}}<span class="kv-reqd">*</span></label>
        <input type="number" value="{{$service->monthly_value}}" id="monthlyCharge_service" class="form-control" minlength="8" maxlength="255" name="monthlyCharge_service" placeholder="{{trans('adminlte::services.monthlyCharge')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="promptPaymentCollection_service" class="control-label">{{trans('adminlte::services.promptPaymentCollection')}}<span class="kv-reqd">*</span></label>
        <input type="number" value="{{$service->timely_payment_value}}" id="promptPaymentCollection_service" class="form-control" minlength="8" maxlength="255" name="promptPaymentCollection_service" placeholder="{{trans('adminlte::services.promptPaymentCollection')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="inscription_service" class="control-label">{{trans('adminlte::services.inscription')}}<span class="kv-reqd">*</span></label>
        <input type="number" value="{{$service->registration_value}}" id="inscription_service" class="form-control" minlength="8" maxlength="255" name="inscription_service" placeholder="{{trans('adminlte::services.inscription')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="status_service" class="control-label">{{trans('adminlte::services.status')}}<span class="kv-reqd">*</span></label>
        <select id="status_service" name="status_service" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            <option @if($service->status == 1) selected="selected" @endif value="1">{{trans('adminlte::adminlte.active')}}</option>
            <option @if($service->status == 0) selected="selected" @endif value="0">{{trans('adminlte::adminlte.inactive')}}</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="description_service" class="control-label">{{trans('adminlte::adminlte.description')}}</label>
        <textarea id="description_service" class="form-control" rows="3" maxlength="1000" name="description_service">{{$service->description}}</textarea>
    </div>
</div>