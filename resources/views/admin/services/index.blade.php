@extends('adminlte::page')
@section('title',trans('adminlte::services.services'))
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fas fa-briefcase" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::services.services')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::services.services')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            @if($permits['create'])
                <a href="#" class="btn btn-success btnModalCreate" data-toggle="modal">Crear</a>
            @endif
            @if($permitsTypes['list'] || $permitsTypes['create'] || $permitsTypes['edit'] || $permitsTypes['delete'])
                <a href="#"  class="btn btn-success types" id="btn-types" name="btn-types"><i class="fas fa-list-ul"></i>&nbsp;&nbsp;{{trans('adminlte::services.types')}}</a>
            @endif
            @if($permitsLevels['list'] || $permitsLevels['create'] || $permitsLevels['edit'] || $permitsLevels['delete'])
                <a href="#"  class="btn btn-success levels" id="btn-levels" name="btn-levels"><i class="fas fa-list-ul"></i>&nbsp;&nbsp;{{trans('adminlte::services.levels')}}</a>
            @endif
        </div>
    </div>
    <div class="card-body">
        {!! $html->table(['class' => 'table table-striped table-bordered dt-responsive', 'style' => 'width:100%']) !!}
    </div>
</div>
<div class="modal fade" id="modal-create-service" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fa fa-briefcase"></i> {{trans('adminlte::services.createService')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="content-create-service"></div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-service" name="btn-save-service" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-edit-service" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fa fa-briefcase"></i> {{trans('adminlte::services.editService')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="content-edit-service"></div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-edit-service" name="btn-save-edit-service" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@if($permitsTypes['list'] || $permitsTypes['create'] || $permitsTypes['edit'] || $permitsTypes['delete'])
<div class="modal fade" id="modal-types" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fas fa-list-ul"></i> {{trans('adminlte::services.types')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="content-types"></div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endif

@if($permitsLevels['list'] || $permitsLevels['create'] || $permitsLevels['edit'] || $permitsLevels['delete'])
<div class="modal fade" id="modal-levels" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fas fa-list-ul"></i> {{trans('adminlte::services.levels')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="content-levels"></div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endif

@if($permitsTypes['list'] || $permitsTypes['create'] || $permitsTypes['edit'] || $permitsTypes['delete'])
<div id="confirmDelete-type" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header" style="display:flow-root">
                <div class="icon-box">
                  <i class="fa fa-times"></i>
                </div>
                <h4 class="modal-title">{{trans('adminlte::adminlte.confirm_msg_delete_1')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>{{trans('adminlte::adminlte.confirm_msg_delete_2')}}</p>
                <input type="hidden" name="idDelete-type" id="idDelete-type">
            </div>
            <div class="modal-footer" style="display:flow-root">
                <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('adminlte::adminlte.cancel')}}</button>
                <button type="button" class="btn btn-danger deleteForm-type">{{trans('adminlte::adminlte.accept')}}</button>
            </div>
        </div>
    </div>
</div>
@endif

@if($permitsLevels['list'] || $permitsLevels['create'] || $permitsLevels['edit'] || $permitsLevels['delete'])
<div id="confirmDelete-level" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header" style="display:flow-root">
                <div class="icon-box">
                  <i class="fa fa-times"></i>
                </div>
                <h4 class="modal-title">{{trans('adminlte::adminlte.confirm_msg_delete_1')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>{{trans('adminlte::adminlte.confirm_msg_delete_2')}}</p>
                <input type="hidden" name="idDelete-level" id="idDelete-level">
            </div>
            <div class="modal-footer" style="display:flow-root">
                <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('adminlte::adminlte.cancel')}}</button>
                <button type="button" class="btn btn-danger deleteForm-level">{{trans('adminlte::adminlte.accept')}}</button>
            </div>
        </div>
    </div>
</div>
@endif

@stop
@section('js')
{!! $html->scripts() !!}
types<script>
_token = "{{csrf_token()}}";
_method = "PUT";
$(function () {
    
    $("#btn-types").click(function(){
        get_tbl_types();
        $('#modal-types').modal('show');
    });    
    
    $(document).on('click','#btn-save-type',function(){
        if($("#name_type").val() === '' || $("#status_type").val() === ''){
            return false;
        }//if
        create_type();        
    });
    
    $("#btn-levels").click(function(){
        get_tbl_levels();    
        $('#modal-levels').modal('show');
    });
    
    $(document).on('click','#btn-save-level',function(){
        if($("#name_level").val() === '' || $("#status_level").val() === ''){
            return false;
        }//if
        create_level();        
    });
    
    $(document).on('click','.status_type',function(){
        update_status('type',$(this).data('id'),$(this).data('val'));
    });

    $(document).on('click','.status_level',function(){
        update_status('level',$(this).data('id'),$(this).data('val'));
    });    
    
    $(".btnModalCreate").click(function(){
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('services.create')}}",
            type:'GET',
            cache:false,
            data:{'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                if(data.status !== 1){
                    ctlErrorRequestAjax(data);
                }//if
                else{
                    $("#content-create-service").html(data.html);
                    $('.select2').select2();
                    $('#modal-create-service').modal('show');
                }//else
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });
    
    $("#btn-save-service").click(function(){
                
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('services.store')}}",
            type:'POST',
            cache:false,
            data:{'name':$("#name").val(),'duration':$("#duration").val(),'type':$("#type").val(),'level':$("#level").val(),'monthlyCharge':$("#monthlyCharge").val(),'promptPaymentCollection':$("#promptPaymentCollection").val(),'inscription':$("#inscription").val(),'status':$("#status").val(),'description':$("#description").val(),'_token':_token},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){                                
                if(data.status === 1){
                    $('#modal-create-service').modal('hide');
                    msgNotification(data.type,data.msg);
                    let table = $('.table').DataTable();
                    table.ajax.reload();
                }else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 

    $(document).on('click','.btnModalEdit',function(){

        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        var id = $(this).data('id');

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('services.ajax.edit')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_token':_token,'_method':_method},
            datatype:'html',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                $("#content-edit-service").html(data);
                $('.select2').select2();
                $('#modal-edit-service').modal('show');
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });

    $(document).on('click','#btn-save-edit-service',function (){

        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('services.ajax.update')}}",
            type:'POST',
            cache:false,
            data:{'_id':$("#id").val(),'name':$("#name_service").val(),'duration':$("#duration_service").val(),'type':$("#type_service").val(),'level':$("#level_service").val(),'monthlyCharge':$("#monthlyCharge_service").val(),'promptPaymentCollection':$("#promptPaymentCollection_service").val(),'inscription':$("#inscription_service").val(),'status':$("#status_service").val(),'description':$("#description").val(),'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
            $('.contaninerLoading').show();
            },
            complete: function(){
            $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#modal-edit-service').modal('hide');
                    msgNotification(data.type,data.msg);
                    let table = $('.table').DataTable();
                    table.ajax.reload();
                }
                else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 
    
    // Confirm delete
    $('.deleteForm').click(function(){

        let _token = "{{csrf_token()}}";
        let _method = "PUT";
        let id = $('#idDelete').val();

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content') },
            url: "{{route('services.ajax.delete')}}",
            type: 'POST',
            cache: false,
            data:{id:id,'_token':_token,'_method':_method},
            datatype: 'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#confirmDelete').modal('hide');
                    msgNotification(data.type, data.msg);
                    // Se refresca tabla por ajax
                    let table = $('.table').DataTable();
                    table.ajax.reload();
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    });
    
    $(document).on('click','.confirmDelete-type',function (){
        $("#idDelete-type").val($(this).data('id'));
        $('#confirmDelete-type').modal('show');
    });
    
    $('.deleteForm-type').click(function(){
        delete_type_or_level('type');
    });    
    
    $(document).on('click','.confirmDelete-level',function (){
        $("#idDelete-level").val($(this).data('id'));
        $('#confirmDelete-level').modal('show');
    });
    
    $('.deleteForm-level').click(function(){
        delete_type_or_level('level');
    });
});

function delete_type_or_level(model){
    
    var id = null;
    
    if(model === 'type'){
        id = $("#idDelete-type").val();
    }//if
    if(model === 'level'){
        id = $("#idDelete-level").val();
    }//if
                         
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content') },
        url: "{{route('services.ajax.delete_type_or_level')}}",
        type: 'POST',
        cache: false,
        data:{'model':model,'id':id,'_token':_token,'_method':_method},
        datatype: 'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete: function(){
            $('.contaninerLoading').hide(); 
        },
        success: function(data){
            if(data.status === 1){
                if(model === 'type'){
                    get_tbl_types();
                    $('#confirmDelete-type').modal('hide');
                }//if
                if(model === 'level'){
                    get_tbl_levels();
                    $('#confirmDelete-level').modal('hide');   
                }//if
                msgNotification(data.type,data.msg);
            }//if
            else{
                ctlErrorRequestAjax(data);
                if(model === 'type'){
                    $('#confirmDelete-type').modal('hide');
                }//if
                if(model === 'level'){
                    $('#confirmDelete-level').modal('hide');   
                }//if
            }//else
        },
        error: function (xhr,textStatus,thrownError) {
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });    
}//delete_type_or_level

function update_status(model,id,value){
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url: "{{route('services.ajax.update_status')}}",
        type:'POST',
        cache:false,
        data:{'model':model,'id':id,'value':value,'_token':_token,'_method':_method},
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete:function(){
            $('.contaninerLoading').hide(); 
        },
        success:function(data){
            if(data.status !== 1){
                ctlErrorRequestAjax(data);
            }//if
            else{
                if(model === 'type'){
                    get_tbl_types();    
                }//if
                if(model === 'level'){
                    get_tbl_levels();    
                }//if                
            }//else
        },
        error:function(xhr,textStatus,thrownError){
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });    
}//update_status

function get_tbl_types(){
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url: "{{route('services.ajax.table_types')}}",
        type:'POST',
        cache:false,
        data:{'_token':_token,'_method':_method},
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete:function(){
            $('.contaninerLoading').hide(); 
        },
        success:function(data){
            if(data.status !== 1){
                ctlErrorRequestAjax(data);
            }//if
            else{
                $("#content-types").html(data.html);
                $('.select2').select2();                
            }//else
        },
        error:function(xhr,textStatus,thrownError){
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });    
}//get_tbl_types

function create_type(){
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url:"{{route('services.ajax.store_type')}}",
        type:'POST',
        cache:false,
        data:{'name':$("#name_type").val(),'description':$("#description_type").val(),'status':$("#status_type").val(),'_token':_token,'_method':_method},
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete: function(){
            $('.contaninerLoading').hide(); 
        },
        success: function(data){                                
            if(data.status === 1){
                msgNotification(data.type,data.msg);
                get_tbl_types();
            }else{
                ctlErrorRequestAjax(data);
            }
        },
        error: function (xhr,textStatus,thrownError) {
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });    
}//create_type

function get_tbl_levels(){
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url: "{{route('services.ajax.table_levels')}}",
        type:'POST',
        cache:false,
        data:{'_token':_token,'_method':_method},
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete:function(){
            $('.contaninerLoading').hide(); 
        },
        success:function(data){
            if(data.status !== 1){
                ctlErrorRequestAjax(data);
            }//if
            else{
                $("#content-levels").html(data.html);
                $('.select2').select2();                    
            }//else
        },
        error:function(xhr,textStatus,thrownError){
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });    
}//get_tbl_levels

function create_level(){
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url:"{{route('services.ajax.store_level')}}",
        type:'POST',
        cache:false,
        data:{'name':$("#name_level").val(),'description':$("#description_level").val(),'status':$("#status_level").val(),'_token':_token,'_method':_method},
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete: function(){
            $('.contaninerLoading').hide(); 
        },
        success: function(data){                                
            if(data.status === 1){
                msgNotification(data.type,data.msg);
                get_tbl_levels();
            }else{
                ctlErrorRequestAjax(data);
            }
        },
        error: function (xhr,textStatus,thrownError) {
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });    
}//create_type
</script>
@stop  