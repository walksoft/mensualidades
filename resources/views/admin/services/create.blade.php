<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="name" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
        <input type="text" id="name" class="form-control" maxlength="190" name="name" placeholder="{{trans('adminlte::adminlte.name')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="duration" class="control-label">{{trans('adminlte::services.duration')}}<span class="kv-reqd">*</span></label>
        <input type="number" id="duration" class="form-control" maxlength="190" name="duration" placeholder="{{trans('adminlte::services.duration')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="type" class="control-label">{{trans('adminlte::services.type')}}<span class="kv-reqd">*</span></label>
        <select id="type" name="type" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            @foreach(\App\Models\Type_service::where('status',1)->get() as $type)
                <option value="{{$type->id}}">{{$type->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="level" class="control-label">{{trans('adminlte::services.level')}}<span class="kv-reqd">*</span></label>
        <select id="level" name="level" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            @foreach(\App\Models\Level_service::where('status',1)->get() as $level)
                <option value="{{$level->id}}">{{$level->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="monthlyCharge" class="control-label">{{trans('adminlte::services.monthlyCharge')}}<span class="kv-reqd">*</span></label>
        <input type="number" id="monthlyCharge" class="form-control" minlength="1" maxlength="10" name="monthlyCharge" placeholder="{{trans('adminlte::services.monthlyCharge')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="promptPaymentCollection" class="control-label">{{trans('adminlte::services.promptPaymentCollection')}}<span class="kv-reqd">*</span></label>
        <input type="number" id="promptPaymentCollection" class="form-control" minlength="1" maxlength="10" name="promptPaymentCollection" placeholder="{{trans('adminlte::services.promptPaymentCollection')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="inscription" class="control-label">{{trans('adminlte::services.inscription')}}<span class="kv-reqd">*</span></label>
        <input type="number" id="inscription" class="form-control" minlength="1" maxlength="10" name="inscription" placeholder="{{trans('adminlte::services.inscription')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="status" class="control-label">{{trans('adminlte::services.status')}}<span class="kv-reqd">*</span></label>
        <select id="status" name="status" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            <option value="1">{{trans('adminlte::adminlte.active')}}</option>
            <option value="0">{{trans('adminlte::adminlte.inactive')}}</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="description" class="control-label">{{trans('adminlte::adminlte.description')}}</label>
        <textarea id="description" class="form-control" rows="3" maxlength="1000" name="description"></textarea>
    </div>
</div>