@if(auth()->user()->hasPermissionTo('create.typeservice'))
    <div class="row">
        <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <label for="name_type" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
            <input type="text" id="name_type" class="form-control" name="name_type" placeholder="{{trans('adminlte::adminlte.name')}}">
        </div>      
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <label for="description_type" class="control-label">{{trans('adminlte::adminlte.description')}}</label>
            <input type="text" id="description_type" class="form-control" name="description_type" placeholder="{{trans('adminlte::adminlte.description')}}">
        </div> 
        <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <label for="status_type" class="control-label">{{trans('adminlte::adminlte.status')}}<span class="kv-reqd">*</span></label>
            <select id="status_type" name="status_type" class="form-control select-2 select2 select2-hidden-accessible" data-placeholder="{{trans('adminlte::adminlte.status')}}" style="width:100%;">
                <option value=""></option>
                <option value="1">{{trans('adminlte::adminlte.active')}}</option>
                <option value="0">{{trans('adminlte::adminlte.inactive')}}</option>
            </select>
        </div> 
        <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
            <label>&nbsp;&nbsp;</label><br>
            <button type="button" id="btn-save-type" name="btn-save-type" class="btn btn-block btn-success"><i class="fas fa-save"></i>&nbsp;&nbsp;{{trans('adminlte::adminlte.save')}}</button>
        </div>
    </div>
@endif
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-bordered dt-responsive table-head-fixed">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>{{trans('adminlte::adminlte.name')}}</th>
                    <th>{{trans('adminlte::adminlte.description')}}</th>
                    <th>{{trans('adminlte::adminlte.status')}}</th>
                    @if(auth()->user()->hasPermissionTo('delete.typeservice'))
                        <th>{{trans('adminlte::adminlte.delete')}}</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @if($types->count() > 0)
                    @foreach($types as $type)
                        <tr>
                            <td>{{$type->id}}</td>
                            <td>
                                @if(auth()->user()->hasPermissionTo('edit.typeservice'))
                                    <input type="text" id="name_{{$type->id}}" class="form-control input-sm name_type" name="name_{{$type->id}}" value="{{$type->name}}" placeholder="{{trans('adminlte::adminlte.name')}}">
                                @else
                                    {{$type->name}}
                                @endif
                            </td>
                            <td>
                                @if(auth()->user()->hasPermissionTo('edit.typeservice'))
                                    <input type="text" id="description_{{$type->id}}" class="form-control input-sm description_type" name="description_{{$type->id}}" value="{{$type->description}}" placeholder="{{trans('adminlte::adminlte.description')}}">
                                @else
                                    {{$type->description}}
                                @endif
                            </td>                   
                            <td>
                                @if($type->status)                        
                                    @if(auth()->user()->hasPermissionTo('edit.typeservice'))
                                        <a href="#" class="status_type" data-id="{{$type->id}}" data-val="0">
                                    @endif
                                        <span class="badge badge-success">{{trans('adminlte::adminlte.active')}}</span>
                                    @if(auth()->user()->hasPermissionTo('edit.typeservice'))
                                        </a>
                                    @endif
                                @else
                                    @if(auth()->user()->hasPermissionTo('edit.typeservice'))
                                        <a href="#" class="status_type" data-id="{{$type->id}}" data-val="1">
                                    @endif
                                        <span class="badge badge-danger">{{trans('adminlte::adminlte.inactive')}}</span>
                                    @if(auth()->user()->hasPermissionTo('edit.typeservice'))
                                        </a>
                                    @endif
                                @endif                    
                            </td>
                            @if(auth()->user()->hasPermissionTo('delete.typeservice'))
                                <td>
                                    <a href="#" class="confirmDelete-type" data-id="{{$type->id}}" ><span class="badge badge-danger">{{trans('adminlte::adminlte.delete')}}</span></a>    
                                </td>
                            @endif
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="7">No se encontraron registros...</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>