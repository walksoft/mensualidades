{{csrf_field()}}
{{ method_field('PUT') }}
<input type="hidden" name="id" id="id" value="{{$user->id}}">
<div class="row">
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="name" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
        <input type="text" value="{{$user->name}}" id="name" class="form-control" maxlength="190" name="name" placeholder="{{trans('adminlte::adminlte.name')}}">
    </div>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="lastName" class="control-label">{{trans('adminlte::instructors.lastName')}}</label>
        <input type="text" value="{{$user->last_name}}" id="lastName" class="form-control" maxlength="190" name="lastName" placeholder="{{trans('adminlte::instructors.lastName')}}">
    </div>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="email" class="control-label">{{trans('adminlte::adminlte.email')}}<span class="kv-reqd">*</span></label>
        <input type="email" value="{{$user->email}}" id="email" class="form-control" maxlength="190" name="email" placeholder="{{trans('adminlte::adminlte.email')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="phone" class="control-label">{{trans('adminlte::adminlte.phone')}}</label>
        <input type="text" value="{{$user->phone_number}}" id="phone" class="form-control" maxlength="190" name="phone" placeholder="{{trans('adminlte::adminlte.phone')}}">
    </div>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="address" class="control-label">{{trans('adminlte::adminlte.direction')}}</label>
        <input type="text" value="{{$user->address}}" id="address" class="form-control" minlength="8" maxlength="190" name="address" placeholder="{{trans('adminlte::adminlte.direction')}}">
    </div>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="status" class="control-label">{{trans('adminlte::adminlte.status')}}<span class="kv-reqd">*</span></label>
        <select id="status" name="status" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            <option @if($user->status == 1) selected="selected" @endif value="1">{{trans('adminlte::adminlte.active')}}</option>
            <option @if($user->status == 0) selected="selected" @endif value="0">{{trans('adminlte::adminlte.inactive')}}</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="observations" class="control-label">{{trans('adminlte::adminlte.observations')}}</label>
        <textarea id="observations" class="form-control" rows="3" maxlength="5000" name="observations">{{$user->observations}}</textarea>
    </div>
</div>
@if(array_key_exists('3',$files) || \App\Models\File_type::find(3)->status == 1)
    <div class="row">    
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="cursos-tab" data-toggle="tab" href="#tabCursos" role="tab" aria-controls="tabCursos" aria-selected="false">{{trans('adminlte::courses.courses')}}</a>
                </li>   
                <li class="nav-item">
                    <a class="nav-link" id="files-tab" data-toggle="tab" href="#tabFiles" role="tab" aria-controls="tabFiles" aria-selected="false">{{trans('adminlte::adminlte.files')}}</a>
                </li>       
            </ul>            
            <div class="tab-content">
                <div class="tab-pane active" id="tabCursos" role="tabpanel" aria-labelledby="cursos-tab">
                    <div class="card-body table-responsive p-0" id="table_cash_register" style="height:200px;">
                        <table class="table table-bordered dt-responsive table-head-fixed">
                            <thead>
                                <tr>
                                    <th>{{trans('adminlte::courses.code')}}</th>
                                    <th>{{trans('adminlte::adminlte.name')}}</th>
                                    <th>{{trans('adminlte::courses.schedule')}}</th>
                                    <th>{{trans('adminlte::courses.frequency')}}</th>                                      
                                    <th>{{trans('adminlte::adminlte.status')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($user->courses->count() > 0)
                                    @foreach($user->courses as $course)
                                        <tr>
                                            <td>{{$course->code}}</td>
                                            <td>{{$course->service->name}}</td>
                                            <td>
                                                @php
                                                    $start_time = $course->start_time ? \Carbon\Carbon::parse($course->start_time)->format('g:iA') : NULL;
                                                    $end_time = $course->end_time ? \Carbon\Carbon::parse($course->end_time)->format('g:iA') : NULL;
                                                @endphp
                                                {{$start_time}} - {{$end_time}}
                                            </td>
                                            <td>
                                                @if($course->array_frequency())
                                                   @foreach($course->array_frequency() as $day)
                                                        <span class="badge badge-success">{{$day}}</span>
                                                    @endforeach
                                                @endif                                    
                                            </td>   
                                            <td>
                                                @if($course->status)                        
                                                    <span class="badge badge-success">{{trans('adminlte::adminlte.active')}}</span>
                                                @else
                                                    <span class="badge badge-danger">{{trans('adminlte::adminlte.inactive')}}</span>
                                                @endif                    
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td colspan="6">Aun no tiene cursos asignados...</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>                    
                    </div>
                </div>
                <div class="tab-pane" id="tabFiles" role="tabpanel" aria-labelledby="files-tab">
                    <table class="table table-bordered">
                        <thead>                  
                            <tr>
                                <th>{{trans('adminlte::adminlte.file')}}</th>
                                <th><i class="fas fa-paperclip"></i></th>
                                <th><i class="fas fa-file-upload"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{trans('adminlte::instructors.annexed')}}</td>
                                <td>
                                    <a target="_blank" href="@if(array_key_exists('3',$files) && $files[3]['id']) {{$files[3]['file_url']}} @endif" class="" name="curp_student" id="curp_student">@if(array_key_exists('3',$files) && $files[3]['file_name']) {{$files[3]['file_name']}} @endif</a>
                                </td>
                                <td>
                                    @if(\App\Models\File_type::find(3)->status == 1)
                                        <input type="file" id="anexo_instructor" class="form-control-file" name="anexo_instructor">
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endif