{{csrf_field()}}
<div class="row">
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="name" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
        <input type="text" id="name" class="form-control" maxlength="190" name="name" placeholder="{{trans('adminlte::adminlte.name')}}">
    </div>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="lastName" class="control-label">{{trans('adminlte::students.lastName')}}</label>
        <input type="text" id="lastName" class="form-control" maxlength="190" name="lastName" placeholder="{{trans('adminlte::students.lastName')}}">
    </div>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="email" class="control-label">{{trans('adminlte::adminlte.email')}}<span class="kv-reqd">*</span></label>
        <input type="email" id="email" class="form-control" maxlength="190" name="email" placeholder="{{trans('adminlte::adminlte.email')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="phone" class="control-label">{{trans('adminlte::adminlte.phone')}}</label>
        <input type="text" id="phone" class="form-control" maxlength="190" name="phone" placeholder="{{trans('adminlte::adminlte.phone')}}">
    </div>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="address" class="control-label">{{trans('adminlte::adminlte.direction')}}</label>
        <input type="text" id="address" class="form-control" minlength="8" maxlength="190" name="address" placeholder="{{trans('adminlte::adminlte.direction')}}">
    </div>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="status" class="control-label">{{trans('adminlte::adminlte.status')}}<span class="kv-reqd">*</span></label>
        <select id="status" name="status" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
            <option value=""></option>
            <option value="1">{{trans('adminlte::adminlte.active')}}</option>
            <option value="0">{{trans('adminlte::adminlte.inactive')}}</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="observations" class="control-label">{{trans('adminlte::adminlte.observations')}}</label>
        <textarea id="observations" class="form-control" rows="3" maxlength="5000" name="observations"></textarea>
    </div>
</div>
<div class="row">    
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-bordered">
            <thead>                  
                <tr>
                    <th>{{trans('adminlte::adminlte.file')}}</th>
                    <th><i class="fas fa-paperclip"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Anexo</td>
                    <td><input type="file" id="anexo" class="form-control-file" name="anexo"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>