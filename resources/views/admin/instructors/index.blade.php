@extends('adminlte::page')
@section('title',trans('adminlte::instructors.instructors'))
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fas fa-chalkboard-teacher" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::instructors.instructors')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::instructors.instructors')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            @if($permits['create'])
                <a href="#" class="btn btn-block btn-success btnModalCreate" data-toggle="modal">{{trans('adminlte::adminlte.create')}}</a>
            @endif
        </div>
    </div>
    <div class="card-body">
        {!! $html->table(['class' => 'table table-striped table-bordered dt-responsive table-instructor', 'style' => 'width:100%']) !!}
    </div>
</div>
<div class="modal fade" id="modal-create-instructor" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fa fa-chalkboard-teacher"></i> {{trans('adminlte::instructors.createInstructor')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm-crate-instructor" name="frm-crate-instructor" enctype="multipart/form-data">
                <div class="modal-body" id="content-create-instructor"></div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="btn-save-instructor" name="btn-save-instructor" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-edit-instructor" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fa fa-chalkboard-teacher"></i> {{trans('adminlte::instructors.editInstructor')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm-edit-instructor" name="frm-edit-instructor" enctype="multipart/form-data">
                <div class="modal-body" id="content-edit-instructor"></div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="btn-save-edit-instructor" name="btn-save-edit-instructor" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop
@section('js')
{!! $html->scripts() !!}
<script>
_token = "{{csrf_token()}}";
_method = "PUT";
$(function () {
    
    $(".btnModalCreate").click(function(){
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('instructors.create')}}",
            type:'GET',
            cache:false,
            data:{'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                if(data.status !== 1){
                    ctlErrorRequestAjax(data);
                }//if
                else{
                    $("#content-create-instructor").html(data.html);
                    $('.select2').select2();
                    $('#modal-create-instructor').modal('show');
                }//else
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });
    
    $('#frm-crate-instructor').submit(function(e){
        
        e.preventDefault();
        
        var formData = new FormData(this);
                
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('instructors.store')}}",
            type:'POST',
            data:formData,
            cache:false,
            contentType:false,
            processData:false,
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){                                
                if(data.status === 1){
                    $('#modal-create-instructor').modal('hide');
                    msgNotification(data.type, data.msg);
                    let table = $('.table-instructor').DataTable();
                    table.ajax.reload();
                }else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 

    $(document).on('click','.btnModalEdit',function(){

        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        var id = $(this).data('id');

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('instructors.ajax.edit')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_token':_token,'_method':_method},
            datatype:'html',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                $("#content-edit-instructor").html(data);
                $('.select2').select2();
                $('#modal-edit-instructor').modal('show');
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });

    $('#frm-edit-instructor').submit(function(e){
        
        e.preventDefault();
        
        var formData = new FormData(this);
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('instructors.ajax.update')}}",
            type:'POST',
            data:formData,
            cache:false,
            contentType:false,
            processData:false,
            datatype:'JSON',
            beforeSend:function(){
            $('.contaninerLoading').show();
            },
            complete: function(){
            $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#modal-edit-instructor').modal('hide');
                    msgNotification(data.type,data.msg);
                    let table = $('.table-instructor').DataTable();
                    table.ajax.reload();
                }
                else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 
    
    // Confirm delete
    $('.deleteForm').click(function(){

        let _token = "{{csrf_token()}}";
        let _method = "PUT";
        let id = $('#idDelete').val();

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content') },
            url: "{{route('instructors.ajax.delete')}}",
            type: 'POST',
            cache: false,
            data:{id:id,'_token':_token,'_method':_method},
            datatype: 'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#confirmDelete').modal('hide');
                    msgNotification(data.type, data.msg);
                    let table = $('.table-instructor').DataTable();
                    table.ajax.reload();
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    });

});

</script>
@stop  