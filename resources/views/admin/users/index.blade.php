@extends('adminlte::page')
@section('title',trans('adminlte::users.users'))
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fas fa-user-friends" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::users.users')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::users.users')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            @if($permits['create'])
                <a href="#" class="btn btn-block btn-success btnModalCreate" data-toggle="modal">Crear</a>
            @endif
        </div>
    </div>
    <div class="card-body">
        {!! $html->table(['class' => 'table table-striped table-bordered dt-responsive', 'style' => 'width:100%']) !!}
    </div>
</div>
<div class="modal fade" id="modal-create-user" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fa fa-user-friends"></i> Crear Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="content-create-user"></div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-user" name="btn-save-user" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-edit-user" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info disabled color-palette">
                <h5 class="modal-title"><i class="fa fa-user-friends"></i> Editar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="content-edit-user"></div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-edit-user" name="btn-save-edit-user" class="btn btn-success">{{trans('adminlte::adminlte.save')}}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop
@section('js')
{!! $html->scripts() !!}
<script>
_token = "{{csrf_token()}}";
_method = "PUT";
$(function () {
    
    $(".btnModalCreate").click(function(){
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('users.create')}}",
            type:'GET',
            cache:false,
            data:{'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                if(data.status !== 1){
                    ctlErrorRequestAjax(data);
                }//if
                else{
                    $("#content-create-user").html(data.html);
                    $('.select2').select2();
                    $('#modal-create-user').modal('show');
                }//else
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });
    
    $("#btn-save-user").click(function(){
        
        var roles = $("#roles").val().length ? JSON.stringify($("#roles").val()) : null;
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('users.store')}}",
            type:'POST',
            cache:false,
            data:{roles:roles,name:$("#name").val(),email:$("#email").val(),password:$("#password").val(),password_confirmation:$("#password_confirmation").val(),description:$("#description").val(),'_token':_token},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){                                
                if(data.status === 1){
                    $('#modal-create-user').modal('hide');
                    msgNotification(data.type, data.msg);
                    // Se refresca tabla por ajax
                    let table = $('.table').DataTable();
                    table.ajax.reload();
                }else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 

    $(document).on('click','.btnModalEdit',function(){

        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        var id = $(this).data('id');

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url: "{{route('users.ajax.edit')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_token':_token,'_method':_method},
            datatype:'html',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success:function(data){
                $("#content-edit-user").html(data);
                $('.select2').select2();
                $('#modal-edit-user').modal('show');
            },
            error:function(xhr,textStatus,thrownError){
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });       
    });

    $(document).on('click','#btn-save-edit-user',function (){

        var _token = "{{csrf_token()}}";
        var _method = "PUT";
        var roles = $("#roles_user").val().length ? JSON.stringify($("#roles_user").val()) : null;
        
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('users.ajax.update')}}",
            type:'POST',
            cache:false,
            data:{'_id':$("#id").val(),roles:roles,name:$("#name_user").val(),email:$("#email_user").val(),password:$("#password_user").val(),password_confirmation:$("#password_confirmation_user").val(),description:$("#description_user").val(),'_token':_token,'_method':_method},
            datatype:'JSON',
            beforeSend:function(){
            $('.contaninerLoading').show();
            },
            complete: function(){
            $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#modal-edit-user').modal('hide');
                    msgNotification(data.type,data.msg);
                    // Se refresca tabla por ajax
                    let table = $('.table').DataTable();
                    table.ajax.reload();
                }
                else{
                    ctlErrorRequestAjax(data);
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }); 
    
    // Confirm delete
    $('.deleteForm').click(function(){

        let _token = "{{csrf_token()}}";
        let _method = "PUT";
        let id = $('#idDelete').val();

        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content') },
            url: "{{route('users.ajax.delete')}}",
            type: 'POST',
            cache: false,
            data:{id:id,'_token':_token,'_method':_method},
            datatype: 'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete: function(){
                $('.contaninerLoading').hide(); 
            },
            success: function(data){
                if(data.status === 1){
                    $('#confirmDelete').modal('hide');
                    msgNotification(data.type, data.msg);
                    // Se refresca tabla por ajax
                    let table = $('.table').DataTable();
                    table.ajax.reload();
                }
            },
            error: function (xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    });

});

</script>
@stop  