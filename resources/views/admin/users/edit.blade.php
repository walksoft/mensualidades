<input type="hidden" name="id" id="id" value="{{$user->id}}">
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="roles_user[]" class="control-label">{{trans('adminlte::adminlte.roles')}}<span class="kv-reqd">*</span></label>
        <select name="roles_user[]" id="roles_user" class="form-control select2" multiple="multiple" data-placeholder="{{trans("adminlte::roles.selectRoles")}}"  style="width:100%;">
        @foreach(\App\Models\Role::all() as $role)
            <option @if($user->hasRole($role->slug))  selected="selected" @endif value="{{$role->slug}}">{{ $role->name}}</option>
        @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="name_user" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
        <input type="text" id="name_user" class="form-control" maxlength="190" name="name_user" value="{{$user->name}}" placeholder="{{trans('adminlte::adminlte.name')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="email_user" class="control-label">{{trans('adminlte::adminlte.email')}}<span class="kv-reqd">*</span></label>
        <input type="text" id="email_user" class="form-control" maxlength="190" name="email_user" value="{{$user->email}}" placeholder="{{trans('adminlte::adminlte.email')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="password_user" class="control-label">{{trans('adminlte::adminlte.password')}}</label>
        <input type="password" id="password_user" class="form-control" minlength="8" maxlength="255" name="password_user" placeholder="{{trans('adminlte::adminlte.password')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="password_confirmation_user" class="control-label">{{trans('adminlte::adminlte.confirm_password')}}</label>
        <input type="password" id="password_confirmation_user" class="form-control" minlength="8" maxlength="255" name="password_confirmation_user" placeholder="{{trans('adminlte::adminlte.confirm_password')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="description" class="control-label">{{trans('adminlte::adminlte.observations')}}</label>
        <textarea id="description_user" class="form-control" rows="3" maxlength="1000" name="description_user">{{$user->description}}</textarea>
    </div>
</div>