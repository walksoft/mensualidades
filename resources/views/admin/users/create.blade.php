<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="roles[]" class="control-label">{{trans('adminlte::adminlte.roles')}}<span class="kv-reqd">*</span></label>
        <select name="roles[]" id="roles" class="form-control select2" multiple="multiple" data-placeholder="{{trans("adminlte::roles.selectRoles")}}"  style="width:100%;">
        @foreach(\App\Models\Role::all() as $role)
            <option value="{{$role->slug}}">{{ $role->name}}</option>
        @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="name" class="control-label">{{trans('adminlte::adminlte.name')}}<span class="kv-reqd">*</span></label>
        <input type="text" id="name" class="form-control" maxlength="190" name="name" placeholder="{{trans('adminlte::adminlte.name')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="email" class="control-label">{{trans('adminlte::adminlte.email')}}<span class="kv-reqd">*</span></label>
        <input type="text" id="email" class="form-control" maxlength="190" name="email" placeholder="{{trans('adminlte::adminlte.email')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="password" class="control-label">{{trans('adminlte::adminlte.password')}}<span class="kv-reqd">*</span></label>
        <input type="password" id="password" class="form-control" minlength="8" maxlength="255" name="password" placeholder="{{trans('adminlte::adminlte.password')}}">
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label for="password_confirmation" class="control-label">{{trans('adminlte::adminlte.confirm_password')}}<span class="kv-reqd">*</span></label>
        <input type="password" id="password_confirmation" class="form-control" minlength="8" maxlength="255" name="password_confirmation" placeholder="{{trans('adminlte::adminlte.confirm_password')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="description" class="control-label">{{trans('adminlte::adminlte.observations')}}</label>
        <textarea id="description" class="form-control" rows="3" maxlength="1000" name="description"></textarea>
    </div>
</div>