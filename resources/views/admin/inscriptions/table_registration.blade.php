<table class="table table-bordered dt-responsive table-head-fixed">
    <thead>
        <tr>
            <th>ID</th>
            <th>{{trans('adminlte::adminlte.name')}}</th>
            <th>{{trans('adminlte::adminlte.email')}}</th>
            <th>{{trans('adminlte::adminlte.phone')}}</th>
            <th>{{trans('adminlte::inscriptions.inscription')}}</th>
            <th>{{trans('adminlte::adminlte.status')}}</th>
            <th>{{trans('adminlte::adminlte.delete')}}</th>
        </tr>
    </thead>
    <tbody>
        @if($course->registration->count() > 0)
            @foreach($course->registration as $registration)
                <tr>
                    <td>{{$registration->student->id}}</td>
                    <td>{{$registration->student->name()}}</td>
                    <td>{{$registration->student->email}}</td>
                    <td>{{$registration->student->phone_number}}</td>
                    <td>{{\Carbon\Carbon::parse($registration->created_at)->format('d/m/Y')}}</td>
                    <td>
                        @if($registration->student->status)                        
                            <span class="badge badge-success">{{trans('adminlte::adminlte.active')}}</span>
                        @else
                            <span class="badge badge-danger">{{trans('adminlte::adminlte.inactive')}}</span>
                        @endif                    
                    </td>
                    <td>
                        <a href="" class="confirmDelete-inscription" data-id="{{$registration->id}}" data-toggle="modal" data-target="#confirmDelete-inscription"><span class="badge badge-danger">{{trans('adminlte::adminlte.delete')}}</span></a>
                    </td>
                </tr>
            @endforeach
        @else
        <tr>
            <td colspan="7">No se encontraron alumnos inscritos...</td>
        </tr>
        @endif
    </tbody>
</table>