{{csrf_field()}}   
<input type="hidden" name="course_id" id="course_id" value="{{$course->course_id}}">
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:0px">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title" style="font-size:1.0rem">
                    <i class="fab fa-buffer"></i> {{$course->code}} - {{$course->service->name}} | <i class="fas fa-chalkboard-teacher"></i> {{$course->instructor->name()}} | <i class="far fa-calendar-alt"></i> {{implode(' - ',$course->array_frequency())}} | <i class="far fa-clock"></i> {{\Carbon\Carbon::parse($course->start_time)->format('g:iA')}} - {{\Carbon\Carbon::parse($course->end_time)->format('g:iA')}} | [ <i class="text-success fas fa-users"></i> <span id="total-inscriptions" name="total-inscriptions">{{\App\Models\Student_registration::where('course_id',$course->course_id)->count()}}</span> ]
                </h5>
            </div>
        </div>
        <!-- /.card -->
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="student" class="control-label">{{trans('adminlte::students.student')}}</label>
        <select id="student" name="student" class="form-control form-control-sm select-student-inscription select2 select2-hidden-accessible" data-placeholder="{{trans('adminlte::students.student')}}" style="width:100%;">
            <option value="">--Seleccione--</option>
            @foreach(\App\User::where('status',1)->where('type_user',3)->whereNotIn('id',\App\Models\Student_registration::where('course_id',$course->course_id)->pluck('user_id'))->orderBy('name','ASC')->get() as $student)
                <option value="{{$student->id}}">{{$student->name()}}</option>
            @endforeach
        </select>        
    </div>    
    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <label for="value_monthlyCharge" class="control-label">{{trans('adminlte::services.monthlyCharge')}}</label>
        <input type="number" value="{{$course->service->monthly_value}}" @if(!auth()->user()->hasPermissionTo('update_values.inscription')) disabled="disabled" @endif id="value_monthlyCharge" class="form-control" minlength="1" maxlength="10" name="value_monthlyCharge" placeholder="{{trans('adminlte::services.monthlyCharge')}}">
    </div>
    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <label for="value_promptPaymentCollection" class="control-label">{{trans('adminlte::services.promptPaymentCollection')}}</label>
        <input type="number" value="{{$course->service->timely_payment_value}}" @if(!auth()->user()->hasPermissionTo('update_values.inscription')) disabled="disabled" @endif id="value_promptPaymentCollection" class="form-control" minlength="1" maxlength="10" name="value_promptPaymentCollection" placeholder="{{trans('adminlte::services.promptPaymentCollection')}}">
    </div>
    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <label for="value_inscription" class="control-label">{{trans('adminlte::services.inscription')}}</label>
        <input type="text" value="{{ $course->service->registration_value }}" @if(!auth()->user()->hasPermissionTo('update_values.inscription')) disabled="disabled" @endif id="value_inscription" class="form-control" minlength="1" maxlength="10" name="value_inscription" placeholder="{{trans('adminlte::services.inscription')}}">
    </div>
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="observations-inscription" class="control-label">{{trans('adminlte::adminlte.observations')}}</label>
        <textarea id="observations-inscription" class="form-control" style="resize:none" rows="2" maxlength="1000" name="observations-inscription"></textarea>
    </div>
</div>  
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:0px">
        <div class="card">
            <div class="card-header" id="card-student" style="display:none">
                <h5 class="card-title"><i class="fas fa-hashtag"></i> <span id="id-student"></span> - <i class="fas fa-user-graduate"></i> <span id="name-student"></span> - <i class="fas fa-envelope"></i> <span id="email-student"></span> - <i class="fas fa-phone-square"></i> <span id="phone-student"></span></h5>
                <button type="button" id="btn-save-inscription" name="btn-save-inscription" class="btn btn-sm btn-success float-right">{{trans('adminlte::inscriptions.inscribe')}}</button>
            </div>
        </div>  
    </div>
</div>
<div class="row">    
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card-body table-responsive p-0" id="table_registration" style="height:300px;">
            @include('admin.inscriptions.table_registration')
        </div>
    </div>
</div>
