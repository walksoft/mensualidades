@if(auth()->user()->hasRole('student999'))    

    @include('admin.dashboards.student')
    
@else

    @include('admin.dashboards.admin')
    
@endif

