<div class="row">
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="student_cash_register" class="control-label">{{trans('adminlte::students.student')}}</label>
        <select id="student_cash_register" name="student_cash_register" class="form-control select-student-cash-register select2 select2-hidden-accessible" data-placeholder="{{trans("adminlte::students.student")}}" style="width:100%;">
            <option value=""></option>
            @foreach(\App\User::where('status',1)->where('type_user',3)->get() as $user)
                <option value="{{$user->id}}">{{$user->name}} {{$user->last_name}}</option>
            @endforeach
        </select>
    </div>    
    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <label for="value_cash_register" class="control-label">{{trans('adminlte::payments.value')}}</label>
        <input type="text" value="" disabled="disabled" id="value_cash_register" class="form-control" minlength="1" maxlength="10" name="value_cash_register" placeholder="{{trans('adminlte::payments.value')}}">
    </div>
    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <label for="additional_cash_register" class="control-label">{{trans('adminlte::payments.additional')}}</label>
        <input type="number" value="" id="additional_cash_register" class="form-control" minlength="1" maxlength="10" name="additional_cash_register" placeholder="{{trans('adminlte::payments.additional')}}">
    </div>
    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <label for="discount_cash_register" class="control-label">{{trans('adminlte::payments.discount')}}</label>
        <input type="number" value="" id="discount_cash_register" class="form-control" minlength="1" maxlength="10" name="discount_cash_register" placeholder="{{trans('adminlte::payments.discount')}}">
    </div>
    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <label for="total_cash_register" class="control-label">{{trans('adminlte::payments.total')}}</label>
        <input type="text" value="" disabled="disabled" id="total_cash_register" class="form-control" minlength="1" maxlength="10" name="total_cash_register" placeholder="{{trans('adminlte::payments.total')}}">
    </div>    
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <textarea id="observations_cash_register" class="form-control" rows="1" maxlength="1000" name="observations_cash_register" placeholder="{{trans('adminlte::payments.observations')}}" style="resize:none"></textarea>
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:0px">
        <div class="card">
            <div class="card-header" id="card-student-frm-cash" style="display:none">
                <h5 class="card-title" style="font-size:0.9rem">
                    <i class="fas fa-hashtag"></i> <span id="id-student-frm-cash"></span> | <i class="fas fa-user-graduate"></i> <span id="name-student-frm-cash"></span> | <i class="fas fa-envelope"></i> <span id="email-student-frm-cash"></span> | <i class="fas fa-phone-square"></i> <span id="phone-student-frm-cash"></span> - <i id="icon-code-course-student-frm-cash" style="display:none" class="fab fa-buffer"></i> <span id="code-course-student-frm-cash"></span> <span class="pipe-line-frm-cash" style="display:none">|</span> <i id="icon-name-instructor-frm-cash" style="display:none" class="fas fa-chalkboard-teacher"></i> <span id="name-instructor-frm-cash"></span> <span class="pipe-line-frm-cash" style="display:none">|</span> <i id="icon-course-frequency-frm-cash" style="display:none" class="far fa-calendar-alt"></i> <span id="course-frequency-frm-cash"></span> <span class="pipe-line-frm-cash" style="display:none">|</span> <i id="icon-course-hour-frm-cash" style="display:none" class="far fa-clock"></i> <span id="course-hour-frm-cash"></span>
                </h5>                
            </div>
        </div>  
    </div>
</div>
<div class="row">    
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" style="width:200px;">
                @if(auth()->user()->hasPermissionTo('generate.payment'))
                    <div id="content_add_new_payment_button" style="float:right;padding:5px;"></div> 
                @endif
                <a class="nav-link active" id="pagos-tab-cash-register" data-toggle="tab" href="#tabPagos-cash-register" role="tab" aria-controls="tabPagos-cash-register" aria-selected="false">{{trans('adminlte::payments.pendingPayments')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pagos-records-tab-cash-register" data-toggle="tab" href="#tabPagos-records-cash-register" role="tab" aria-controls="tabPagos-records-cash-register" aria-selected="false">{{trans('adminlte::payments.paymentHistory')}}</a>
            </li>         
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tabPagos-cash-register" role="tabpanel" aria-labelledby="pagos-tab-cash-register">
                <div class="card-body table-responsive p-0" id="table_cash_register" style="height:200px;">
                    <table class="table table-bordered dt-responsive table-head-fixed">
                        <thead>
                            <tr>
                                <th style="width:10px;text-align:center">#</th>
                                <th>{{trans('adminlte::payments.reference')}}</th>
                                <th>{{trans('adminlte::payments.amountToPaid')}}</th>
                                <th>{{trans('adminlte::payments.concept')}}</th>
                                <th>{{trans('adminlte::payments.course')}}</th>
                                <th>{{trans('adminlte::payments.status')}}</th>
                                <th style="text-align:center">{{trans('adminlte::payments.pay')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7">No se encontraron pagos pendientes...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>                 
            </div>    
            <div class="tab-pane" id="tabPagos-records-cash-register" role="tabpanel" aria-labelledby="pagos-records-tab-cash-register">
                <div class="card-body table-responsive p-0" id="table_record_cash_register" style="height:200px;">
                    <table class="table table-bordered dt-responsive table-head-fixed">
                        <thead>
                            <tr>
                                <th style="width:10px;text-align:center">#</th>
                                <th>{{trans('adminlte::payments.reference')}}</th>
                                <th>{{trans('adminlte::payments.total')}}</th>
                                <th>{{trans('adminlte::payments.concept')}}</th>
                                <th>{{trans('adminlte::payments.course')}}</th>
                                <th>{{trans('adminlte::payments.status')}}</th>
                                <th>{{trans('adminlte::payments.paymentDate')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7">No se encontraron pagos pendientes...</td>
                            </tr>
                        </tbody>
                    </table>                   
                </div>                    
            </div>
        </div>                                
    </div>
</div>   