@extends('adminlte::page')
@section('title',trans('adminlte::payments.payments'))
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1>
            <i class="fas fa-file-invoice-dollar" style="margin:10px 10px 10px 10px;"></i>
            {{trans('adminlte::payments.payments')}}
        </h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::payments.payments')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <button id="btn-export" name="btn-export" class="btn btn-success"><i class="fas fa-file-export"></i>&nbsp;&nbsp;Exportar</button>
        </div>
    </div>
    <div class="card-body">
        <div class="row ">
            <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="status" class="control-label">{{trans('adminlte::payments.status')}}</label>
                <select id="status" name="status" class="form-control select2 select2-hidden-accessible" data-placeholder="{{trans("adminlte::payments.status")}}" style="width:100%;">
                    <option value=""></option>
                    @foreach(\App\Models\Status_payment::where('status',1)->get() as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-xs-2 col-sm-2 col-md-3 col-lg-2">
                <label for="concept" class="control-label">{{trans('adminlte::payments.concept')}}</label>
                <select id="concept" name="concept" class="form-control select2 select2-hidden-accessible" data-placeholder="{{trans("adminlte::payments.concept")}}" style="width:100%;">
                    <option value=""></option>
                    @foreach(\App\Models\Payment_concept::where('status',1)->get() as $concept)
                        <option value="{{$concept->id}}">{{$concept->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">                                
                <label for="start_date" class="control-label">{{trans('adminlte::payments.startDate')}}</label>
                <div class="input-group date datetimepicker" id="content_start_date" name="content_start_date" data-target-input="nearest">
                      <input type="text" id="start_date" name="start_date" class="form-control datetimepicker-input" data-target="#content_start_date"/>
                      <div class="input-group-append" data-target="#content_start_date" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                  </div>
            </div>
            <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="end_date" class="control-label">{{trans('adminlte::payments.endDate')}}</label>
                <div class="input-group date datetimepicker" id="content_end_date" name="content_end_date" data-target-input="nearest">
                    <input type="text" id="end_date" name="end_date" class="form-control datetimepicker-input" data-target="#content_end_date"/>
                    <div class="input-group-append" data-target="#content_end_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
            <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                <label>&nbsp;&nbsp;</label><br>
                <button id="btn-filter" name="btn-filter" class="btn btn-info"><i class="fas fa-filter"></i>&nbsp;&nbsp;Filtrar</button>
            </div>
            <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                <label>&nbsp;&nbsp;</label><br>
                <button id="btn-clear-filter" name="btn-clear-filter" class="btn btn-info"><i class="fas fa-eraser"></i>&nbsp;&nbsp;Limpiar</button>
            </div>
        </div>
    </div>
    <div class="card-body">   
        {!! $html->table(['class' => 'table table-striped dt-responsive', 'style' => 'width:100%']) !!}
    </div>
    <!-- /.card-body -->
    <div class="card-footer"></div>
    <!-- /.card-footer-->
</div>
@stop
@section('js')
{!! $html->scripts() !!}
<script>
_token = "{{csrf_token()}}";
_method = "PUT";
$(document).ready(function(){
    
    table = $('.table').DataTable();
    
    $('.select2').select2({allowClear:true});  
    $('.datetimepicker').datetimepicker({format:'DD-MM-YYYY'});
    
    $("#btn-filter").click(function(){        
        table.ajax.reload();
    });
    
    $("#btn-clear-filter").click(function(){        
        $('#status').val(null).trigger('change');
        $('#concept').val(null).trigger('change');
        $("#start_date").val('');
        $("#end_date").val('');   
        table.ajax.reload();
    });    
    
    $("#btn-export").click(function(){         
        var status = $('#status').val() ? $('#status').val() : '0';
        var concept = $('#concept').val() ? $('#concept').val() : '0';
        var start_date = $('#start_date').val() ? $('#start_date').val() : '0';
        var end_date = $('#end_date').val() ? $('#end_date').val() : '0';        
        var url = "{{route('payments.export.excel',['status'=>'STATUS','concept'=>'CONCEPT','start_date'=>'STARTDATE','end_date'=>'ENDDATE'])}}";        
        url = url.replace('STATUS',status).replace('CONCEPT',concept).replace('STARTDATE',start_date).replace('ENDDATE',end_date);
        location.href = url;        
    }); 
    
          
 
});
</script>
 @stop