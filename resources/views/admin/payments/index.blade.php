@extends('adminlte::page')
@section('title',trans('adminlte::payments.box'))
@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1><i class="fas fa-dollar-sign" style="margin:10px 10px 10px 10px;"></i>{{trans('adminlte::payments.box')}}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::payments.box')}}</li>
        </ol>
    </div>
</div>
@stop
@section('content')
<div class="card">
    <form id="form_cash_register" name="form_cash_register">
        {{csrf_field()}}
        {{ method_field('PUT') }}
        <div class="card-body">
            <div class="row">
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="student_cash_register" class="control-label">{{trans('adminlte::payments.searchStudentInstructor')}}</label>
                    <select id="student_cash_register" name="student_cash_register" class="form-control select-student-cash-register select2 select2-hidden-accessible" data-placeholder="{{trans("adminlte::payments.searchStudentInstructor")}}" style="width:100%;">
                        <option value=""></option>
                        @foreach(\App\User::where('status',1)->whereIn('type_user',[3,2])->orderBy('name','ASC')->get() as $user)
                        <option data-type="{{$user->type_user}}" value="{{$user->id}}">{{$user->name}} {{$user->last_name}}</option>
                        @endforeach
                    </select>
                </div>                                   
            </div>
            <div class="row">
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:0px">
                    <div class="card">
                        <div class="card-header" id="card-student-frm-cash" style="display:none">
                            <h5 class="card-title" style="font-size:0.9rem">
                                <i class="fas fa-hashtag"></i> <span id="id-student-frm-cash"></span> | <i id="icon-fa-user-graduate" class="fas fa-user-graduate"></i> <span id="name-student-frm-cash"></span> | <i class="fas fa-envelope"></i> <span id="email-student-frm-cash"></span> | <i class="fas fa-phone-square"></i> <span id="phone-student-frm-cash"></span> - <i id="icon-code-course-student-frm-cash" style="display:none" class="fab fa-buffer"></i> <span id="code-course-student-frm-cash"></span> <span class="pipe-line-frm-cash" style="display:none">|</span> <i id="icon-name-instructor-frm-cash" style="display:none" class="fas fa-chalkboard-teacher"></i> <span id="name-instructor-frm-cash"></span> <span class="pipe-line-frm-cash" style="display:none">|</span> <i id="icon-course-frequency-frm-cash" style="display:none" class="far fa-calendar-alt"></i> <span id="course-frequency-frm-cash"></span> <span class="pipe-line-frm-cash" style="display:none">|</span> <i id="icon-course-hour-frm-cash" style="display:none" class="far fa-clock"></i> <span id="course-hour-frm-cash"></span>
                            </h5>                
                        </div>
                    </div>  
                </div>
            </div>
            <div class="row">    
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" style="width:200px;">
                            @if(auth()->user()->hasPermissionTo('generate.payment'))
                                <div id="content_add_new_payment_button" style="float:right;padding:5px;"></div> 
                            @endif
                            <a class="nav-link active" id="pagos-tab-cash-register" data-toggle="tab" href="#tabPagos-cash-register" role="tab" aria-controls="tabPagos-cash-register" aria-selected="false">{{trans('adminlte::payments.pendingPayments')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pagos-records-tab-cash-register" data-toggle="tab" href="#tabPagos-records-cash-register" role="tab" aria-controls="tabPagos-records-cash-register" aria-selected="false">{{trans('adminlte::payments.paymentHistory')}}</a>
                        </li>         
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabPagos-cash-register" role="tabpanel" aria-labelledby="pagos-tab-cash-register">
                            <div class="card-body table-responsive p-0" id="table_cash_register" style="height:200px;">
                                <table class="table table-bordered dt-responsive table-head-fixed">
                                    <thead>
                                        <tr>
                                            <th style="width:10px;text-align:center">#</th>
                                            <th>{{trans('adminlte::payments.reference')}}</th>
                                            <th>{{trans('adminlte::payments.amountToPaid')}}</th>
                                            <th>{{trans('adminlte::payments.concept')}}</th>
                                            <th>{{trans('adminlte::payments.course')}}</th>
                                            <th>{{trans('adminlte::payments.status')}}</th>
                                            <th style="text-align:center">{{trans('adminlte::payments.pay')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7">No se encontraron pagos pendientes...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                 
                        </div>    
                        <div class="tab-pane" id="tabPagos-records-cash-register" role="tabpanel" aria-labelledby="pagos-records-tab-cash-register">
                            <div class="card-body table-responsive p-0" id="table_record_cash_register" style="height:200px;">
                                <table class="table table-bordered dt-responsive table-head-fixed">
                                    <thead>
                                        <tr>
                                            <th style="width:10px;text-align:center">#</th>
                                            <th>{{trans('adminlte::payments.reference')}}</th>
                                            <th>{{trans('adminlte::payments.total')}}</th>
                                            <th>{{trans('adminlte::payments.concept')}}</th>
                                            <th>{{trans('adminlte::payments.course')}}</th>
                                            <th>{{trans('adminlte::payments.status')}}</th>
                                            <th>{{trans('adminlte::payments.paymentDate')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7">No se encontraron pagos pendientes...</td>
                                        </tr>
                                    </tbody>
                                </table>                   
                            </div>                    
                        </div>
                    </div>                                
                </div>
            </div>   
            <div class="row">
                <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <label for="value_cash_register" class="control-label">{{trans('adminlte::payments.value')}}</label>
                    <input type="text" value="" disabled="disabled" id="value_cash_register" class="form-control" minlength="1" maxlength="10" name="value_cash_register" placeholder="{{trans('adminlte::payments.value')}}">
                </div>
                <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <label for="additional_cash_register" class="control-label">{{trans('adminlte::payments.additional')}}</label>
                    <input type="number" value="" id="additional_cash_register" class="form-control" minlength="1" maxlength="10" name="additional_cash_register" placeholder="{{trans('adminlte::payments.additional')}}">
                </div>
                <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <label for="discount_cash_register" class="control-label">{{trans('adminlte::payments.discount')}}</label>
                    <input type="number" value="" id="discount_cash_register" class="form-control" minlength="1" maxlength="10" name="discount_cash_register" placeholder="{{trans('adminlte::payments.discount')}}">
                </div>
                <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <label for="total_cash_register" class="control-label">{{trans('adminlte::payments.total')}}</label>
                    <input type="text" value="" disabled="disabled" id="total_cash_register" class="form-control" minlength="1" maxlength="10" name="total_cash_register" placeholder="{{trans('adminlte::payments.total')}}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea id="observations_cash_register" class="form-control" rows="3" maxlength="1000" name="observations_cash_register" placeholder="{{trans('adminlte::payments.observations')}}" style="resize:none"></textarea>
                </div>
            </div>            
            <div class="row">
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @if(auth()->user()->hasPermissionTo('cash.register'))
                        <button type="button" id="btn_frm_cash_register" name="btn_frm_cash_register" class="btn btn-success">{{trans('adminlte::payments.savePayment')}}</button>
                    @endif   
                </div>
            </div>                                        
        </div>        
    </form>
</div>
@if(auth()->user()->hasPermissionTo('generate.payment'))
    <div class="modal fade" id="modal_frm_add_new_payment" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">                                           
                <div class="modal-header bg-gradient-info disabled color-palette">
                    <h5 class="modal-title"><i class="fas fa-plus-circle"></i> {{trans('adminlte::payments.generateCharge')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="form_add_new_payment" name="form_add_new_payment">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                    <div class="modal-body" id="content_frm_add_new_payment"></div>
                </form>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btn_frm_add_new_payment" name="btn_frm_add_new_payment" class="btn btn-sm btn-success">{{trans('adminlte::payments.generateCharge')}}</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endif
@stop
@section('js')
<script>
_token = "{{csrf_token()}}";
_method = "PUT";
$(document).ready(function(){    
    $('.select-student-cash-register').select2({templateResult:format_select2,allowClear:true}).on('select2:select',function(e){
        info_student_frm_cash(e.params.data.id);
    });         
});
$(document).on('click',"#btn_frm_cash_register",function(){
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url:"{{route('payments.ajax.store')}}",
        type:'POST',
        cache:false,
        data:$("#form_cash_register").serialize(),
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete:function(){
            $('.contaninerLoading').hide();
        },
        success:function(data){
            if(data.status === 1){
                msgNotification(data.type,data.msg);  
                clear_values_cash(); 
                refresh_tables_cash_register(data.id,0);
            }//if
            else{                
                ctlErrorRequestAjax(data);
            }//else
        },
        error:function(xhr,textStatus,thrownError){
            $('.contaninerLoading').hide();
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });         
});
$(document).on('change',"#additional_cash_register, #discount_cash_register",function(){    
    var id = $("input[name='radio_selected_payment_frm_cash']:checked").data('payment');
    if(!id){
        return false;
    }//if
    var additional = $("#additional_cash_register").val();
    var discount = $("#discount_cash_register").val();
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url:"{{route('payments.ajax.get_total')}}",
        type:'POST',
        cache:false,
        data:{'_id':id,'_additional':additional,'_discount':discount,'_token':_token,'_method':'PUT'},
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete:function(){
            $('.contaninerLoading').hide();
        },
        success:function(data){
            if(data.status === 1){
                $("#total_cash_register").val('$'+data.total);                                                            
            }//if
            else{                
                ctlErrorRequestAjax(data);
            }//else
        },
        error:function(xhr,textStatus,thrownError){
            $('.contaninerLoading').hide();
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    }); 
});
$(document).on('click',".radio_selected_payment_frm_cash",function(){
    $(".cell_table_cash_register").removeAttr('style');
    $(".radio_deselect_payment_frm_cash").css('display','none');
    $(this).next().next().css('display','inline-block');
    $(this).parent().parent().parent().css('background-color','#22a6bb');
    info_course_frm_cash($(this).data('course'),$(this).data('payment'));
});
$(document).on('click',".radio_deselect_payment_frm_cash",function(){
    $(this).prev().prev().prop("checked",false);
    $(".cell_table_cash_register").removeAttr('style');
    clear_info_course();
    $(this).css('display','none');
});
$(document).on('click',"#btn_frm_add_new_payment",function(){
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url:"{{route('payments.ajax.store_new_payment')}}",
        type:'POST',
        cache:false,
        data:{'id':$("#student_cash_register").val(),'concept':$("#concept_add_new_payment").val(),'course':$("#class_add_new_payment").val(),'month':$("#month_add_new_payment").val(),'_token':_token,'_method':'PUT'},
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete:function(){
            $('.contaninerLoading').hide();
        },
        success:function(data){
            if(data.status === 1){
                msgNotification(data.type,data.msg);
                refresh_tables_cash_register(data.id,data.id_new);                
                $("#modal_frm_add_new_payment").modal('hide');
            }//if
            else{                
                ctlErrorRequestAjax(data);
            }//else
        },
        error:function(xhr,textStatus,thrownError){
            $('.contaninerLoading').hide();
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });
});
$(document).on('click',"#add_new_cash_register",function(){
    $.ajax({
        headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
        url:"{{route('payments.ajax.add_new_payment')}}",
        type:'POST',
        cache:false,
        data:{'_token':_token,'_method':'PUT'},
        datatype:'JSON',
        beforeSend:function(){
            $('.contaninerLoading').show();
        },
        complete:function(){
            $('.contaninerLoading').hide();
        },
        success:function(data){
            if(data.status !== 1){
                ctlErrorRequestAjax(data);
            }//if
            else{                
                $("#content_frm_add_new_payment").html(data.html);        
                $('.select-concept-add-new-payment').select2({allowClear:true}).on('select2:select',function(e){
                    get_courses_student(e.params.data.id,$("#student_cash_register").val());
                });                
                $('.select-class-add-new-payment').select2({allowClear:true});                
                $('.select-month-add-new-payment').select2({allowClear:true});
                $("#modal_frm_add_new_payment").modal('show');
            }//else
        },
        error:function(xhr,textStatus,thrownError){
            $('.contaninerLoading').hide();
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });   
});
function refresh_tables_cash_register(id,id_new){
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('payments.ajax.get_courses_student')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_ind_new':id_new,'_token':_token,'_method':'PUT'},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide();
            },
            success:function(data){              
                //tables Payments
                $("#table_cash_register").html(data.tbl_cash_register);  
                if($('#content_add_new_payment_button').length){
                    $("#content_add_new_payment_button").html(data.add_new_payment_button);  
                }//if
                $("#table_record_cash_register").html(data.tbl_record_cash_register);                                
            },
            error:function(xhr,textStatus,thrownError){
                $('.contaninerLoading').hide();
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });    
}//refresh_tables_cash_register
function info_student_frm_cash(id){  
        clear_info_course();
        clear_info_student();
        if(id === ''){
            return false;
        }//if               
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('payments.ajax.get_courses_student')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_ind_new':0,'_token':_token,'_method':'PUT'},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide();
            },
            success:function(data){
                var name = data.student.name+(data.student.last_name ? ' '+data.student.last_name : '');
                $("#card-student-frm-cash").css('display','block');
                $("#icon-fa-user-graduate").removeClass();
                if(data.student.type_user === 2){
                    $("#icon-fa-user-graduate").addClass('fas fa-chalkboard-teacher');
                }//if
                else{
                    $("#icon-fa-user-graduate").addClass('fas fa-user-graduate');    
                }//else
                $("#name-student-frm-cash").html(name);
                $("#id-student-frm-cash").html(data.student.id);
                $("#email-student-frm-cash").html(data.student.email);
                $("#phone-student-frm-cash").html(data.student.phone_number);                
                //tables Payments
                $("#table_cash_register").html(data.tbl_cash_register);   
                if($('#content_add_new_payment_button').length){
                    $("#content_add_new_payment_button").html(data.add_new_payment_button);                
                }//if
                $("#table_record_cash_register").html(data.tbl_record_cash_register);                                
            },
            error:function(xhr,textStatus,thrownError){
                $('.contaninerLoading').hide();
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });     
}//update_info_student
function get_courses_student(concept,id){  
        if(id === '' || concept === ''){
            return false;
        }//if               
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('payments.ajax.get_courses_student_new_payment')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_concept':concept,'_token':_token,'_method':'PUT'},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide();
            },
            success:function(data){                
                $('.select-class-add-new-payment').empty();                                        
                $('.select-class-add-new-payment').select2({data:data.courses});
                $('.select-class-add-new-payment').select2('destroy').val("all").select2();
                $('.select-class-add-new-payment').select2({allowClear:true}).on('select2:select',function(e){
                    info_value_cash_new_payment($("#concept_add_new_payment").val(),e.params.data.id,$("#student_cash_register").val());
                });
            },
            error:function(xhr,textStatus,thrownError){
                $('.contaninerLoading').hide();
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
}//get_courses_student
function info_course_frm_cash(id,payment){
        clear_info_course();
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('payments.ajax.info_course')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_payment':payment,'_token':_token,'_method':'PUT'},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide();
            },
            success:function(data){    
                if(data.course.course_id){
                    $(".pipe-line-frm-cash").removeAttr('style');
                    $("#icon-code-course-student-frm-cash").removeAttr('style');
                    $("#code-course-student-frm-cash").html(data.course.code_name);
                    $("#icon-name-instructor-frm-cash").removeAttr('style');
                    $("#name-instructor-frm-cash").html(data.course.instructor);                
                    $("#icon-course-frequency-frm-cash").removeAttr('style');
                    $("#course-frequency-frm-cash").html(data.course.calendar);                                
                    $("#icon-course-hour-frm-cash").removeAttr('style');
                    $("#course-hour-frm-cash").html(data.course.clock);  
                }//if
                var amount = ((data.payment.amount) ? data.payment.amount : 0);
                $("#value_cash_register").val('$'+amount);               
                $("#total_cash_register").val('$'+amount);
            },
            error:function(xhr,textStatus,thrownError){
                $('.contaninerLoading').hide();
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });    
}//info_course_frm_cash
function info_value_cash_new_payment(concept,course,id){
        if(concept ==='' || course === '' || id === ''){
            return false;
        }//if    
        $.ajax({
            headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
            url:"{{route('payments.ajax.info_value_cash_new_payment')}}",
            type:'POST',
            cache:false,
            data:{'_id':id,'_concept':concept,'_course':course,'_token':_token,'_method':'PUT'},
            datatype:'JSON',
            beforeSend:function(){
                $('.contaninerLoading').show();
            },
            complete:function(){
                $('.contaninerLoading').hide();
            },
            success:function(data){                
                var amount = ((data.amount) ? data.amount : 0);
                $("#value_add_new_payment").val(amount);
            },
            error:function(xhr,textStatus,thrownError){
                $('.contaninerLoading').hide();
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });     
}//info_value_cash_new_payment
function clear_info_course(){
        clear_values_cash();
        $("#icon-code-course-student-frm-cash").css('display','none');
        $("#code-course-student-frm-cash").html('');                
        $("#icon-name-instructor-frm-cash").css('display','none');
        $("#name-instructor-frm-cash").html('');                
        $("#icon-course-frequency-frm-cash").css('display','none');
        $("#course-frequency-frm-cash").html('');                                
        $("#icon-course-hour-frm-cash").css('display','none');
        $("#course-hour-frm-cash").html('');         
        $(".pipe-line-frm-cash").css('display','none');
}//clear_info_course
function clear_values_cash(){
    $("#value_cash_register").val('');        
    $("#additional_cash_register").val('');
    $("#discount_cash_register").val('');        
    $("#total_cash_register").val(''); 
    $("#observations_cash_register").val('');
}//clear_values_cash
function clear_info_student(){
    $("#card-student-frm-cash").css('display','none');
    $("#name-student-frm-cash").html('');
    $("#id-student-frm-cash").html('');
    $("#email-student-frm-cash").html('');
    $("#phone-student-frm-cash").html('');
    if($('#content_add_new_payment_button').length){
        $("#content_add_new_payment_button").html(''); 
    }//if
}//clear_info_student 

function format_select2(data){
    if($(data.element).data('type') === 2){
        return $('<span class="badge badge-danger right"><i class="fas fa-chalkboard-teacher"></i></span>&nbsp;&nbsp;<span>'+data.text+'</span>');
    }//if
    else{
        return $('<span class="badge badge-success right"><i class="fas fa-user-graduate"></i></span>&nbsp;&nbsp;<span>'+data.text+'</span>');    
    }//else    
}//format_select2

</script>
@stop 