<table class="table table-bordered dt-responsive table-head-fixed">
    <thead>
        <tr>
            <th style="width:10px;text-align:center">#</th>
            <th>{{trans('adminlte::payments.reference')}}</th>            
            @if($ind == 1)
                <th style="text-align:center">{{trans('adminlte::payments.amountToPaid')}}</th>
            @else
                <th style="text-align:center">{{trans('adminlte::payments.total')}}</th>
            @endif            
            <th>{{trans('adminlte::payments.concept')}}</th>
            <th>{{trans('adminlte::payments.deadline')}}</th>                        
            <th>{{trans('adminlte::payments.course')}}</th>
            <th>{{trans('adminlte::payments.status')}}</th>
            @if($ind == 2)
                <th>{{trans('adminlte::payments.payment_method')}}</th>
                <th>{{trans('adminlte::payments.paymentDate')}}</th>
            @endif
            @if($ind == 1)
                <th style="text-align:center">{{trans('adminlte::payments.pay')}}</th>
            @endif
        </tr>
    </thead>
    <tbody>        
        @if(count($payments) > 0)
            @foreach($payments as $payment)
            <tr class="@if($ind == 1)cell_table_cash_register @else cell_table_record_cash_register @endif">
                    <td style="width:10px;text-align:center">{{$count++}}</td>
                    <td>{{$payment->reference}}</td>
                    @if($ind == 1)
                        <td style="text-align:center">${{number_format($payment->amount,0,',','.')}}</td>
                    @else
                        <td style="text-align:center">${{number_format($payment->total_paid,0,',','.')}}</td>
                    @endif
                    <td>{{$payment->concept->name}} @if($ind_new == $payment->id)<span class="badge badge-success">Nuevo</span>@endif</td>
                    <td>@if($payment->deadline) {{\Carbon\Carbon::parse($payment->deadline)->format('d/m/Y')}} @endif</td>
                    <td>@if($payment->course) {{$payment->course->code}} - {{$payment->course->service->name}} @endif</td>
                    <td>{{$payment->status->name}}</td>
                    @if($ind == 2)
                        <td>{{$payment->payment_method}}</td>
                        <td>@if($payment->payment_date) {{\Carbon\Carbon::parse($payment->payment_date)->format('d/m/Y g:i A')}} @endif</td>
                    @endif
                    @if($ind == 1)
                        <td style="text-align:center;padding:0px;margin:0px;">
                            <div class="icheck-danger">
                                <input type="radio" data-course="@if($payment->course) {{$payment->course->course_id}} @endif" data-payment="{{$payment->id}}" value="{{$payment->id}}" id="radio_selected_payment_frm_cash{{$payment->id}}" name="radio_selected_payment_frm_cash" class="radio_selected_payment_frm_cash">
                                <label for="radio_selected_payment_frm_cash{{$payment->id}}"></label>
                                <a class="radio_deselect_payment_frm_cash text-danger" href="javascript:void(0);" style="display:none"><i class="fas fa-undo"></i></a>
                            </div>
                        </td>
                    @endif
                </tr>
            @endforeach
        @else
            <tr>
                @if($ind == 1)
                    <td colspan="7">
                @else
                    <td colspan="7">
                @endif
                    No se encontraron pagos pendientes...
                </td>
            </tr>
        @endif        
    </tbody>
</table>