<div class="row">   
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="concept_add_new_payment" class="control-label">{{trans('adminlte::payments.concept')}}</label>
        <select id="concept_add_new_payment" name="concept_add_new_payment" class="form-control select-concept-add-new-payment select2 select2-hidden-accessible" data-placeholder="{{trans("adminlte::payments.concept")}}" style="width:100%;">
            <option value=""></option>
            @foreach(\App\Models\Payment_concept::where('status',1)->get() as $concept)
                <option value="{{$concept->id}}">{{$concept->name}}</option>
            @endforeach
        </select>
    </div>    
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="class_add_new_payment" class="control-label">{{trans('adminlte::courses.course')}}</label>
        <select id="class_add_new_payment" name="class_add_new_payment" class="form-control select-class-add-new-payment select2 select2-hidden-accessible" data-placeholder="{{trans('adminlte::courses.class')}}" style="width:100%;">
            <option value=""></option>
        </select> 
    </div>   
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="value_add_new_payment" class="control-label">{{trans('adminlte::payments.value')}}</label>
        <input type="number" value="" disabled="disabled" id="value_add_new_payment" class="form-control" minlength="1" maxlength="10" name="value_add_new_payment" placeholder="{{trans('adminlte::payments.value')}}">
    </div> 
    <div class="form-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="month_add_new_payment" class="control-label">{{trans('adminlte::adminlte.month')}}</label>
        <select id="month_add_new_payment" name="month_add_new_payment" class="form-control select-month-add-new-payment select2 select2-hidden-accessible" data-placeholder="{{trans("adminlte::adminlte.month")}}" style="width:100%;">
            <option value=""></option>
            <option value="01">{{trans('adminlte::adminlte.January')}}</option>
            <option value="02">{{trans('adminlte::adminlte.February')}}</option>
            <option value="03">{{trans('adminlte::adminlte.March')}}</option>
            <option value="04">{{trans('adminlte::adminlte.April')}}</option>
            <option value="05">{{trans('adminlte::adminlte.May')}}</option>
            <option value="06">{{trans('adminlte::adminlte.June')}}</option>
            <option value="07">{{trans('adminlte::adminlte.July')}}</option>
            <option value="08">{{trans('adminlte::adminlte.August')}}</option>
            <option value="09">{{trans('adminlte::adminlte.September')}}</option>
            <option value="10">{{trans('adminlte::adminlte.October')}}</option>
            <option value="11">{{trans('adminlte::adminlte.November')}}</option>
            <option value="12">{{trans('adminlte::adminlte.December')}}</option>
        </select>
    </div>
</div>