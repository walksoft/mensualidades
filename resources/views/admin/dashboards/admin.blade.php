@extends('adminlte::page')
@section('title', 'Admin')
@section('content')
<div class="row">
    @if(auth()->user()->hasPermissionTo('list.config') || auth()->user()->hasPermissionTo('edit.config'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('configurations')}}" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fas fa-cogs"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{trans('adminlte::configurations.configurations')}}</span>
                        <span class="info-box-number"></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
    @endif
    @if(auth()->user()->hasPermissionTo('list.roles') || auth()->user()->hasPermissionTo('create.roles') || auth()->user()->hasPermissionTo('edit.roles') || auth()->user()->hasPermissionTo('delete.roles'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('roles.index')}}" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fas fa-unlock-alt"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{trans('adminlte::roles.roles')}}</span>
                        <span class="info-box-number">{{\App\Models\Role::count()}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
    @endif
    @if(auth()->user()->hasPermissionTo('list.course') || auth()->user()->hasPermissionTo('create.course') || auth()->user()->hasPermissionTo('edit.course') || auth()->user()->hasPermissionTo('delete.course') || auth()->user()->hasPermissionTo('list.inscription') || auth()->user()->hasPermissionTo('create.inscription') || auth()->user()->hasPermissionTo('edit.inscription') || auth()->user()->hasPermissionTo('delete.inscription'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('courses.index')}}" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fab fa-buffer"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{trans('adminlte::courses.courses')}}</span>
                        <span class="info-box-number">{{\App\Models\Course::count()}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
    @endif
    @if(auth()->user()->hasPermissionTo('list.user') || auth()->user()->hasPermissionTo('create.user') || auth()->user()->hasPermissionTo('edit.user') || auth()->user()->hasPermissionTo('delete.user'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('users.index')}}" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fas fa-user-friends"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{trans('adminlte::users.users')}}</span>
                        <span class="info-box-number">{{\App\User::count()}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
    @endif
    @if(auth()->user()->hasPermissionTo('list.instructor') || auth()->user()->hasPermissionTo('create.instructor') || auth()->user()->hasPermissionTo('edit.instructor') || auth()->user()->hasPermissionTo('delete.instructor'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('instructors.index')}}" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fas fa-chalkboard-teacher"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Instructores</span>
                        <span class="info-box-number">{{\App\User::where('type_user',2)->count()}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
    @endif
    @if(auth()->user()->hasPermissionTo('list.student') || auth()->user()->hasPermissionTo('create.student') || auth()->user()->hasPermissionTo('edit.student') || auth()->user()->hasPermissionTo('delete.student'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('students.index')}}" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fas fa-user-graduate"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{trans('adminlte::students.students')}}</span>
                        <span class="info-box-number">{{\App\User::where('type_user',3)->count()}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
    @endif
    @if(auth()->user()->hasPermissionTo('list.service') || auth()->user()->hasPermissionTo('create.service') || auth()->user()->hasPermissionTo('edit.service') || auth()->user()->hasPermissionTo('delete.service'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('services.index')}}" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fas fa-briefcase"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{trans('adminlte::services.services')}}</span>
                        <span class="info-box-number">{{\App\Models\Service::count()}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
    @endif
    @if(auth()->user()->hasPermissionTo('list.payment') || auth()->user()->hasPermissionTo('create.payment') || auth()->user()->hasPermissionTo('edit.payment') || auth()->user()->hasPermissionTo('delete.payment'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('payments.index_payments')}}" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fas fa-file-invoice-dollar"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{trans('adminlte::payments.payments')}}</span>
                        <span class="info-box-number">{{\App\Models\Payment::count()}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
    @endif
    @if(auth()->user()->hasPermissionTo('list.report') || auth()->user()->hasPermissionTo('create.report') || auth()->user()->hasPermissionTo('edit.report') || auth()->user()->hasPermissionTo('delete.report'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="#" class="modules">
                <div class="info-box">
                    <span class="info-box-icon bg-blue">
                        <i class="fas fa-chart-pie"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{trans('adminlte::reports.reports')}}</span>
                        <span class="info-box-number">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div> 
    @endif
</div>
@stop