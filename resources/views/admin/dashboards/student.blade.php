@extends('adminlte::page')
@section('title','Admin')
@section('content')
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>{{trans('adminlte::adminlte.welcome')}}</h1>    
    </div>    
</div>   
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-award"></i> {{trans('adminlte::courses.activeCourses')}}</h3>
            </div>  
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-bordered dt-responsive table-head-fixed">
                            <thead>
                                <tr>
                                    <th>{{trans('adminlte::courses.code')}}</th>
                                    <th>{{trans('adminlte::adminlte.name')}}</th>
                                    <th>{{trans('adminlte::courses.schedule')}}</th>
                                    <th>{{trans('adminlte::courses.frequency')}}</th>      
                                    <th>{{trans('adminlte::inscriptions.inscription')}}</th>                                  
                                    <th>{{trans('adminlte::adminlte.status')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(Auth::user()->active_registration->count() > 0)
                                    @foreach(Auth::user()->active_registration as $course)
                                        <tr>
                                            <td>{{$course->course->code}}</td>
                                            <td>{{$course->course->service->name}}</td>
                                            <td>
                                                @php
                                                    $start_time = $course->course->start_time ? \Carbon\Carbon::parse($course->course->start_time)->format('g:iA') : NULL;
                                                    $end_time = $course->course->end_time ? \Carbon\Carbon::parse($course->course->end_time)->format('g:iA') : NULL;
                                                @endphp
                                                {{$start_time}} - {{$end_time}}
                                            </td>
                                            <td>
                                                @if($course->course->array_frequency())
                                                   @foreach($course->course->array_frequency() as $day)
                                                        <span class="badge badge-success">{{$day}}</span>
                                                    @endforeach
                                                @endif                                    
                                            </td>   
                                            <td>{{\Carbon\Carbon::parse($course->created_at)->format('d/m/Y')}}</td>
                                            <td>
                                                @if($course->status)                        
                                                    <span class="badge badge-success">{{trans('adminlte::adminlte.active')}}</span>
                                                @else
                                                    <span class="badge badge-danger">{{trans('adminlte::adminlte.inactive')}}</span>
                                                @endif                    
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td colspan="6">Aun no se encuentran cursos registrados...</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><i class="far fa-money-bill-alt"></i> {{trans('adminlte::payments.pendingPayments')}}</h3>
            </div>  
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-bordered dt-responsive table-head-fixed">
                            <thead>
                                <tr>
                                    <th style="width:10px;text-align:center">#</th>
                                    <th>{{trans('adminlte::payments.reference')}}</th>            
                                    <th style="text-align:center">{{trans('adminlte::payments.value')}}</th>          
                                    <th>{{trans('adminlte::payments.concept')}}</th>
                                    <th>{{trans('adminlte::payments.course')}}</th>
                                    <th>{{trans('adminlte::payments.status')}}</th>
                                    <th>{{trans('adminlte::payments.paydayLimit')}}</th>
                                    <th>{{trans('adminlte::payments.pay')}}</th>
                                </tr>
                            </thead>
                            <tbody>  
                                @php 
                                    $count = 1;
                                @endphp
                                @if(Auth::user()->pending_payments->count() > 0)
                                    @foreach(Auth::user()->pending_payments as $payment)
                                    <tr class="cell_table_record_cash_register">
                                            <td style="width:10px;text-align:center">{{$count++}}</td>
                                            <td>{{$payment->reference}}</td>
                                            <td style="text-align:center">${{number_format($payment->amount,0,',','.')}}</td>
                                            <td>{{$payment->concept->name}}</td>
                                            <td>{{$payment->course->code}} - {{$payment->course->service->name}}</td>
                                            <td>{{$payment->status->name}}</td>
                                            <td>@if($payment->deadline) {{\Carbon\Carbon::parse($payment->deadline)->format('d/m/Y')}} @endif</td>
                                            <td style="text-align:center">
                                                <a href="javascript:void(0);" onclick="exec_payment('{{$payment->id}}')" data-payment="{{$payment->id}}"><i class="fab fa-cc-paypal fa-2x"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8">
                                            No se encontraron pagos pendientes...
                                        </td>
                                    </tr>
                                @endif        
                            </tbody>
                        </table>     
                        <form class="hidden" method="POST" id="payment-form"  action="{{route('students.paypal_payment')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="payment" id="payment" value="">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
@stop
@section('js')
<script>
function exec_payment(e){
    if(!e || e == ''){
        return false;
    }//if
    $("#payment").val(e);
    $("#payment-form").submit();
}//exec_payment
</script>
@stop