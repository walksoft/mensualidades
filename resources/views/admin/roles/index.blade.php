@extends('adminlte::page')

@section('title',trans('adminlte::roles.roles'))

@section('content_header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1>
            <i class="fa fa-unlock-alt" style="margin:10px 10px 10px 10px;"></i>
            {{trans('adminlte::roles.roles')}}
        </h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            
            <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('adminlte::adminlte.administration')}}</a></li>
            <li class="breadcrumb-item active">{{trans('adminlte::roles.roles')}}</li>
        </ol>
    </div>
</div>

@stop


@section('content')

    <div class="card">
        <div class="card-header">
          <div class="card-title">
          @if($permits['create'])<a href="#" class="btn btn-block btn-success btnModalCreate" data-toggle="modal">Crear</a>@endif
          </div>
        </div>

        <div class="card-body">          
            {!! $html->table(['class' => 'table table-striped dt-responsive', 'style' => 'width:100%']) !!}
        </div>

        <!-- /.card-body -->
        <div class="card-footer"></div>
        <!-- /.card-footer-->
    </div>


    <div class="modal fade" id="modal-create-rol" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
                <h5 class="modal-title" id="myModalLabelCreate"><i class="fa fa-unlock-alt"></i> Crear Rol</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">

            <div class="container-fluid">
                    <div id="content-create-rol"></div>
                    </div>               
            </div>
        <div class="modal-footer">
            
            <div class="w-100">
                <button type="button" class="btn btn-secondary float-left" data-dismiss="modal">{{trans('adminlte::adminlte.close')}}</button>
                <button type="button" id="btn-save-create-rol" name="btn-save-ceate-rol" class="btn btn-success float-right">{{trans('adminlte::adminlte.save')}}</button>
            </div>

        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    

    <div class="modal fade" id="modal-edit-rol" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
                <h5 class="modal-title" id="myModalLabel"><i class="fa fa-unlock-alt"></i> Editar Rol</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">

            <div class="container-fluid">
                    <div id="content-edit-rol"></div>
                    </div>               
            </div>
        <div class="modal-footer">
            
            <div class="w-100">
                <button type="button" class="btn btn-secondary float-left" data-dismiss="modal">{{trans('adminlte::adminlte.close')}}</button>
                <button type="button" id="btn-save-edit-rol" name="btn-save-edit-rol" class="btn btn-success float-right" data-target="#confirmEdit">{{trans('adminlte::adminlte.save')}}</button>
            </div>

        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>

@stop
@section('js')

{!! $html->scripts() !!}

<script>

  $(function () {

        var checkAll = $('input.checkall');

        $(document).on('click', ".checkall", function () {
            $('#'+$(this).data('rel')).children().find("input[type=checkbox]").prop('checked', this.checked);
        });

        $(".btnModalCreate").click(function(){
            
            var _token = "{{csrf_token()}}";
            var _method = "PUT";
            var idRol = $(this).data('id');
            
            // Limpiamos modal de EDICIÓN para evitar errores de ids y clases duplicados
            $("#content-edit-rol").html('');

            $.ajax({
                headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
                url: "{{route('roles.create')}}",
                type:'GET',
                cache:false,
                data:{'_token':_token,'_method':_method},
                datatype:'html',
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete: function(){
                    $('.contaninerLoading').hide(); 
                },
                success:function(data){

                  $("#content-create-rol").html(data);
                  $('#modal-create-rol').modal('show');
                
                },
                error:function(xhr,textStatus,thrownError){
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
            });       
        });


        $(document).on('click', '#btn-save-create-rol', function () {
            
            let _token = "{{csrf_token()}}";
            let _method = "POST";
            let name = $('#name').val();
            let description = $('#description').val();
            let permits = [];

            $('#createForm input[type=checkbox]').each(function(){
                if (this.checked) {
                    permits.push( $(this).val() );
                }
            }); 


            $.ajax({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                url: "{{route('roles.store')}}",
                type: 'POST',
                cache: false,
                data: {
                        name:name,
                        description:description, 
                        permits:permits,
                        '_token':_token,
                        '_method':_method
                },
                datatype: 'html',
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete: function(){
                    $('.contaninerLoading').hide(); 
                },
                success: function(data){

                    if(data.status == 1){
                        $('#modal-create-rol').modal('hide');
                        msgNotification(data.type, data.msg);

                        // Se refresca tabla por ajax
                        let table = $('.table').DataTable();
                        table.ajax.reload();
                    }else{
                        ctlErrorRequestAjax(data);
                    }
                },
                error: function (xhr,textStatus,thrownError) {
 
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
    
            });
        
        }); 

        
        $(document).on('click', '.btnModalEdit', function(){
            
            var _token = "{{csrf_token()}}";
            var _method = "PUT";
            var idRol = $(this).data('id');
            
            // Limpiamos modal de CREACIÓN para evitar errores de ids y clases duplicados
            $("#content-create-rol").html('');


            $.ajax({
                headers:{'X-CSRF-Token':$('meta[name=_token]').attr('content')},
                url: "{{route('roles.ajax.edit')}}",
                type:'POST',
                cache:false,
                data:{'_id': idRol,'_token':_token,'_method':_method},
                datatype:'html',
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete: function(){
                    $('.contaninerLoading').hide(); 
                },
                success:function(data){

                  
                  $("#content-edit-rol").html(data);
                  $('#modal-edit-rol').modal('show');
                
                },
                error:function(xhr,textStatus,thrownError){
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
            });       
        });
  
        $(document).on('click', '#btn-save-edit-rol', function () {
            
                let _token = "{{csrf_token()}}";
                let _method = "PUT";
                let id = $("#id").val();
                let name = $('#name').val();
                let description = $('#description').val();
                let permits = [];

                $('#editForm input[type=checkbox]').each(function(){
                    if (this.checked) {
                        permits.push( $(this).val() );
                    }
                }); 


                $.ajax({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    url: "{{route('roles.ajax.update')}}",
                    type: 'POST',
                    cache: false,
                    data: {
                            id:id,
                            name:name,
                            description:description, 
                            permits:permits,
                            '_token':_token,
                            '_method':_method
                    },
                    datatype: 'html',
                    beforeSend:function(){
                        $('.contaninerLoading').show();
                    },
                    complete: function(){
                        $('.contaninerLoading').hide(); 
                    },
                    success: function(data){

                        if(data.status == 1){
                            $('#modal-edit-rol').modal('hide');
                            msgNotification(data.type, data.msg);

                            // Se refresca tabla por ajax
                            let table = $('.table').DataTable();
                            table.ajax.reload();

                        }else{
                            ctlErrorRequestAjax(data);
                        }
                    },
                    error: function (xhr,textStatus,thrownError) {
     
                        alert(xhr + "\n" + textStatus + "\n" + thrownError);
                    }
        
                });
        
        }); 

        // Confirm delete
        $('.deleteForm').click(function(){

            let _token = "{{csrf_token()}}";
            let _method = "PUT";
            let id = $('#idDelete').val();

            $.ajax({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                url: "{{route('roles.ajax.delete')}}",
                type: 'POST',
                cache: false,
                data: {
                        id:id,
                        '_token':_token,
                        '_method':_method
                },
                datatype: 'html',
                beforeSend:function(){
                    $('.contaninerLoading').show();
                },
                complete: function(){
                    $('.contaninerLoading').hide(); 
                },
                success: function(data){

                    if(data.status == 1){
                        $('#confirmDelete').modal('hide');
                        msgNotification(data.type, data.msg);

                        // Se refresca tabla por ajax
                        let table = $('.table').DataTable();
                        table.ajax.reload();

                    }
                },
                error: function (xhr,textStatus,thrownError) {
    
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
    
            });
        });

    });

    </script>
 @stop









  