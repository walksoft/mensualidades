
<style>
  .project-tab #tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #0062cc;
    border-bottom: 3px solid !important;
    font-size: 16px;
    font-weight: bold;
  }
</style>
<div class="row">
    <div class="col-md-12">
       
      
            <form  id="createForm" method="post" action="{{route('roles.store')}}">
            {{csrf_field()}}
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label for="name" class="control-label">{{trans('adminlte::adminlte.name')}}(*)</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="{{trans('adminlte::adminlte.name')}}">
                    </div>
                </div>

                <div class="form-group">
                  <label for="description" class="control-label">{{trans('adminlte::adminlte.description')}}</label>
                    <textarea id="description" class="form-control" rows="3" maxlength="255" name="description"></textarea>
                </div>


                <div class="clearfix">
                    <h5 class="box-title">{{trans('adminlte::roles.permissions')}}</h5>
                </div>

            
                <div class="row">

                  <div class="col-6 col-sm-6">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                    
                      @foreach(config('permits.modules') as $module)
                      <a class="nav-link @if(array_key_exists('activeTab',$module)) active @endif" id="vert-tabs-{{trans($module['module'])}}-tab" data-toggle="pill" href="#vert-tabs-{{trans($module['module'])}}" role="tab" aria-controls="vert-tabs-{{trans($module['module'])}}" aria-selected="false">{{trans($module['module'])}}</a>
                      @endforeach
                    
                    </div>
                  </div>

                  <div class="col-6 col-sm-6">
                    <div class="tab-content" id="vert-tabs-tabContent">


                        @foreach(config('permits.modules') as $module)
                          <div class="tab-pane fade @if(array_key_exists('activeTab',$module)) show active @endif" id="vert-tabs-{{trans($module['module'])}}" role="tabpanel" aria-labelledby="vert-tabs-{{trans($module['module'])}}-  -tab">
                            
                            <!-- Minimal style -->
                            <div class="row">
                              <div class="col-sm-6">

                          
                                  <!-- checkbox -->
                                  <div class="form-group clearfix">
                                          <div class="icheck-primary d-inline">
                                              <input type="checkbox" name="checkall-{{$module['slug']}}" id="checkall-{{$module['slug']}}" value="" class="checkall" data-rel="vert-tabs-{{trans($module['module'])}}">
                                              <label for="checkall-{{$module['slug']}}">
                                              {{trans('adminlte::adminlte.all')}}
                                              </label>
                                          </div>
                                      </div>
                                  @if(array_key_exists('slugs',$module))
                                                        
                                    @php
                                    if(is_array($module['slugs'])){
                                        $module_slugs = $module['slugs'];
                                    }else{
                                    eval('$module_slugs = '.$module['slugs'].';');
                                    }
                                    @endphp

                                    @foreach($module_slugs as $slug)
                                      <!-- checkbox -->
                                      <div class="form-group clearfix">
                                          <div class="icheck-primary d-inline">
                                              <input type="checkbox" name="permits[{{$slug['slug']}}]" id="permits-{{$slug['slug']}}" value="{{$slug['slug']}}">
                                              <label for="permits-{{$slug['slug']}}">
                                              {{trans($slug['display'])}}
                                              </label>
                                          </div>
                                      </div>
                                    @endforeach
                                  @endif
                                
                              </div>
                            </div>

                          </div>
                        @endforeach
                      

                    </div>
                  </div>

                </div>
        

            </div>

            </form>
        






    </div>



       
</div>

@section('js')
<script type="text/javascript">
    $(function(){

      var checkAll = $('input.checkall');

      $(".checkall").click(function(){
          $('#'+$(this).data('rel')).children().find("input[type=checkbox]").prop('checked', this.checked);
      });
  
    });
</script>
@stop