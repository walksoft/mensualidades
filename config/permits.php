<?php

return [

 
    /*
    |--------------------------------------------------------------------------
    | Menu Items - permits
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */
   'modules' => [
        //Configuraciones
        'configuraciones' =>[
            'module'=> 'Configuraciones',
            'slug'  => 'configuraciones',       
            'activeTab' => true,
            'slugs' => [
                [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List Config',
                    'slug' => 'list.config',
                ],
		[
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit Config',
                    'slug' => 'edit.config',
                ],
            ],
            'arraySlugs'=>[
                'list.config',
                'edit.config',
            ],
        ],
        //Tipos de servicios
        'tipos_de_servicios' =>[
            'module'=> 'Tipos_de_servicios',
            'slug'  => 'tipos_de_servicios',       
            'slugs' => [
                [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List type service',
                    'slug' => 'list.typeservice',
                ],
		[
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create type service',
                    'slug' => 'create.typeservice',
                ],
                [
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit type service',
                    'slug' => 'edit.typeservice',
                ],
                [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete type service',
                    'slug' => 'delete.typeservice',
                ],
            ],
            'arraySlugs'=>[
                'list.typeservice',
                'create.typeservice',
                'edit.typeservice',
                'delete.typeservice',
            ],
        ],
        //Tipos de niveles
        'tipos_de_niveles' =>[
            'module'=> 'Tipos_de_niveles',
            'slug'  => 'tipos_de_niveles',       
            'slugs' => [
                [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List type level',
                    'slug' => 'list.typelevel',
                ],
		[
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create type level',
                    'slug' => 'create.typelevel',
                ],
                [
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit type level',
                    'slug' => 'edit.typelevel',
                ],
                [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete type level',
                    'slug' => 'delete.typelevel',
                ],
            ],
            'arraySlugs'=>[
                'list.typelevel',
                'create.typelevel',
                'edit.typelevel',
                'delete.typelevel',
            ],
        ],       
        //Roles
        'roles' =>[
            'module'=> 'adminlte::roles.roles',
            'slug'  => 'roles',            
            'slugs' => [
		        [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List roles',
                    'slug' => 'list.roles',
                ],
                [
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create roles',
                    'slug' => 'create.roles',
                ],
                [
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit roles',
                    'slug' => 'edit.roles',
                ],
                [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete roles',
                    'slug' => 'delete.roles',
                ],
            ],
            'arraySlugs'=>[
                'list.roles',
                'create.roles',
                'edit.roles',
                'delete.roles',
            ],
        ],
        //Inscripciones
        'inscripciones' =>[
            'module'=> 'adminlte::inscriptions.inscriptions',
            'slug'  => 'inscripciones',          
            'slugs' => [
                [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List Inscriptions',
                    'slug' => 'list.inscription',
                ],
                [
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create Inscription',
                    'slug' => 'create.inscription',
                ],
                [
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit Inscription',
                    'slug' => 'edit.inscription',
                ],
                [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete Inscription',
                    'slug' => 'delete.inscription',
                ],
                [
                    'display' => 'adminlte::inscriptions.modifyPrices',//delete
                    'name' => 'Update Values',
                    'slug' => 'update_values.inscription',
                ],
            ],
            'arraySlugs'=>[
                'list.inscription',
                'create.inscription',
                'edit.inscription',
                'delete.inscription',
                'update_values.inscription',
            ],
        ],
        //Courses
        'courses' =>[
            'module'=> 'adminlte::courses.courses',
            'slug'  => 'cursos',          
            'slugs' => [
		[
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List Course',
                    'slug' => 'list.course',
                ],
                [
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create Course',
                    'slug' => 'create.course',
                ],
		[
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit Course',
                    'slug' => 'edit.course',
                ],
                [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete Course',
                    'slug' => 'delete.course',
                ],
                [
                    'display' => 'adminlte::courses.payment_instructor',//payment
                    'name' => 'Payment Course',
                    'slug' => 'payment.course',
                ],                
            ],
            'arraySlugs'=>[
                'list.course',
                'create.course',
                'edit.course',
                'delete.course',
                'payment.course',
            ],
        ],
        //Users
        'users' =>[
            'module'=> 'adminlte::users.users',
            'slug'  => 'usuarios',          
            'slugs' => [
		        [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List User',
                    'slug' => 'list.user',
                ],
                [
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create User',
                    'slug' => 'create.user',
                ],
				[
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit User',
                    'slug' => 'edit.user',
                ],
		        [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete User',
                    'slug' => 'delete.user',
                ],
            ],
            'arraySlugs'=>[
                'list.user',
                'create.user',
                'edit.user',
                'delete.user',
            ],
        ],
        //Instructores
        'instructors' =>[
            'module'=> 'adminlte::instructors.instructors',
            'slug'  => 'instructores',          
            'slugs' => [
		        [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List Instructor',
                    'slug' => 'list.instructor',
                ],
                [
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create Instructor',
                    'slug' => 'create.instructor',
                ],
				[
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit Instructor',
                    'slug' => 'edit.instructor',
                ],
		        [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete Instructor',
                    'slug' => 'delete.instructor',
                ],
            ],
            'arraySlugs'=>[
                'list.instructor',
                'create.instructor',
                'edit.instructor',
                'delete.instructor',
            ],
        ],
        //Students
        'alumnos' =>[
            'module'=> 'Alumnos',
            'slug'  => 'alumnos',          
            'slugs' => [
                [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List Student',
                    'slug' => 'list.student',
                ],
                [
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create Student',
                    'slug' => 'create.student',
                ],
		[
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit Student',
                    'slug' => 'edit.student',
                ],
		[
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete Student',
                    'slug' => 'delete.student',
                ],
            ],
            'arraySlugs'=>[
                'list.student',
                'create.student',
                'edit.student',
                'delete.student',
            ],
        ],
        //Servicios
        'servicios' =>[
            'module'=> 'Servicios',
            'slug'  => 'servicios',          
            'slugs' => [
		        [
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List Service',
                    'slug' => 'list.service',
                ],
                [
                    'display' => 'adminlte::adminlte.create',//create
                    'name' => 'Create Service',
                    'slug' => 'create.service',
                ],
				[
                    'display' => 'adminlte::adminlte.edit',//edit
                    'name' => 'Edit Service',
                    'slug' => 'edit.service',
                ],
		        [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete Service',
                    'slug' => 'delete.service',
                ],
            ],
            'arraySlugs'=>[
                'list.service',
                'create.service',
                'edit.service',
                'delete.service',
            ],
        ],
        //Pagos
        'pagos' =>[
            'module'=> 'Pagos',
            'slug'  => 'pagos',          
            'slugs' => [
		[
                    'display' => 'adminlte::adminlte.list',//listar
                    'name' => 'List Payment',
                    'slug' => 'list.payment',
                ],
//                [
//                    'display' => 'adminlte::adminlte.create',//create
//                    'name' => 'Create Payment',
//                    'slug' => 'create.payment',
//                ],
//                [
//                    'display' => 'adminlte::adminlte.edit',//edit
//                    'name' => 'Edit Payment',
//                    'slug' => 'edit.payment',
//                ],
                [
                    'display' => 'adminlte::adminlte.delete',//delete
                    'name' => 'Delete Payment',
                    'slug' => 'delete.payment',
                ],
            ],
            'arraySlugs'=>[
                'list.payment',
                'create.payment',
                'edit.payment',
                'delete.payment',
            ],
        ],
        //Caja
        'caja' =>[
            'module'=> 'Caja',
            'slug'  => 'caja',          
            'slugs' => [
		[
                    'display' => 'adminlte::payments.savePayment',//Guardar pago
                    'name' => 'List Payment',
                    'slug' => 'cash.register',
                ],
                [
                    'display' => 'adminlte::payments.generatePayment',//Generar cobro
                    'name' => 'Create Payment',
                    'slug' => 'generate.payment',
                ],
            ],
            'arraySlugs'=>[
                'cash.register',
                'generate.payment',
            ],
        ],
      
    ],

];
