<?php 
return [ 
    
    'debug' => true,
    
    'settings' => [
                
        'http.ConnectionTimeOut' => 30,
        
        'log.LogEnabled' => true,
        
        'log.FileName' => storage_path() . '/logs/paypal.log',
        
        'log.LogLevel' => 'ERROR'
        
    ],
];